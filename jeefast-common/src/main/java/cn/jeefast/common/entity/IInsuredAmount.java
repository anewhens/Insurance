package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-03-06
 */
@TableName("i_insured_amount")
public class IInsuredAmount extends Model<IInsuredAmount> {

    private static final long serialVersionUID = 1L;

	@TableId(value="iia_id", type= IdType.AUTO)
	private Long iiaId;
    /**
     * 产品名称
     */
	@TableField("iia_title")
	private String iiaTitle;
    /**
     * 保险公司
     */
	@TableField("iia_comp")
	private Long iiaComp;
    /**
     * 产品类别
     */
	@TableField("iia_type")
	private Long iiaType;
    /**
     * 缩略图
     */
	@TableField("iia_cover")
	private String iiaCover;
    /**
     * 产品附件 {"files":[{"fileName":"附件1","fileUrl":"http://www.baidu.com"}]}
     */
	@TableField("iia_file")
	private String iiaFile;
    /**
     * 附加险
     */
	@TableField("iia_add")
	private String iiaAdd;
    /**
     * 计划书背景图
     */
	@TableField("iia_banner")
	private String iiaBanner;
    /**
     * 计划书条目：{"tips":[{"tipTitle":"身故保障","tip":[{"subTitle":"105种重疾险","subDescrb":"第一次确诊，，，，"},{"subTitle":"25种重症","subDescrb":"第一次确诊，，，，"}]},{"tipTitle":"身故保障","tip":[{"subTitle":"105种重疾险","subDescrb":"第一次确诊，，，，"},{"subTitle":"25种重症","subDescrb":"第一次确诊，，，，"}]}]}
     */
	@TableField("iia_tips")
	private String iiaTips;
    /**
     * 疾病：{"disease":[{"dName":"105种重疾险","dDescrib":"第一组：蛇精病"},{"dName":"第二组迷人的郭老师","dDescrib":"第一组：蛇精病"}]}
     */
	@TableField("iia_disease")
	private String iiaDisease;

	@TableField("iia_i_type")
	private Integer iiaIType;

	private Integer deletes;

	@TableField("pc_banner")
	private String pcBanner;

	public String getPcBanner() {
		return pcBanner;
	}

	public void setPcBanner(String pcBanner) {
		this.pcBanner = pcBanner;
	}

	public Integer getDeletes() {
		return deletes;
	}

	public void setDeletes(Integer deletes) {
		this.deletes = deletes;
	}

	public Integer getIiaIType() {
		return iiaIType;
	}

	public void setIiaIType(Integer iiaIType) {
		this.iiaIType = iiaIType;
	}

	public Long getIiaId() {
		return iiaId;
	}

	public void setIiaId(Long iiaId) {
		this.iiaId = iiaId;
	}

	public String getIiaTitle() {
		return iiaTitle;
	}

	public void setIiaTitle(String iiaTitle) {
		this.iiaTitle = iiaTitle;
	}

	public Long getIiaComp() {
		return iiaComp;
	}

	public void setIiaComp(Long iiaComp) {
		this.iiaComp = iiaComp;
	}

	public Long getIiaType() {
		return iiaType;
	}

	public void setIiaType(Long iiaType) {
		this.iiaType = iiaType;
	}

	public String getIiaCover() {
		return iiaCover;
	}

	public void setIiaCover(String iiaCover) {
		this.iiaCover = iiaCover;
	}

	public String getIiaFile() {
		return iiaFile;
	}

	public void setIiaFile(String iiaFile) {
		this.iiaFile = iiaFile;
	}

	public String getIiaAdd() {
		return iiaAdd;
	}

	public void setIiaAdd(String iiaAdd) {
		this.iiaAdd = iiaAdd;
	}

	public String getIiaBanner() {
		return iiaBanner;
	}

	public void setIiaBanner(String iiaBanner) {
		this.iiaBanner = iiaBanner;
	}

	public String getIiaTips() {
		return iiaTips;
	}

	public void setIiaTips(String iiaTips) {
		this.iiaTips = iiaTips;
	}

	public String getIiaDisease() {
		return iiaDisease;
	}

	public void setIiaDisease(String iiaDisease) {
		this.iiaDisease = iiaDisease;
	}

	@Override
	protected Serializable pkVal() {
		return this.iiaId;
	}

	@Override
	public String toString() {
		return "IInsuredAmount{" +
			", iiaId=" + iiaId +
			", iiaTitle=" + iiaTitle +
			", iiaComp=" + iiaComp +
			", iiaType=" + iiaType +
			", iiaCover=" + iiaCover +
			", iiaFile=" + iiaFile +
			", iiaAdd=" + iiaAdd +
			", iiaBanner=" + iiaBanner +
			", iiaTips=" + iiaTips +
			", iiaDisease=" + iiaDisease +
			"}";
	}
}
