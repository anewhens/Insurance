package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
@TableName("i_insured_amount_book")
public class IInsuredAmountBook extends Model<IInsuredAmountBook> {

    private static final long serialVersionUID = 1L;

	@TableId(value="iiab_id", type= IdType.AUTO)
	private Long iiabId;
    /**
     * 被保人姓名
     */
	@TableField("iiab_bname")
	private String iiabBname;
    /**
     * 1:男 0：女
     */
	@TableField("iiab_bsex")
	private Integer iiabBsex;
    /**
     * 出生年月
     */
	@TableField("iiab_bbirth")
	private String iiabBbirth;
    /**
     * 投保人姓名
     */
	@TableField("iiab_tname")
	private String iiabTname;
    /**
     * 1:男 0：女
     */
	@TableField("iiab_tsex")
	private Integer iiabTsex;
    /**
     * 出生年月
     */
	@TableField("iiab_tbirth")
	private String iiabTbirth;
    /**
     * 保额
     */
	@TableField("iiab_money")
	private String iiabMoney;
    /**
     * 缴费期间
     */
	@TableField("iiab_pay")
	private String iiabPay;
    /**
     * 保险期间
     */
	@TableField("iiab_time")
	private String iiabTime;
    /**
     * 附加险，逗号分隔
     */
	@TableField("iiab_add")
	private String iiabAdd;
    /**
     * 保费
     */
	@TableField("iiab_fee")
	private String iiabFee;

	@TableField("iiab_add_time")
	private Date iiabAddTime;

	@TableField("iiab_share")
	private Long iiabShare;

	@TableField("iiab_iaId")
	private Long iiabIaId;

	@TableField("iiab_feelist")
	private String iiabFeelist;

	public Long getIiabId() {
		return iiabId;
	}

	public void setIiabId(Long iiabId) {
		this.iiabId = iiabId;
	}

	public String getIiabBname() {
		return iiabBname;
	}

	public void setIiabBname(String iiabBname) {
		this.iiabBname = iiabBname;
	}

	public Integer getIiabBsex() {
		return iiabBsex;
	}

	public void setIiabBsex(Integer iiabBsex) {
		this.iiabBsex = iiabBsex;
	}

	public String getIiabBbirth() {
		return iiabBbirth;
	}

	public void setIiabBbirth(String iiabBbirth) {
		this.iiabBbirth = iiabBbirth;
	}

	public String getIiabTname() {
		return iiabTname;
	}

	public void setIiabTname(String iiabTname) {
		this.iiabTname = iiabTname;
	}

	public Integer getIiabTsex() {
		return iiabTsex;
	}

	public void setIiabTsex(Integer iiabTsex) {
		this.iiabTsex = iiabTsex;
	}

	public String getIiabTbirth() {
		return iiabTbirth;
	}

	public void setIiabTbirth(String iiabTbirth) {
		this.iiabTbirth = iiabTbirth;
	}

	public String getIiabMoney() {
		return iiabMoney;
	}

	public void setIiabMoney(String iiabMoney) {
		this.iiabMoney = iiabMoney;
	}

	public String getIiabPay() {
		return iiabPay;
	}

	public void setIiabPay(String iiabPay) {
		this.iiabPay = iiabPay;
	}

	public String getIiabTime() {
		return iiabTime;
	}

	public void setIiabTime(String iiabTime) {
		this.iiabTime = iiabTime;
	}

	public String getIiabAdd() {
		return iiabAdd;
	}

	public void setIiabAdd(String iiabAdd) {
		this.iiabAdd = iiabAdd;
	}

	public String getIiabFee() {
		return iiabFee;
	}

	public void setIiabFee(String iiabFee) {
		this.iiabFee = iiabFee;
	}

	public Date getIiabAddTime() {
		return iiabAddTime;
	}

	public void setIiabAddTime(Date iiabAddTime) {
		this.iiabAddTime = iiabAddTime;
	}

	public Long getIiabShare() {
		return iiabShare;
	}

	public void setIiabShare(Long iiabShare) {
		this.iiabShare = iiabShare;
	}

	public Long getIiabIaId() {
		return iiabIaId;
	}

	public void setIiabIaId(Long iiabIaId) {
		this.iiabIaId = iiabIaId;
	}

	public String getIiabFeelist() {
		return iiabFeelist;
	}

	public void setIiabFeelist(String iiabFeelist) {
		this.iiabFeelist = iiabFeelist;
	}

	@Override
	protected Serializable pkVal() {
		return this.iiabId;
	}

	@Override
	public String toString() {
		return "IInsuredAmountBook{" +
			", iiabId=" + iiabId +
			", iiabBname=" + iiabBname +
			", iiabBsex=" + iiabBsex +
			", iiabBbirth=" + iiabBbirth +
			", iiabTname=" + iiabTname +
			", iiabTsex=" + iiabTsex +
			", iiabTbirth=" + iiabTbirth +
			", iiabMoney=" + iiabMoney +
			", iiabPay=" + iiabPay +
			", iiabTime=" + iiabTime +
			", iiabAdd=" + iiabAdd +
			", iiabFee=" + iiabFee +
			"}";
	}
}
