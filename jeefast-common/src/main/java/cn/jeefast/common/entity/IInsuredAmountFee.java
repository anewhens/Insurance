package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-03-06
 */
@TableName("i_insured_amount_fee")
public class IInsuredAmountFee extends Model<IInsuredAmountFee> {

    private static final long serialVersionUID = 1L;

	@TableId(value="iiaf_id", type= IdType.AUTO)
	private Long iiafId;
	@TableField("iia_id")
	private Long iiaId;
    /**
     * 年龄
     */
	@TableField("iiaf_age")
	private Integer iiafAge;
    /**
     * 性别
     */
	@TableField("iiaf_sex")
	private String iiafSex;
    /**
     * 保险期间
     */
	@TableField("iiaf_time")
	private String iiafTime;
    /**
     * 缴费期间
     */
	@TableField("iiaf_fee_time")
	private String iiafFeeTime;
    /**
     * 保额
     */
	@TableField("iiaf_money")
	private String iiafMoney;


	public Long getIiafId() {
		return iiafId;
	}

	public void setIiafId(Long iiafId) {
		this.iiafId = iiafId;
	}

	public Long getIiaId() {
		return iiaId;
	}

	public void setIiaId(Long iiaId) {
		this.iiaId = iiaId;
	}

	public Integer getIiafAge() {
		return iiafAge;
	}

	public void setIiafAge(Integer iiafAge) {
		this.iiafAge = iiafAge;
	}

	public String getIiafSex() {
		return iiafSex;
	}

	public void setIiafSex(String iiafSex) {
		this.iiafSex = iiafSex;
	}

	public String getIiafTime() {
		return iiafTime;
	}

	public void setIiafTime(String iiafTime) {
		this.iiafTime = iiafTime;
	}

	public String getIiafFeeTime() {
		return iiafFeeTime;
	}

	public void setIiafFeeTime(String iiafFeeTime) {
		this.iiafFeeTime = iiafFeeTime;
	}

	public String getIiafMoney() {
		return iiafMoney;
	}

	public void setIiafMoney(String iiafMoney) {
		this.iiafMoney = iiafMoney;
	}

	@Override
	protected Serializable pkVal() {
		return this.iiafId;
	}

	@Override
	public String toString() {
		return "IInsuredAmountFee{" +
			", iiafId=" + iiafId +
			", iiaId=" + iiaId +
			", iiafAge=" + iiafAge +
			", iiafSex=" + iiafSex +
			", iiafTime=" + iiafTime +
			", iiafFeeTime=" + iiafFeeTime +
			", iiafMoney=" + iiafMoney +
			"}";
	}
}
