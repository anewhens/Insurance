package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-03-05
 */
@TableName("ir_additional")
public class IrAdditional extends Model<IrAdditional> {

    private static final long serialVersionUID = 1L;

	@TableId(value="ia_id", type= IdType.AUTO)
	private Long iaId;
    /**
     * 产品名称
     */
	@TableField("ia_title")
	private String iaTitle;
    /**
     * 保险公司
     */
	@TableField("ia_comm")
	private Long iaComm;


	public Long getIaId() {
		return iaId;
	}

	public void setIaId(Long iaId) {
		this.iaId = iaId;
	}

	public String getIaTitle() {
		return iaTitle;
	}

	public void setIaTitle(String iaTitle) {
		this.iaTitle = iaTitle;
	}

	public Long getIaComm() {
		return iaComm;
	}

	public void setIaComm(Long iaComm) {
		this.iaComm = iaComm;
	}

	@Override
	protected Serializable pkVal() {
		return this.iaId;
	}

	@Override
	public String toString() {
		return "IrAdditional{" +
			", iaId=" + iaId +
			", iaTitle=" + iaTitle +
			", iaComm=" + iaComm +
			"}";
	}
}
