package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-03-05
 */
@TableName("ir_additional_fee")
public class IrAdditionalFee extends Model<IrAdditionalFee> {

    private static final long serialVersionUID = 1L;

	@TableId(value="iaf_id", type= IdType.AUTO)
	private Long iafId;
	@TableField("ia_id")
	private Long iaId;
    /**
     * 年龄
     */
	@TableField("ia_age")
	private Integer iaAge;
    /**
     * 性别
     */
	@TableField("ia_sex")
	private String iaSex;
    /**
     * 保险期间
     */
	@TableField("ia_time")
	private String iaTime;
    /**
     * 缴费期间
     */
	@TableField("ia_fee_time")
	private String iaFeeTime;

	@TableField("ia_money")
	private String iaMoney;

	public Long getIafId() {
		return iafId;
	}

	public void setIafId(Long iafId) {
		this.iafId = iafId;
	}

	public Long getIaId() {
		return iaId;
	}

	public void setIaId(Long iaId) {
		this.iaId = iaId;
	}

	public Integer getIaAge() {
		return iaAge;
	}

	public void setIaAge(Integer iaAge) {
		this.iaAge = iaAge;
	}

	public String getIaSex() {
		return iaSex;
	}

	public void setIaSex(String iaSex) {
		this.iaSex = iaSex;
	}

	public String getIaTime() {
		return iaTime;
	}

	public void setIaTime(String iaTime) {
		this.iaTime = iaTime;
	}

	public String getIaFeeTime() {
		return iaFeeTime;
	}

	public void setIaFeeTime(String iaFeeTime) {
		this.iaFeeTime = iaFeeTime;
	}

	public String getIaMoney() {
		return iaMoney;
	}

	public void setIaMoney(String iaMoney) {
		this.iaMoney = iaMoney;
	}

	@Override
	protected Serializable pkVal() {
		return this.iaId;
	}

	@Override
	public String toString() {
		return "IrAdditionalFee{" +
			", iafId=" + iafId +
			", iaId=" + iaId +
			", iaAge=" + iaAge +
			", iaSex=" + iaSex +
			", iaTime=" + iaTime +
			", iaFeeTime=" + iaFeeTime +
			"}";
	}
}
