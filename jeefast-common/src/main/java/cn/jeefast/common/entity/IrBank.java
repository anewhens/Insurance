package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-13
 */
@TableName("ir_bank")
public class IrBank extends Model<IrBank> {

    private static final long serialVersionUID = 1L;

    @TableId("bank_id")
	private Long bankId;
    /**
     * 1:市行 2：支行 3：网点
     */
	@TableField("bank_type")
	private Integer bankType;
    /**
     * 银行名称
     */
	@TableField("bank_name")
	private String bankName;
    /**
     * 父级ID
     */
	@TableField("back_parent")
	private Long backParent;

	private Integer deletes;

	public Integer getDeletes() {
		return deletes;
	}

	public void setDeletes(Integer deletes) {
		this.deletes = deletes;
	}

	public Long getBankId() {
		return bankId;
	}

	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}

	public Integer getBankType() {
		return bankType;
	}

	public void setBankType(Integer bankType) {
		this.bankType = bankType;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Long getBackParent() {
		return backParent;
	}

	public void setBackParent(Long backParent) {
		this.backParent = backParent;
	}

	@Override
	protected Serializable pkVal() {
		return this.bankId;
	}

	@Override
	public String toString() {
		return "IrBank{" +
			", bankId=" + bankId +
			", bankType=" + bankType +
			", bankName=" + bankName +
			", backParent=" + backParent +
			"}";
	}
}
