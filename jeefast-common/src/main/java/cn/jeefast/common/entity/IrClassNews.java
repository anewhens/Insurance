package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
@TableName("ir_class_news")
public class IrClassNews extends Model<IrClassNews> {

    private static final long serialVersionUID = 1L;

	@TableId(value="cn_id", type= IdType.AUTO)
	private Long cnId;
    /**
     * 课堂资讯标题
     */
	@TableField("cn_title")
	private String cnTitle;
    /**
     * 课堂资讯封面
     */
	@TableField("cn_cover")
	private String cnCover;
    /**
     * 1：重疾险专题 2：小白入门 3：医疗险专题 4：意外险专题
     */
	@TableField("cn_type")
	private Integer cnType;
    /**
     * 发布时间
     */
	@TableField("cn_time")
	private Date cnTime;
    /**
     * 点击量
     */
	@TableField("cn_count")
	private Integer cnCount;
    /**
     * 课堂资讯内容
     */
	@TableField("cn_content")
	private String cnContent;
	/**
     * 关联得到的类型名称
     */
	@TableField(exist = false)
	private String typeName;

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Long getCnId() {
		return cnId;
	}

	public void setCnId(Long cnId) {
		this.cnId = cnId;
	}

	public String getCnTitle() {
		return cnTitle;
	}

	public void setCnTitle(String cnTitle) {
		this.cnTitle = cnTitle;
	}

	public String getCnCover() {
		return cnCover;
	}

	public void setCnCover(String cnCover) {
		this.cnCover = cnCover;
	}

	public Integer getCnType() {
		return cnType;
	}

	public void setCnType(Integer cnType) {
		this.cnType = cnType;
	}

	public Date getCnTime() {
		return cnTime;
	}

	public void setCnTime(Date cnTime) {
		this.cnTime = cnTime;
	}

	public Integer getCnCount() {
		return cnCount;
	}

	public void setCnCount(Integer cnCount) {
		this.cnCount = cnCount;
	}

	public String getCnContent() {
		return cnContent;
	}

	public void setCnContent(String cnContent) {
		this.cnContent = cnContent;
	}

	@Override
	protected Serializable pkVal() {
		return this.cnId;
	}

	@Override
	public String toString() {
		return "IrClassNews{" +
			", cnId=" + cnId +
			", cnTitle=" + cnTitle +
			", cnCover=" + cnCover +
			", cnType=" + cnType +
			", cnTime=" + cnTime +
			", cnCount=" + cnCount +
			", cnContent=" + cnContent +
			"}";
	}
}
