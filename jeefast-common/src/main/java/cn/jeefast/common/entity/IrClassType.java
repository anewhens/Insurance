package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-20
 */
@TableName("ir_class_type")
public class IrClassType extends Model<IrClassType> {

    private static final long serialVersionUID = 1L;

	@TableId(value="ict_id", type= IdType.AUTO)
	private Long ictId;
    /**
     * 超级课堂类别管理
     */
	@TableField("ict_title")
	private String ictTitle;
    /**
     * 背景图
     */
	@TableField("ict_url")
	private String ictUrl;

	@TableField("ict_slogan")
	private String ictSlogan;

	public String getIctSlogan() {
		return ictSlogan;
	}

	public void setIctSlogan(String ictSlogan) {
		this.ictSlogan = ictSlogan;
	}

	public Long getIctId() {
		return ictId;
	}

	public void setIctId(Long ictId) {
		this.ictId = ictId;
	}

	public String getIctTitle() {
		return ictTitle;
	}

	public void setIctTitle(String ictTitle) {
		this.ictTitle = ictTitle;
	}

	public String getIctUrl() {
		return ictUrl;
	}

	public void setIctUrl(String ictUrl) {
		this.ictUrl = ictUrl;
	}

	@Override
	protected Serializable pkVal() {
		return this.ictId;
	}

	@Override
	public String toString() {
		return "IrClassType{" +
			", ictId=" + ictId +
			", ictTitle=" + ictTitle +
			", ictUrl=" + ictUrl +
			"}";
	}
}
