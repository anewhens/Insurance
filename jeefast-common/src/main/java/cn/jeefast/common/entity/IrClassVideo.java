package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-24
 */
@TableName("ir_class_video")
public class IrClassVideo extends Model<IrClassVideo> {

    private static final long serialVersionUID = 1L;

	@TableId(value="cv_id", type= IdType.AUTO)
	private Long cvId;
    /**
     * 视频标题
     */
	@TableField("cv_title")
	private String cvTitle;
    /**
     * 视频时长
     */
	@TableField("cv_legth")
	private Double cvLegth;
    /**
     * 保险公司ID
     */
	@TableField("cp_id")
	private Long cpId;
    /**
     * 保险类别ID
     */
	@TableField("it_id")
	private Long itId;
    /**
     * 上传时间
     */
	@TableField("cv_up_time")
	private Date cvUpTime;
    /**
     * 所属省行
     */
	@TableField("cv_bank")
	private String cvBank;

	@TableField("cv_url")
	private String cvUrl;

	@TableField("cv_img")
	private String cvImg;

	public String getCvImg() {
		return cvImg;
	}

	public void setCvImg(String cvImg) {
		this.cvImg = cvImg;
	}

	public String getCvUrl() {
		return cvUrl;
	}

	public void setCvUrl(String cvUrl) {
		this.cvUrl = cvUrl;
	}

	public Long getCvId() {
		return cvId;
	}

	public void setCvId(Long cvId) {
		this.cvId = cvId;
	}

	public String getCvTitle() {
		return cvTitle;
	}

	public void setCvTitle(String cvTitle) {
		this.cvTitle = cvTitle;
	}

	public Double getCvLegth() {
		return cvLegth;
	}

	public void setCvLegth(Double cvLegth) {
		this.cvLegth = cvLegth;
	}

	public Long getCpId() {
		return cpId;
	}

	public void setCpId(Long cpId) {
		this.cpId = cpId;
	}

	public Long getItId() {
		return itId;
	}

	public void setItId(Long itId) {
		this.itId = itId;
	}

	public Date getCvUpTime() {
		return cvUpTime;
	}

	public void setCvUpTime(Date cvUpTime) {
		this.cvUpTime = cvUpTime;
	}

	public String getCvBank() {
		return cvBank;
	}

	public void setCvBank(String cvBank) {
		this.cvBank = cvBank;
	}

	@Override
	protected Serializable pkVal() {
		return this.cvId;
	}

	@Override
	public String toString() {
		return "IrClassVideo{" +
			", cvId=" + cvId +
			", cvTitle=" + cvTitle +
			", cvLegth=" + cvLegth +
			", cpId=" + cpId +
			", itId=" + itId +
			", cvUpTime=" + cvUpTime +
			", cvBank=" + cvBank +
			"}";
	}
}
