package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-24
 */
@TableName("ir_class_video_progress")
public class IrClassVideoProgress extends Model<IrClassVideoProgress> {

    private static final long serialVersionUID = 1L;

	@TableId(value="cvp_id", type= IdType.AUTO)
	private Long cvpId;
    /**
     * 视频ID
     */
	@TableField("cv_id")
	private Long cvId;
    /**
     * 用户ID
     */
	@TableField("user_id")
	private Long userId;
    /**
     * 0：未开始 1：学习中 2：已完成
     */
	@TableField("cvp_status")
	private Integer cvpStatus;

	/**
	 * 播放进度
	 */
	@TableField("cvp_progress")
	private String cvpProgress;

	@TableField("cvp_time")
	private String cvpTime;

	public String getCvpProgress() {
		return cvpProgress;
	}

	public String getCvpTime() {
		return cvpTime;
	}

	public void setCvpTime(String cvpTime) {
		this.cvpTime = cvpTime;
	}

	public void setCvpProgress(String cvpProgress) {
		this.cvpProgress = cvpProgress;
	}

	public Long getCvpId() {
		return cvpId;
	}

	public void setCvpId(Long cvpId) {
		this.cvpId = cvpId;
	}

	public Long getCvId() {
		return cvId;
	}

	public void setCvId(Long cvId) {
		this.cvId = cvId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getCvpStatus() {
		return cvpStatus;
	}

	public void setCvpStatus(Integer cvpStatus) {
		this.cvpStatus = cvpStatus;
	}

	@Override
	protected Serializable pkVal() {
		return this.cvpId;
	}

	@Override
	public String toString() {
		return "IrClassVideoProgress{" +
			", cvpId=" + cvpId +
			", cvId=" + cvId +
			", userId=" + userId +
			", cvpStatus=" + cvpStatus +
			"}";
	}
}
