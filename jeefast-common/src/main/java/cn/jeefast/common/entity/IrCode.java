package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
@TableName("ir_code")
public class IrCode extends Model<IrCode> {

    private static final long serialVersionUID = 1L;

	@TableId(value="code_id", type= IdType.AUTO)
	private Long codeId;
    /**
     * 验证码
     */
	@TableField("code_text")
	private String codeText;
    /**
     * 过期时间
     */
	@TableField("code_expir")
	private Date codeExpir;
    /**
     * 手机号
     */
	@TableField("code_phone")
	private String codePhone;
    /**
     * 0:未使用 1：已使用默认 0
     */
	@TableField("code_userd")
	private Integer codeUserd;


	public Long getCodeId() {
		return codeId;
	}

	public void setCodeId(Long codeId) {
		this.codeId = codeId;
	}

	public String getCodeText() {
		return codeText;
	}

	public void setCodeText(String codeText) {
		this.codeText = codeText;
	}

	public Date getCodeExpir() {
		return codeExpir;
	}

	public void setCodeExpir(Date codeExpir) {
		this.codeExpir = codeExpir;
	}

	public String getCodePhone() {
		return codePhone;
	}

	public void setCodePhone(String codePhone) {
		this.codePhone = codePhone;
	}

	public Integer getCodeUserd() {
		return codeUserd;
	}

	public void setCodeUserd(Integer codeUserd) {
		this.codeUserd = codeUserd;
	}

	@Override
	protected Serializable pkVal() {
		return this.codeId;
	}

	@Override
	public String toString() {
		return "IrCode{" +
			", codeId=" + codeId +
			", codeText=" + codeText +
			", codeExpir=" + codeExpir +
			", codePhone=" + codePhone +
			", codeUserd=" + codeUserd +
			"}";
	}
}
