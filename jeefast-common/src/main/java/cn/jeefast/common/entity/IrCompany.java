package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-20
 */
@TableName("ir_company")
public class IrCompany extends Model<IrCompany> {

    private static final long serialVersionUID = 1L;

	@TableId(value="ic_id", type= IdType.AUTO)
	private Long icId;
    /**
     * 保险公司名称
     */
	@TableField("ic_title")
	private String icTitle;
    /**
     * 0:不推荐  1：推荐
     */
	@TableField("ic_recommend")
	private Integer icRecommend;

	@TableField(exist = false)
	private Integer index;


	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Long getIcId() {
		return icId;
	}

	public void setIcId(Long icId) {
		this.icId = icId;
	}

	public String getIcTitle() {
		return icTitle;
	}

	public void setIcTitle(String icTitle) {
		this.icTitle = icTitle;
	}

	public Integer getIcRecommend() {
		return icRecommend;
	}

	public void setIcRecommend(Integer icRecommend) {
		this.icRecommend = icRecommend;
	}

	@Override
	protected Serializable pkVal() {
		return this.icId;
	}

	@Override
	public String toString() {
		return "IrCompany{" +
			", icId=" + icId +
			", icTitle=" + icTitle +
			", icRecommend=" + icRecommend +
			"}";
	}
}
