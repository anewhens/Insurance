package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
@TableName("ir_dict")
public class IrDict extends Model<IrDict> {

    private static final long serialVersionUID = 1L;

	@TableId(value="dict_id", type= IdType.AUTO)
	private Long dictId;
    /**
     * 字典值
     */
	@TableField("dict_value")
	private String dictValue;

	/**
	 * 字典键
	 */
	@TableField("dict_key")
	private String dictKey;

	/**
	 * 备注
	 */
	@TableField("remark")
	private String remark;


	public Long getDictId() {
		return dictId;
	}

	public void setDictId(Long dictId) {
		this.dictId = dictId;
	}

	public String getDictValue() {
		return dictValue;
	}

	public void setDictValue(String dictValue) {
		this.dictValue = dictValue;
	}

	public String getDictKey() {
		return dictKey;
	}

	public void setDictKey(String dictKey) {
		this.dictKey = dictKey;
	}

	@Override
	protected Serializable pkVal() {
		return this.dictId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "IrDict{" +
			", dictId=" + dictId +
			", dictValue=" + dictValue +
			"}";
	}
}
