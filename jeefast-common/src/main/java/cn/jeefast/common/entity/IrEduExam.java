package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-18
 */
@TableName("ir_edu_exam")
public class IrEduExam extends Model<IrEduExam> {

    private static final long serialVersionUID = 1L;

    /**
     * 考核ID
     */
    @TableId("ee_id")
	private Long eeId;
    /**
     * 批次ID
     */
	@TableField("et_id")
	private Long etId;
    /**
     * 视频地址
     */
	@TableField("ee_vider")
	private String eeVider;
    /**
     * 投连考试 0：否 1：是
     */
	@TableField("ee_is_dt")
	private Integer eeIsDt;
	@TableField("edu_type")
	private Integer eduType;
    /**
     * 题目JSON串  {"questions":[ {"title":"唐僧不是唐三藏对吗？","score":5,"correct":"0","multiple":false,"answers":[ {"answer":"是"},{"answer":"否"} ]} ]}
     */
	@TableField("ee_questions")
	private String eeQuestions;

	/**
	 * 分数
	 */
	@TableField("ee_score")
	private Integer eeScore;

	@TableField("ee_img")
	private String eeImg;

	@TableField("ee_title")
	private String eeTitle;

	@TableField("ee_time")
	private Date eeTime;

	@TableField(exist = false)
	private String typeName;

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getEeImg() {
		return eeImg;
	}

	public void setEeImg(String eeImg) {
		this.eeImg = eeImg;
	}

	public String getEeTitle() {
		return eeTitle;
	}

	public void setEeTitle(String eeTitle) {
		this.eeTitle = eeTitle;
	}

	public Date getEeTime() {
		return eeTime;
	}

	public void setEeTime(Date eeTime) {
		this.eeTime = eeTime;
	}

	public Long getEeId() {
		return eeId;
	}

	public void setEeId(Long eeId) {
		this.eeId = eeId;
	}

	public Long getEtId() {
		return etId;
	}

	public void setEtId(Long etId) {
		this.etId = etId;
	}

	public String getEeVider() {
		return eeVider;
	}

	public void setEeVider(String eeVider) {
		this.eeVider = eeVider;
	}

	public Integer getEeIsDt() {
		return eeIsDt;
	}

	public void setEeIsDt(Integer eeIsDt) {
		this.eeIsDt = eeIsDt;
	}

	public Integer getEduType() {
		return eduType;
	}

	public void setEduType(Integer eduType) {
		this.eduType = eduType;
	}

	public String getEeQuestions() {
		return eeQuestions;
	}

	public void setEeQuestions(String eeQuestions) {
		this.eeQuestions = eeQuestions;
	}

	public Integer getEeScore() {
		return eeScore;
	}

	public void setEeScore(Integer eeScore) {
		this.eeScore = eeScore;
	}

	@Override
	protected Serializable pkVal() {
		return this.eeId;
	}

	@Override
	public String toString() {
		return "IrEduExam{" +
			", eeId=" + eeId +
			", etId=" + etId +
			", eeVider=" + eeVider +
			", eeIsDt=" + eeIsDt +
			", eduType=" + eduType +
			", eeQuestions=" + eeQuestions +
			"}";
	}
}
