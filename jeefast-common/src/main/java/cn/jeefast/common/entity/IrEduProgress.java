package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-18
 */
@TableName("ir_edu_progress")
public class IrEduProgress extends Model<IrEduProgress> {

    private static final long serialVersionUID = 1L;

	@TableId(value="ep_id", type= IdType.AUTO)
	private Long epId;
    /**
     * 用户ID
     */
	@TableField("user_id")
	private Long userId;
    /**
     * 批次ID
     */
	@TableField("et_id")
	private Long etId;
    /**
     * 题目ID
     */
	@TableField("ee_id")
	private Long eeId;
    /**
     * 最新的答题时间
     */
	@TableField("ep_time")
	private Date epTime;
    /**
     * 播放进度
     */
	@TableField("ep_video_progress")
	private String epVideoProgress;
    /**
     * 答题分数
     */
	@TableField("ep_score")
	private Integer epScore;
    /**
     * 0:可以重做  1:不可以重做
     */
	@TableField("ep_re_exam")
	private Integer epReExam;


	public Long getEpId() {
		return epId;
	}

	public void setEpId(Long epId) {
		this.epId = epId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getEtId() {
		return etId;
	}

	public void setEtId(Long etId) {
		this.etId = etId;
	}

	public Long getEeId() {
		return eeId;
	}

	public void setEeId(Long eeId) {
		this.eeId = eeId;
	}

	public Date getEpTime() {
		return epTime;
	}

	public void setEpTime(Date epTime) {
		this.epTime = epTime;
	}

	public String getEpVideoProgress() {
		return epVideoProgress;
	}

	public void setEpVideoProgress(String epVideoProgress) {
		this.epVideoProgress = epVideoProgress;
	}

	public Integer getEpScore() {
		return epScore;
	}

	public void setEpScore(Integer epScore) {
		this.epScore = epScore;
	}

	public Integer getEpReExam() {
		return epReExam;
	}

	public void setEpReExam(Integer epReExam) {
		this.epReExam = epReExam;
	}

	@Override
	protected Serializable pkVal() {
		return this.epId;
	}

	@Override
	public String toString() {
		return "IrEduProgress{" +
			", epId=" + epId +
			", userId=" + userId +
			", etId=" + etId +
			", eeId=" + eeId +
			", epTime=" + epTime +
			", epVideoProgress=" + epVideoProgress +
			", epScore=" + epScore +
			", epReExam=" + epReExam +
			"}";
	}
}
