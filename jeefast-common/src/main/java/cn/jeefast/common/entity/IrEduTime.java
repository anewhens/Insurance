package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-17
 */
@TableName("ir_edu_time")
public class IrEduTime extends Model<IrEduTime> {

    private static final long serialVersionUID = 1L;

	@TableId(value="et_id", type= IdType.AUTO)
	private Long etId;
    /**
     * 报名开始时间
     */
	@TableField("et_sign_start")
	private Date etSignStart;
    /**
     * 报名结束时间
     */
	@TableField("et_sign_end")
	private Date etSignEnd;
    /**
     * 考试开始时间
     */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField("et_exam_start")
	private Date etExamStart;
    /**
     * 考试结束时间
     */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField("et_exam_end")
	private Date etExamEnd;

	@TableField("et_comm_score")
	private Integer etCommScore;

	@TableField("et_dt_score")
	private Integer etDtScore;


	public Long getEtId() {
		return etId;
	}

	public void setEtId(Long etId) {
		this.etId = etId;
	}

	public Date getEtSignStart() {
		return etSignStart;
	}

	public void setEtSignStart(Date etSignStart) {
		this.etSignStart = etSignStart;
	}

	public Date getEtSignEnd() {
		return etSignEnd;
	}

	public void setEtSignEnd(Date etSignEnd) {
		this.etSignEnd = etSignEnd;
	}

	public Date getEtExamStart() {
		return etExamStart;
	}

	public void setEtExamStart(Date etExamStart) {
		this.etExamStart = etExamStart;
	}

	public Date getEtExamEnd() {
		return etExamEnd;
	}

	public void setEtExamEnd(Date etExamEnd) {
		this.etExamEnd = etExamEnd;
	}

	public Integer getEtCommScore() {
		return etCommScore;
	}

	public void setEtCommScore(Integer etCommScore) {
		this.etCommScore = etCommScore;
	}

	public Integer getEtDtScore() {
		return etDtScore;
	}

	public void setEtDtScore(Integer etDtScore) {
		this.etDtScore = etDtScore;
	}

	@Override
	protected Serializable pkVal() {
		return this.etId;
	}

	@Override
	public String toString() {
		return "IrEduTime{" +
			", etId=" + etId +
			", etSignStart=" + etSignStart +
			", etSignEnd=" + etSignEnd +
			", etExamStart=" + etExamStart +
			", etExamEnd=" + etExamEnd +
			"}";
	}
}
