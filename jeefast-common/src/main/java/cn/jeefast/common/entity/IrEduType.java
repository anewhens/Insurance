package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-18
 */
@TableName("ir_edu_type")
public class IrEduType extends Model<IrEduType> {

    private static final long serialVersionUID = 1L;

	@TableId(value="etype_id", type= IdType.AUTO)
	private Long etypeId;
    /**
     * 类别
     */
	@TableField("etype_title")
	private String etypeTitle;


	public Long getEtypeId() {
		return etypeId;
	}

	public void setEtypeId(Long etypeId) {
		this.etypeId = etypeId;
	}

	public String getEtypeTitle() {
		return etypeTitle;
	}

	public void setEtypeTitle(String etypeTitle) {
		this.etypeTitle = etypeTitle;
	}

	@Override
	protected Serializable pkVal() {
		return this.etypeId;
	}

	@Override
	public String toString() {
		return "IrEduType{" +
			", etypeId=" + etypeId +
			", etypeTitle=" + etypeTitle +
			"}";
	}
}
