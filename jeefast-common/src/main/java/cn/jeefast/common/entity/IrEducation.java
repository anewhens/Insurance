package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-16
 */
@TableName("ir_education")
@ApiModel(value = "education", description = "教育")
public class IrEducation extends Model<IrEducation> {

    private static final long serialVersionUID = 1L;

	@TableId(value="ie_id", type= IdType.AUTO)
	private Long ieId;
    /**
     * 性别
     */
	@TableField("ie_sex")
	@ApiModelProperty(name="ieSex", value="性别 0：女 1：男")
	private Integer ieSex;
    /**
     * 省行
     */
	@TableField("ie_province_bank")
	@ApiModelProperty(name="ieProvinceBank", value="省行")
	private Long ieProvinceBank;
    /**
     * 市行
     */
	@TableField("ie_city_bank")
	@ApiModelProperty(name="ieCityBank", value="市行")
	private Long ieCityBank;
    /**
     * 支行
     */
	@TableField("ie_sub_bank")
	@ApiModelProperty(name="ieSubBank", value="支行")
	private String ieSubBank;
    /**
     * 网点
     */
	@TableField("ie_location")
	@ApiModelProperty(name="ieLocation", value="网点")
	private String ieLocation;
    /**
     * 是否参与投连考试 0：否 1：是
     */
	@TableField("ie_is_dt")
	@ApiModelProperty(name="ieIsDt", value="是否参与投连考试 0：否 1：是")
	private Integer ieIsDt;
    /**
     * 手机
     */
	@TableField("ie_mobile")
	@ApiModelProperty(name="ieMobile", value="联系电话")
	private String ieMobile;

	@TableField("et_id")
	@ApiModelProperty(name="etId", value="批次ID")
	private Long etId;

	@TableField("user_id")
	@ApiModelProperty(name="userId", value="用户ID")
	private Long userId;

	@TableField("user_id_card")
	private String userIdCard;

	@TableField("user_name")
	private String userName;

	@Override
	protected Serializable pkVal() {
		return null;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Long getIeId() {
		return ieId;
	}

	public void setIeId(Long ieId) {
		this.ieId = ieId;
	}

	public Integer getIeSex() {
		return ieSex;
	}

	public void setIeSex(Integer ieSex) {
		this.ieSex = ieSex;
	}

	public Long getIeProvinceBank() {
		return ieProvinceBank;
	}

	public void setIeProvinceBank(Long ieProvinceBank) {
		this.ieProvinceBank = ieProvinceBank;
	}

	public Long getIeCityBank() {
		return ieCityBank;
	}

	public void setIeCityBank(Long ieCityBank) {
		this.ieCityBank = ieCityBank;
	}

	public String getIeSubBank() {
		return ieSubBank;
	}

	public void setIeSubBank(String ieSubBank) {
		this.ieSubBank = ieSubBank;
	}

	public String getIeLocation() {
		return ieLocation;
	}

	public void setIeLocation(String ieLocation) {
		this.ieLocation = ieLocation;
	}

	public Integer getIeIsDt() {
		return ieIsDt;
	}

	public void setIeIsDt(Integer ieIsDt) {
		this.ieIsDt = ieIsDt;
	}

	public String getIeMobile() {
		return ieMobile;
	}

	public void setIeMobile(String ieMobile) {
		this.ieMobile = ieMobile;
	}

	public Long getEtId() {
		return etId;
	}

	public void setEtId(Long etId) {
		this.etId = etId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserIdCard() {
		return userIdCard;
	}

	public void setUserIdCard(String userIdCard) {
		this.userIdCard = userIdCard;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "IrEducation{" +
				"ieId=" + ieId +
				", ieSex=" + ieSex +
				", ieProvinceBank=" + ieProvinceBank +
				", ieCityBank=" + ieCityBank +
				", ieSubBank='" + ieSubBank + '\'' +
				", ieLocation='" + ieLocation + '\'' +
				", ieIsDt=" + ieIsDt +
				", ieMobile='" + ieMobile + '\'' +
				'}';
	}
}
