package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
@ApiModel(value = "feedback", description = "用户反馈")
@TableName("ir_feedback")
public class IrFeedback extends Model<IrFeedback> {

    private static final long serialVersionUID = 1L;

	@TableId(value="fb_id", type= IdType.AUTO)
	private Long fbId;
    /**
     * 反馈内容
     */
	@TableField("fb_content")
	@ApiModelProperty(name="fbContent", value="反馈的内容")
	private String fbContent;
    /**
     * 联系电话
     */
	@TableField("fb_phone")
	@ApiModelProperty(name="fbPhone", value="联系电话（选填）")
	private String fbPhone;


	public Long getFbId() {
		return fbId;
	}

	public void setFbId(Long fbId) {
		this.fbId = fbId;
	}

	public String getFbContent() {
		return fbContent;
	}

	public void setFbContent(String fbContent) {
		this.fbContent = fbContent;
	}

	public String getFbPhone() {
		return fbPhone;
	}

	public void setFbPhone(String fbPhone) {
		this.fbPhone = fbPhone;
	}

	@Override
	protected Serializable pkVal() {
		return this.fbId;
	}

	@Override
	public String toString() {
		return "IrFeedback{" +
			", fbId=" + fbId +
			", fbContent=" + fbContent +
			", fbPhone=" + fbPhone +
			"}";
	}
}
