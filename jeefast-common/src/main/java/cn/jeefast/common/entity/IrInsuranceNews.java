package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-03-09
 */
@TableName("ir_insurance_news")
public class IrInsuranceNews extends Model<IrInsuranceNews> {

    private static final long serialVersionUID = 1L;

	@TableId(value="iin_id", type= IdType.AUTO)
	private Long iinId;
    /**
     * 标题
     */
	@TableField("iin_title")
	private String iinTitle;
    /**
     * 内容
     */
	@TableField("iin_content")
	private String iinContent;
    /**
     * 封面
     */
	@TableField("iin_cover")
	private String iinCover;
    /**
     * 发布时间
     */
	@TableField("iin_time")
	private Date iinTime;


	public Long getIinId() {
		return iinId;
	}

	public void setIinId(Long iinId) {
		this.iinId = iinId;
	}

	public String getIinTitle() {
		return iinTitle;
	}

	public void setIinTitle(String iinTitle) {
		this.iinTitle = iinTitle;
	}

	public String getIinContent() {
		return iinContent;
	}

	public void setIinContent(String iinContent) {
		this.iinContent = iinContent;
	}

	public String getIinCover() {
		return iinCover;
	}

	public void setIinCover(String iinCover) {
		this.iinCover = iinCover;
	}

	public Date getIinTime() {
		return iinTime;
	}

	public void setIinTime(Date iinTime) {
		this.iinTime = iinTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.iinId;
	}

	@Override
	public String toString() {
		return "IrInsuranceNews{" +
			", iinId=" + iinId +
			", iinTitle=" + iinTitle +
			", iinContent=" + iinContent +
			", iinCover=" + iinCover +
			", iinTime=" + iinTime +
			"}";
	}
}
