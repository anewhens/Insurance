package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-20
 */
@TableName("ir_insurance_type")
public class IrInsuranceType extends Model<IrInsuranceType> {

    private static final long serialVersionUID = 1L;

	@TableId(value="iit_id", type= IdType.AUTO)
	private Long iitId;
    /**
     * 保险种类
     */
	@TableField("iit_title")
	private String iitTitle;


	public Long getIitId() {
		return iitId;
	}

	public void setIitId(Long iitId) {
		this.iitId = iitId;
	}

	public String getIitTitle() {
		return iitTitle;
	}

	public void setIitTitle(String iitTitle) {
		this.iitTitle = iitTitle;
	}

	@Override
	protected Serializable pkVal() {
		return this.iitId;
	}

	@Override
	public String toString() {
		return "IrInsuranceType{" +
			", iitId=" + iitId +
			", iitTitle=" + iitTitle +
			"}";
	}
}
