package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
@TableName("ir_low")
public class IrLow extends Model<IrLow> {

    private static final long serialVersionUID = 1L;

	@TableId(value="low_id", type= IdType.AUTO)
	private Long lowId;
    /**
     * 标题
     */
	@TableField("low_title")
	private String lowTitle;
    /**
     * 创建时间
     */
	@TableField("low_time")
	private Date lowTime;
    /**
     * 内容
     */
	@TableField("low_content")
	private String lowContent;


	public Long getLowId() {
		return lowId;
	}

	public void setLowId(Long lowId) {
		this.lowId = lowId;
	}

	public String getLowTitle() {
		return lowTitle;
	}

	public void setLowTitle(String lowTitle) {
		this.lowTitle = lowTitle;
	}

	public Date getLowTime() {
		return lowTime;
	}

	public void setLowTime(Date lowTime) {
		this.lowTime = lowTime;
	}

	public String getLowContent() {
		return lowContent;
	}

	public void setLowContent(String lowContent) {
		this.lowContent = lowContent;
	}

	@Override
	protected Serializable pkVal() {
		return this.lowId;
	}

	@Override
	public String toString() {
		return "IrLow{" +
			", lowId=" + lowId +
			", lowTitle=" + lowTitle +
			", lowTime=" + lowTime +
			", lowContent=" + lowContent +
			"}";
	}
}
