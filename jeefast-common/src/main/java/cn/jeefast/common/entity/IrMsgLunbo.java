package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 首页消息轮播
 * </p>
 *
 * @author theodo
 * @since 2020-02-16
 */
@TableName("ir_msg_lunbo")
public class IrMsgLunbo extends Model<IrMsgLunbo> {

    private static final long serialVersionUID = 1L;

	@TableId(value="ml_id", type= IdType.AUTO)
	private Long mlId;
    /**
     * 轮播标题
     */
	@TableField("ml_title")
	private String mlTitle;
    /**
     * 富文本内容
     */
	@TableField("ml_content")
	private String mlContent;
	/**
     * 缩略图
     */
	@TableField("ml_img")
	private String mlImg;

	@TableField("ml_time")
	private Date mlTime;

	public String getMlImg() {
		return mlImg;
	}

	public void setMlImg(String mlImg) {
		this.mlImg = mlImg;
	}

	public Long getMlId() {
		return mlId;
	}

	public void setMlId(Long mlId) {
		this.mlId = mlId;
	}

	public String getMlTitle() {
		return mlTitle;
	}

	public void setMlTitle(String mlTitle) {
		this.mlTitle = mlTitle;
	}

	public String getMlContent() {
		return mlContent;
	}

	public void setMlContent(String mlContent) {
		this.mlContent = mlContent;
	}

	public Date getMlTime() {
		return mlTime;
	}

	public void setMlTime(Date mlTime) {
		this.mlTime = mlTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.mlId;
	}

	@Override
	public String toString() {
		return "IrMsgLunbo{" +
			", mlId=" + mlId +
			", mlTitle=" + mlTitle +
			", mlContent=" + mlContent +
			"}";
	}
}
