package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
@TableName("ir_news")
public class IrNews extends Model<IrNews> {

    private static final long serialVersionUID = 1L;

	@TableId(value="news_id", type= IdType.AUTO)
	private Long newsId;
    /**
     * 新闻标题
     */
	@TableField("news_title")
	private String newsTitle;
    /**
     * 发布时间
     */
	@TableField("news_time")
	private Date newsTime;
    /**
     * 新闻内容
     */
	@TableField("news_content")
	private String newsContent;

	@TableField("news_img")
	private String newsImg;


	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getNewsTitle() {
		return newsTitle;
	}

	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}

	public Date getNewsTime() {
		return newsTime;
	}

	public void setNewsTime(Date newsTime) {
		this.newsTime = newsTime;
	}

	public String getNewsContent() {
		return newsContent;
	}

	public void setNewsContent(String newsContent) {
		this.newsContent = newsContent;
	}

	public String getNewsImg() {
		return newsImg;
	}

	public void setNewsImg(String newsImg) {
		this.newsImg = newsImg;
	}

	@Override
	protected Serializable pkVal() {
		return this.newsId;
	}

	@Override
	public String toString() {
		return "IrNews{" +
			", newsId=" + newsId +
			", newsTitle=" + newsTitle +
			", newTime=" + newsTime +
			", newContent=" + newsContent +
			"}";
	}
}
