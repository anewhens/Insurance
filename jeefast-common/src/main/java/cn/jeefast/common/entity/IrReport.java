package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-22
 */
@TableName("ir_report")
public class IrReport extends Model<IrReport> {

    private static final long serialVersionUID = 1L;

	@TableId(value="irr_id", type= IdType.AUTO)
	private Long irrId;
    /**
     * 名称
     */
	@TableField("irr_title")
	private String irrTitle;
    /**
     * 上传者
     */
	@TableField("irr_uploader")
	private String irrUploader;
	/**
     * 文件路径
     */
	@TableField("irr_path")
	private String irrPath;
    /**
     * 文件大小
     */
	@TableField("irr_size")
	private String irrSize;
    /**
     * 上传时间
     */
	@TableField("irr_time")
	private Date irrTime;
	/**
     * 关联得到已上传的总数
     */
	@TableField(exist = false)
	private String number;

	public String getIrrPath() {
		return irrPath;
	}

	public void setIrrPath(String irrPath) {
		this.irrPath = irrPath;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Long getIrrId() {
		return irrId;
	}

	public void setIrrId(Long irrId) {
		this.irrId = irrId;
	}

	public String getIrrTitle() {
		return irrTitle;
	}

	public void setIrrTitle(String irrTitle) {
		this.irrTitle = irrTitle;
	}

	public String getIrrUploader() {
		return irrUploader;
	}

	public void setIrrUploader(String irrUploader) {
		this.irrUploader = irrUploader;
	}

	public String getIrrSize() {
		return irrSize;
	}

	public void setIrrSize(String irrSize) {
		this.irrSize = irrSize;
	}

	public Date getIrrTime() {
		return irrTime;
	}

	public void setIrrTime(Date irrTime) {
		this.irrTime = irrTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.irrId;
	}

	@Override
	public String toString() {
		return "IrReport{" +
			", irrId=" + irrId +
			", irrTitle=" + irrTitle +
			", irrUploader=" + irrUploader +
			", irrSize=" + irrSize +
			", irrTime=" + irrTime +
			"}";
	}
}
