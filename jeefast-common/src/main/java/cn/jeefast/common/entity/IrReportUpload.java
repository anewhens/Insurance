package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 模板上传记录表
 * </p>
 *
 * @author theodo
 * @since 2020-03-12
 */
@TableName("ir_report_upload")
public class IrReportUpload extends Model<IrReportUpload> {

    private static final long serialVersionUID = 1L;

    /**
     * 上传的id
     */
	@TableId(value="upload_id", type= IdType.AUTO)
	private Long uploadId;
    /**
     * 上传的模板id
     */
	@TableField("upload_irr_id")
	private Long uploadIrrId;
    /**
     * 上传的用户id
     */
	@TableField("upload_user_id")
	private Long uploadUserId;
    /**
     * 上传后保存的路径
     */
	@TableField("upload_path")
	private String uploadPath;
    /**
     * 创建时间
     */
	@TableField("upload_creation_time")
	private Date uploadCreationTime;


	public Long getUploadId() {
		return uploadId;
	}

	public void setUploadId(Long uploadId) {
		this.uploadId = uploadId;
	}

	public Long getUploadIrrId() {
		return uploadIrrId;
	}

	public void setUploadIrrId(Long uploadIrrId) {
		this.uploadIrrId = uploadIrrId;
	}

	public Long getUploadUserId() {
		return uploadUserId;
	}

	public void setUploadUserId(Long uploadUserId) {
		this.uploadUserId = uploadUserId;
	}

	public String getUploadPath() {
		return uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public Date getUploadCreationTime() {
		return uploadCreationTime;
	}

	public void setUploadCreationTime(Date uploadCreationTime) {
		this.uploadCreationTime = uploadCreationTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.uploadId;
	}

	@Override
	public String toString() {
		return "IrReportUpload{" +
			", uploadId=" + uploadId +
			", uploadIrrId=" + uploadIrrId +
			", uploadUserId=" + uploadUserId +
			", uploadPath=" + uploadPath +
			", uploadCreationTime=" + uploadCreationTime +
			"}";
	}
}
