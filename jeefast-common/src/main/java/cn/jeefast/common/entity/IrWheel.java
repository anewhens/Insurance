package cn.jeefast.common.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
@TableName("ir_wheel")
public class IrWheel extends Model<IrWheel> {

    private static final long serialVersionUID = 1L;

	@TableId(value="wheel_id", type= IdType.AUTO)
	private Long wheelId;
    /**
     * 1:首页轮播图 2：监管政策法规轮播图 3：保险超级课堂轮播图  4：保险公司热销产品轮播图
     */
	@TableField("wheel_tyle")
	private Integer wheelTyle;
    /**
     * 轮播图名称
     */
	@TableField("wheel_title")
	private String wheelTitle;
    /**
     * 轮播图图片
     */
	@TableField("wheel_url")
	private String wheelUrl;
    /**
     * 排序
     */
	@TableField("wheel_index")
	private Integer wheelIndex;
    /**
     * 链接地址
     */
	@TableField("wheel_link")
	private String wheelLink;


	public Long getWheelId() {
		return wheelId;
	}

	public void setWheelId(Long wheelId) {
		this.wheelId = wheelId;
	}

	public Integer getWheelTyle() {
		return wheelTyle;
	}

	public void setWheelTyle(Integer wheelTyle) {
		this.wheelTyle = wheelTyle;
	}

	public String getWheelTitle() {
		return wheelTitle;
	}

	public void setWheelTitle(String wheelTitle) {
		this.wheelTitle = wheelTitle;
	}

	public String getWheelUrl() {
		return wheelUrl;
	}

	public void setWheelUrl(String wheelUrl) {
		this.wheelUrl = wheelUrl;
	}

	public Integer getWheelIndex() {
		return wheelIndex;
	}

	public void setWheelIndex(Integer wheelIndex) {
		this.wheelIndex = wheelIndex;
	}

	public String getWheelLink() {
		return wheelLink;
	}

	public void setWheelLink(String wheelLink) {
		this.wheelLink = wheelLink;
	}

	@Override
	protected Serializable pkVal() {
		return this.wheelId;
	}

	@Override
	public String toString() {
		return "IrWheel{" +
			", wheelId=" + wheelId +
			", wheelTyle=" + wheelTyle +
			", wheelTitle=" + wheelTitle +
			", wheelUrl=" + wheelUrl +
			", wheelIndex=" + wheelIndex +
			", wheelLink=" + wheelLink +
			"}";
	}
}
