package cn.jeefast.common.excel;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * ExcelInfo
 *
 * @author WY
 * @date 2018/12/12 0012
 */
@Data
@AllArgsConstructor
public class ExcelInfo {
  /**标题*/
  private String title;
  /**导出名*/
  private String exportFileName;
  /**模板名*/
  private String templateFileName;
  /**实例完整类名*/
  private String beanClazz;
  /**
   * 数据列表
   */
  private List datas = new ArrayList();

}
