package cn.jeefast.common.excel;

import cn.hutool.core.io.resource.ResourceUtil;

import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Excel工具类
 * 
 * @author theodo
 * @email 36780272@qq.com
 * @date 2017年11月26日 上午10:40:10
 */
public class TemplateFileUtil {

    public static InputStream getTemplates(String tempName) throws FileNotFoundException {
        return ResourceUtil.getStream("excel-templates/"+tempName);
    }
}
