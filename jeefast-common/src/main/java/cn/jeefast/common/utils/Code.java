package cn.jeefast.common.utils;

/**
 * @Description 错误码表.
 * @Author ZHAIZHAI
 * @Version 1.0
 */
public enum Code {
    // 1024以内, 同HTTP
    SUCCESS("0", "成功"),
    NOT_FOUND("404", "未找见"),
    PUB_PARAM_ERROR("6000", "非法参数"),
    PUB_PARAM_WRONG("6001", "参数错误"),
    PUB_SERVICE_EXCEPTION("9000","服务异常"),
    NOT_FOUND_USER("10001", "没有这个用户"),
    NOT_FOUND_DOCKER("10002", "没有这个医生"),
    ;

    private String code;
    private String message;

    Code(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }


}
