package cn.jeefast.common.utils;

import java.math.BigDecimal;

public class DoubleUtils {
    /**
     * @Title: 保留两位小数
     * @author: lzl
     * @Description:
     */
    public static String getDouble(Double d){
        if(d==null){
            return "0.0";
        }
        BigDecimal b = new BigDecimal(d);
        double f1 = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        return f1+"";
    }
    /**
     * @Title: 保留两位小数
     * @author: lzl
     * @Description:
     */
    public static Double getDouble2(Double d){
        if(d==null){
            return 0.0;
        }
        BigDecimal b = new BigDecimal(d);
        double f1 = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        return f1;
    }
}
