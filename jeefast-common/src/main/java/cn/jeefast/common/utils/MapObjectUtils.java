/**
 * Copyright ? 2018河北优枫网络科技有限公司. All rights reserved.
 *
 * @Title: MapUtils.java
 * @Prject:
 * @Package: package com.jy.rest.util
 * @Description: TODO
 * @author: lzl
 * @date: 10:28 2019/11/18 0018
 * @version: V1.0
 */
package cn.jeefast.common.utils;

import io.swagger.models.auth.In;

public class MapObjectUtils {
    public static String getObject(Object object){
        if(object==null){
            return "";
        }
        return object.toString();
    }
    public static Integer getObject2(Object object){
        if(object==null){
            return 0;
        }
        return Integer.parseInt(object.toString());
    }
}