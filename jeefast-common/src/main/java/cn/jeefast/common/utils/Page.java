package cn.jeefast.common.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Page", description = "通用分页信息")
public class Page {
	
	//数据总量
	@ApiModelProperty(name="dataTotal", value="查询数据总量")
	private Long dataTotal;
	
	//页码
	@ApiModelProperty(name="dataTotal", value="页码")
	private Integer pageNo;
	
	//页面容量
	@ApiModelProperty(name="dataTotal", value="页面容量")
	private Integer pageSize;
	
	public Page() {}
	
	public Page(Long dataTotal, Integer pageNo, Integer pageSize) {
		this.dataTotal = dataTotal;
		this.pageNo = pageNo;
		this.pageSize = pageSize;
	}
}
