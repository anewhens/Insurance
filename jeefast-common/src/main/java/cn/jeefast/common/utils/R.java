package cn.jeefast.common.utils;

import io.swagger.annotations.ApiModel;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 返回数据
 * 
 * @author theodo
 * @email 36780272@qq.com
 * @date 2017年10月27日 下午9:59:27
 */
@ApiModel(value = "R", description = "通用返回信息")
public class R extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;
	
	public R() {
		put("code", 0);
	}
	
	public static R error() {
		Long errId = new Date().getTime();
		System.out.println("异常："+errId+"结束");
		return error(500, "未知异常，请联系管理员，错误编号："+errId);
	}
	
	public static R error(String msg) {
		Long errId = new Date().getTime();
		System.out.println("异常："+errId+"结束");
		msg = msg + "错误编号："+errId;
		return error(500, msg);
	}
	
	public static R error(int code, String msg) {
		R r = new R();
		r.put("code", code);
		r.put("msg", msg);
		return r;
	}

	public static R ok(String msg) {
		R r = new R();
		r.put("msg", msg);
		return r;
	}
	
	public static R ok(Map<String, Object> map) {
		R r = new R();
		r.putAll(map);
		return r;
	}
	
	public static R ok() {
		return new R();
	}

	public R put(String key, Object value) {
		super.put(key, value);
		return this;
	}


}
