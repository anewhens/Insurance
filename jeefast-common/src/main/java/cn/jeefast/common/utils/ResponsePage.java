package cn.jeefast.common.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 接口返回值格式定义.
 * <p>
 * 接口返回均为JSON格式, 形式为: {code: XXX, message:, YYY, data: ZZZ}
 * 其中code为错误码, message为简单的错误描述, data为请求成功时返回的所需数据.
 * 注意: message不能作为前端显示错误的凭据, 该值仅为开发调试使用.
 * <p>
 * Created by xingfinal on 15/11/27.
 */
@ApiModel(value = "ResponsePage", description = "通用返回状态信息")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponsePage<T, T2> {
	
	@ApiModelProperty(name="code", value="状态码，\"0\"表示成功，其他值都表示失败。")
    private String code;
	
	@ApiModelProperty(name="msg", value="返回提示信息，可以作为前端显示，也可以作为前端开发人员参考信息。")
    private String msg;
	
	@ApiModelProperty(name="page", value="分页信息。")
    private T2 page;
	
	@ApiModelProperty(name="data", value="具体返回数据。")
    private T data;

    public static <T> ResponsePage<T, T> ok() {
        return ok(Code.SUCCESS);
    }

    public static <T> ResponsePage<T, T> ok(Code code) {
        return ok(code, null);
    }
    
    public static <T> ResponsePage<T, T> ok(Code code, Page page) {
        return ok(code, null);
    }

    public static <T> ResponsePage<T, T> ok(T data) {
        return ok(Code.SUCCESS, data);
    }
    
    public static <T> ResponsePage<T, T> ok(T data, Page page) {
    	
        return ok(data, page);
    }

    public static <T> ResponsePage<T, T> ok(Code code, T data) {
        return new ResponsePage<>(code, data);
    }

    public static <T> ResponsePage<T, T> error(Code code) {
        return ok(code);
    }

    public ResponsePage() {
    }

    public ResponsePage(Code code) {
        this(code.getCode(), code.getMessage(), null);
    }
    
    public ResponsePage(Code code, T2 page) {
    	 this.code = code.getCode();
         this.page = page;
    }
    
    public ResponsePage(Code code, T data, T2 page) {
        this(code.getCode(), code.getMessage(), data, page);
    }
    
    public ResponsePage(String code, String message) {
        this.code = code;
        this.msg = message;
    }

    public ResponsePage(String code, String message, T data) {
        this.code = code;
        this.msg = message;
        this.data = data;
    }
    
    public ResponsePage(T data, T2 page) {
    	this.code = Code.SUCCESS.getCode();
    	this.msg = Code.SUCCESS.getMessage();
        this.data = data;
        this.page = page;
    }
    
    public ResponsePage(String code, T data, T2 page) {
        this.code = code;
        this.data = data;
        this.page = page;
    }
    
    public ResponsePage(String code, String message, T data, T2 page) {
        this.code = code;
        this.msg = message;
        this.data = data;
        this.page = page;
    }
}
