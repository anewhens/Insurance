package cn.jeefast.model;

import cn.jeefast.common.excel.ExcelResources;
import lombok.Data;

/**
 * <p>
 * 导出商品信息
 * </p>
 *
 * @author theodo
 * @since 2017-10-28
 */
@Data
public class ProgressVO {
    /**
     * 序号
     */
    private String cvpId;
    /**
     * 学习进度
     */
    private String status;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户账号
     */
    private String userMobile;
    /**
     * 省行
     */
    private String provinceBankName;
    /**
     * 市行
     */
    private String cityBankName;

    @ExcelResources(title = "序号", order = 1)
    public String getCvpId() {
        return cvpId;
    }
    @ExcelResources(title = "学习进度", order = 2)
    public String getStatus() {
        return status;
    }
    @ExcelResources(title = "用户名称", order = 3)
    public String getUserName() {
        return userName;
    }
    @ExcelResources(title = "用户账号", order = 4)
    public String getUserMobile() {
        return userMobile;
    }
    @ExcelResources(title = "省行", order = 5)
    public String getProvinceBankName() {
        return provinceBankName;
    }
    @ExcelResources(title = "市行", order = 6)
    public String getCityBankName() {
        return cityBankName;
    }
    public void setCvpId(String cvpId) {
        this.cvpId = cvpId;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public void setProvinceBankName(String provinceBankName) {
        this.provinceBankName = provinceBankName;
    }

    public void setCityBankName(String cityBankName) {
        this.cityBankName = cityBankName;
    }
}
