package cn.jeefast.model;

import cn.jeefast.common.excel.ExcelResources;
import lombok.Data;

/**
 * <p>
 * 导出商品信息
 * </p>
 *
 * @author theodo
 * @since 2017-10-28
 */
@Data
public class SysUserVO {
    /**
     * 用户id
     */
    private String userId;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户账号
     */
    private String userMobile;
    /**
     * 角色
     */
    private String roleName;
    /**
     * 时间
     */
    private String creationTime;

    @ExcelResources(title = "用户id", order = 1)
    public String getUserId() {
        return userId;
    }
    @ExcelResources(title = "用户名称", order = 2)
    public String getUserName() {
        return userName;
    }
    @ExcelResources(title = "用户账号", order = 3)
    public String getUserMobile() {
        return userMobile;
    }
    @ExcelResources(title = "角色名称", order = 4)
    public String getRoleName() {
        return roleName;
    }
    @ExcelResources(title = "添加时间", order = 5)
    public String getCreationTime() {
        return creationTime;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }
}
