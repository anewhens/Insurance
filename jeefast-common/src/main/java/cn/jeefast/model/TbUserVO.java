package cn.jeefast.model;

import cn.jeefast.common.excel.ExcelResources;
import lombok.Data;

/**
 * <p>
 * 导出商品信息
 * </p>
 *
 * @author theodo
 * @since 2017-10-28
 */
@Data
public class TbUserVO {
    /**
     * 用户id
     */
    private String userId;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户账号
     */
    private String userMobile;
    /**
     * 省行
     */
    private String provinceBankName;
    /**
     * 市行
     */
    private String cityBankName;
    /**
     * 支行
     */
    private String subBank;
    /**
     * 网点
     */
    private String location;

    @ExcelResources(title = "用户id", order = 1)
    public String getUserId() {
        return userId;
    }
    @ExcelResources(title = "用户名称", order = 2)
    public String getUserName() {
        return userName;
    }
    @ExcelResources(title = "用户账号", order = 3)
    public String getUserMobile() {
        return userMobile;
    }
    @ExcelResources(title = "省行", order = 4)
    public String getProvinceBankName() {
        return provinceBankName;
    }
    @ExcelResources(title = "市行", order = 5)
    public String getCityBankName() {
        return cityBankName;
    }
    @ExcelResources(title = "支行", order = 6)
    public String getSubBank() {
        return subBank;
    }
    @ExcelResources(title = "网点", order = 7)
    public String getLocation() {
        return location;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public void setProvinceBankName(String provinceBankName) {
        this.provinceBankName = provinceBankName;
    }

    public void setCityBankName(String cityBankName) {
        this.cityBankName = cityBankName;
    }

    public void setSubBank(String subBank) {
        this.subBank = subBank;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
