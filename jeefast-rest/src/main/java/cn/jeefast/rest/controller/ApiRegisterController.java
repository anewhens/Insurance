package cn.jeefast.rest.controller;

import cn.jeefast.rest.entity.TbUser;
import cn.jeefast.rest.service.IrCodeService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.jeefast.common.utils.R;
import cn.jeefast.common.validator.Assert;
import cn.jeefast.rest.annotation.AuthIgnore;
import cn.jeefast.rest.service.TbUserService;

import java.util.Date;
import java.util.List;

/**
 * 注册
 * @author theodo
 * @email 36780272@qq.com
 * @date 2017-10-26 17:27
 */
@RestController
@RequestMapping("/api/")
@Api(tags ="用户个人相关接口")
@CrossOrigin
public class ApiRegisterController {
    @Autowired
    private TbUserService userService;

    @Autowired
    private IrCodeService codeService;

    /**
     * 注册
     */
    @AuthIgnore
    @PostMapping("resgiter")
    @ApiOperation(value="用户注册", notes="注册")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "mobile", value = "登录手机号", required = true, dataType="String",paramType="query"),
//            @ApiImplicitParam(name = "password", value = "如果使用密码登录，则code不传", required = false, dataType="String",paramType="query"),
//            @ApiImplicitParam(name = "code", value = "如果使用验证码登录，则password不传", required = false, dataType="String",paramType="query")
//    })
    public R register(TbUser user,String code){
        EntityWrapper ew = new EntityWrapper();
        ew.eq("mobile",user.getMobile());
        List list = userService.selectList(ew);
        if(list.size()!=0){
            return R.error("账号已注册 ").put("data",null);
        }
        R r = codeService.checkCode(user.getMobile(), code);
        if((Integer) r.get("code")!=0){
            return r;
        }
        user.setCreateTime(new Date());
        user.setPassword(DigestUtils.sha256Hex(user.getPassword()));
        userService.insert(user);
        return R.ok("注册成功").put("data",null);
    }

    @AuthIgnore
    @PostMapping("forget")
    @ApiOperation(value="忘记密码", notes="")
    public R forgetPass(String mobile,String password,String code){
        R r = codeService.checkCode(mobile, code);
        if((Integer) r.get("code")!=0){
            return r;
        }
        EntityWrapper ew = new EntityWrapper();
        ew.eq("mobile",mobile);
        List<TbUser> list = userService.selectList(ew);
        if(list.size()==0){
            return R.error("无此用户");
        }
        TbUser tbUser = list.get(0);
        tbUser.setPassword(DigestUtils.sha256Hex(password));
        userService.updateById(tbUser);
        return R.ok("成功").put("data",null);
    }
}
