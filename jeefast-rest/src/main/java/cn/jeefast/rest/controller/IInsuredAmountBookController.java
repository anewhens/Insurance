package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IInsuredAmount;
import cn.jeefast.common.entity.IInsuredAmountBook;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.annotation.AuthIgnore;
import cn.jeefast.rest.service.IInsuredAmountBookService;
import cn.jeefast.rest.service.IInsuredAmountFeeService;
import cn.jeefast.rest.service.IInsuredAmountService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
@RestController
@RequestMapping("/api/insuredAmountBook")
@Api(tags ="计划书管理")
@CrossOrigin
public class IInsuredAmountBookController {

    @Autowired
    private IInsuredAmountBookService iInsuredAmountBookService;

    @Autowired
    private IInsuredAmountService iInsuredAmountService;

    @PostMapping("/addBoook")
    @ApiOperation(value="生成计划书", notes="iiabAddTime、iiabId、iiabShare不传")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "iiabBname", value = "被保人姓名", required = true, dataType="String",paramType="query"),
            @ApiImplicitParam(name = "iiabBsex", value = "被保人性别，1男0女", required = true, dataType="int",paramType="query"),
            @ApiImplicitParam(name = "iiabBbirth", value = "被保人出生年月", required = true, dataType="date",paramType="query"),
            @ApiImplicitParam(name = "iiabTname", value = "投保人姓名", required = true, dataType="String",paramType="query"),
            @ApiImplicitParam(name = "iiabTsex", value = "投保人性别 1男0女", required = true, dataType="int",paramType="query"),
            @ApiImplicitParam(name = "iiabTbirth", value = "投保人出生年月", required = true, dataType="date",paramType="query"),
            @ApiImplicitParam(name = "iiabMoney", value = "保额", required = true, dataType="String",paramType="query"),
            @ApiImplicitParam(name = "iiabPay", value = "缴费期间传", required = true, dataType="String",paramType="query"),
            @ApiImplicitParam(name = "iiabTime", value = "保险期间", required = true, dataType="String",paramType="query"),
            @ApiImplicitParam(name = "iiabAdd", value = "附加险，多个附加险用逗号隔开", required = true, dataType="String",paramType="query"),
            @ApiImplicitParam(name = "iiabFee", value = "保费", required = true, dataType="String",paramType="query")
    })
    public R addBook(IInsuredAmountBook book,HttpServletRequest request){
        Long userId = (Long)request.getAttribute("userId");
        book.setIiabShare(userId);
        book.setIiabAddTime(new Date());
        iInsuredAmountBookService.insert(book);
        Map map = new HashMap();
        map.put("bookId",book.getIiabId());
        return R.ok("成功").put("data",map);
    }


    @GetMapping("/bookList")
    @ApiOperation(value="计划书列表", notes="")
    public R bookList(Integer page, Integer limit, HttpServletRequest request){
        Long userId = (Long) request.getAttribute("userId");
        limit = limit ==null?10:limit;
        page = page ==null?0:(page-1)*limit;
        List<Map> maps = iInsuredAmountBookService.selectPage(userId, page, limit);
        return R.ok("success").put("data",maps);
    }

    @GetMapping("/bookListPc")
    @ApiOperation(value="计划书列表Pc", notes="")
    public R bookListPc(Integer page, Integer limit, HttpServletRequest request){
        Long userId = (Long) request.getAttribute("userId");
        limit = limit ==null?10:limit;
        page = page ==null?0:(page-1)*limit;
        List<Map> maps = iInsuredAmountBookService.selectPage(userId, page, limit);
        List<Map> count = iInsuredAmountBookService.selectPage(userId, 0, 100000);
        Map map = new HashMap();
        map.put("list",maps);
        map.put("count",count.size());
        return R.ok("success").put("data",map);
    }

    @GetMapping("/bookDetail")
    @AuthIgnore
    public R detail(Long id){
        IInsuredAmountBook iInsuredAmountBook = iInsuredAmountBookService.selectById(id);
        IInsuredAmount iInsuredAmount = iInsuredAmountService.selectById(iInsuredAmountBook.getIiabIaId());
        Map map = new HashMap();
        map.put("userInfo",iInsuredAmountBook);
        map.put("insuredInfo",iInsuredAmount);
        return R.ok("成功").put("data",map);
    }
}
