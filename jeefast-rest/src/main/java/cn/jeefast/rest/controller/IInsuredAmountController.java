package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IInsuredAmount;
import cn.jeefast.common.entity.IInsuredAmountFee;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.service.IInsuredAmountFeeService;
import cn.jeefast.rest.service.IInsuredAmountService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
@RestController
@RequestMapping("/api/insuredAmount")
@Api(tags ="计划书管理")
@CrossOrigin
public class IInsuredAmountController {

    @Autowired
    private IInsuredAmountService iInsuredAmountService;

    @Autowired
    private IInsuredAmountFeeService feeService;

    @GetMapping("/amountList")
    @ApiOperation(value="保险列表", notes="")
    public R amountList(Long iiaComp,Integer iiaType,String iiaTitle,Integer page,Integer limit){
        limit = limit == null?10:limit;
        page = page ==null?0:(page-1)*limit;
        List<Map> maps = iInsuredAmountService.irList(iiaComp, iiaType, iiaTitle, page, limit);
        EntityWrapper ew = new EntityWrapper();
        if(iiaType!=null){
            ew.eq("iia_type",iiaType);
        }
        if (iiaComp!=null){
            ew.eq("iia_comp",iiaComp);
        }
        if(iiaTitle!=null && !iiaTitle.equals("")){
            ew.like("iia_title",iiaTitle);
        }
        ew.eq("deletes",0);
        List list = iInsuredAmountService.selectList(ew);
        Map map = new HashMap();
        map.put("list",maps);
        map.put("count",list.size());
        return R.ok("成功").put("data",map);
    }

    @GetMapping("/feeTimeList")
    @ApiOperation(value="缴费期间", notes="iiaId传保险产品ID")
    public R feeTimeList(Long iiaId){
        EntityWrapper ew = new EntityWrapper();
        ew.eq("iia_id",iiaId);
        ew.groupBy("iiaf_fee_time");
        ew.orderBy("iiaf_id",true);
        List<IInsuredAmountFee> list = feeService.selectList(ew);
//        list = list.sort((o1, o2) -> o2.getIiafFeeTime().compareTo(o1.getIiafFeeTime()));
//        t.sort((o1, o2) -> o2.getIiafId().compareTo(o1.getIiafId()));
        return R.ok("成功").put("data",list);
    }

    @GetMapping("/timeList")
    @ApiOperation(value="保险期间", notes="iiaId传保险产品ID,feeTime传保险期间")
    public R timeList(String feeTime,Long iiaId){
        EntityWrapper ew = new EntityWrapper();
        ew.eq("iia_id",iiaId);
        ew.eq("iiaf_fee_time",feeTime);
        ew.groupBy("iiaf_time");
        List<IInsuredAmountFee> list = feeService.selectList(ew);
        return R.ok("成功").put("data",list);
    }

    @GetMapping("/getFee")
    @ApiOperation(value="根据保险ID、性别、年龄等查询保费", notes="iaId传保险ID、feeTime 保费缴纳期间  time 保险期间")
    public R getFee(Long iaId,String sex,Integer age,String feeTime,String time){
        EntityWrapper ew = new EntityWrapper();
        ew.eq("iia_id",iaId);
        ew.eq("iiaf_sex",sex);
        ew.eq("iiaf_age",age);
        if(time!=null){
            ew.eq("iiaf_time",time);
        }
        if(feeTime!=null){
            ew.eq("iiaf_fee_time",feeTime);
        }
        List<IInsuredAmountFee> list = feeService.selectList(ew);
        if(list.size()!=1){
            return R.error("查询不到匹配数据");
        }
        return R.ok("成功").put("data",list.get(0));
    }
}
