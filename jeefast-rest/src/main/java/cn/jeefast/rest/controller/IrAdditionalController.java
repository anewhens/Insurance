package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrAdditional;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.service.IrAdditionalService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
@RestController
@RequestMapping("/api/additional")
@Api(tags ="附加险")
@CrossOrigin
public class IrAdditionalController {

    @Autowired
    private IrAdditionalService irAdditionalService;


    @GetMapping("additionalList")
    @ApiOperation(value="获取附加险列表", notes="登录")
    public R additionList(){
        List<IrAdditional> irAdditionals = irAdditionalService.selectList(new EntityWrapper<>());
        return R.ok("success").put("data",irAdditionals);
    }


	
}
