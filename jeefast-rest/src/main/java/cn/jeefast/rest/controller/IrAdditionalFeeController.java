package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrAdditionalFee;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.service.IrAdditionalFeeService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
@RestController
@RequestMapping("/irAdditionalFee")
@Api(tags ="附加险")
@CrossOrigin
public class IrAdditionalFeeController {

    @Autowired
    private IrAdditionalFeeService irAdditionalFeeService;

    @GetMapping("additionalFee")
    @ApiOperation(value="获取费用", notes="登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "addId", value = "附加险ID", required = true, dataType="int",paramType="query"),
            @ApiImplicitParam(name = "age", value = "年龄", required = false, dataType="int",paramType="query"),
            @ApiImplicitParam(name = "time", value = "保险期间", required = false, dataType="string",paramType="query"),
            @ApiImplicitParam(name = "feeTime", value = "缴费期间", required = false, dataType="string",paramType="query"),
            @ApiImplicitParam(name = "sex", value = "性别 男 女", required = false, dataType="String",paramType="query")
    })
    public R additionalFee(Long addId,Integer age,String sex,String time,String feeTime){
        EntityWrapper ew = new EntityWrapper();
        ew.eq("ia_id",addId);
        ew.eq("ia_age",age);
        ew.eq("ia_sex",sex);
        if(time!=null){
            ew.eq("ia_time",time);
        }
        if(feeTime!=null){
            ew.eq("ia_fee_time",feeTime);
        }
        List<IrAdditionalFee> list = irAdditionalFeeService.selectList(ew);
        if(list.size()>1){
            return R.error("条件参数模糊，有多条结果").put("data",null);
        }
        if(list==null){
            return R.error("无结果").put("data",null);
        }
        return R.ok("success").put("data",list.get(0));
    }
}
