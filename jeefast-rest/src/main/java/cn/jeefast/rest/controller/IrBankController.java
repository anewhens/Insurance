package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrBank;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.annotation.AuthIgnore;
import cn.jeefast.rest.service.IrBankService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-13
 */
@RestController
@RequestMapping("/api/bank")
@Api(tags ="银行信息")
@CrossOrigin
public class IrBankController {
    @Autowired
    private IrBankService bankService;

    @GetMapping("/bankList")
    @AuthIgnore
    @ApiOperation(value="获取银行信息-分页", notes="时间降序")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "parentId", value = "不传默认获取全部省行", dataType="integer",paramType="query"),
            @ApiImplicitParam(name = "page", value = "page", required = true, dataType="integer",paramType="query"),
            @ApiImplicitParam(name = "limit", value = "page", required = true, dataType="integer",paramType="query")
    })
    public R bankList(Integer parentId, Integer page,Integer limit){
        page=(page-1)*limit;
        if(page==null||limit==null){
            return R.error("分页参数有误").put("data",null);
        }

        if(parentId==null){
            EntityWrapper ew  =  new EntityWrapper();
            ew.eq("bank_type",1);
            List<IrBank> bankList = bankService.selectByPage(page, limit);
            List<IrBank> irBanks = bankService.selectList(ew);
            Map map=new HashMap();
            map.put("list",bankList);
            map.put("count",irBanks.size());
            return R.ok("成功").put("data",map);
        }else{
            EntityWrapper ew  = new EntityWrapper();
            ew.eq("back_parent",parentId);
            List<IrBank> bankList = bankService.selectByPage2(parentId,page, limit);
            List<IrBank> irBanks = bankService.selectList(ew);
            Map map=new HashMap();
            map.put("list",bankList);
            map.put("count",irBanks.size());
            return R.ok("成功").put("data",map);
        }
    }
	
}
