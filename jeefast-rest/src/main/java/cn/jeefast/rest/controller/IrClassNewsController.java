package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrClassNews;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.service.IrClassNewsService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
@RestController
@RequestMapping("/api/classNews")
@CrossOrigin
@Api(tags ="首页-保险超级课堂-文章资讯类")
public class IrClassNewsController {
	@Autowired
    private IrClassNewsService classNewsService;


    /**
     * 文章列表
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("/classNewsList")
    @ApiOperation(value="资讯类（小白....）", notes="时间降序")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "page", required = true, dataType="integer",paramType="query"),
            @ApiImplicitParam(name = "limit", value = "page", required = true, dataType="integer",paramType="query")
    })
	private R classNewsByPage(Integer page,Integer limit,String title,Integer type){

        page=(page-1)*limit;

        if(page==null||limit==null){
            return R.error("分页参数有误").put("data",null);
        }
	    //分页查询
        List<IrClassNews> irClassNews = classNewsService.classNewsByPage(page, limit,title,type);
        //查询总数
        EntityWrapper ew = new EntityWrapper();
        ew.like("cn_title",title);
        if(type!=null){
            ew.eq("cn_type",type);
        }
        List<IrClassNews> irClassNews1 = classNewsService.selectList(ew);
        //查询点击量
        Integer integer = classNewsService.clssNewsClickCount(title,type);

        //封装查询结果
        Map map = new HashMap();
        map.put("list",irClassNews);
        map.put("count",irClassNews1.size());
        map.put("click",integer);

        return R.ok("成功").put("data",map);
    }

    /**
     * 获取资讯类详情
     * @param newsId
     * @return
     */
    @GetMapping("/classNewsDetail")
    @ApiOperation(value="根据ID获取资讯类详情", notes="")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "newsId", value = "newsId", required = true, dataType="integer",paramType="query")
    })
    private R lowDetail(Long newsId){
        if(newsId==null){
            return R.error("newsId不能为空").put("data",null);
        }
        IrClassNews irClassNews = classNewsService.selectById(newsId);
        irClassNews.setCnCount(irClassNews.getCnCount()+1);
        classNewsService.updateById(irClassNews);
        if(irClassNews==null){
            return R.error("newsId不存在").put("data",null);
        }

        return  R.ok("成功").put("data",irClassNews);
    }
}
