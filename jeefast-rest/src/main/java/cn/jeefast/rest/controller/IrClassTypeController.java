package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrClassType;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.service.IrClassTypeService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-20
 */
@RestController
@RequestMapping("/api/classType")
@Api(tags ="超级课堂")
@CrossOrigin
public class IrClassTypeController {

    @Autowired
    private IrClassTypeService classTypeService;

    @GetMapping("/classType")
    @ApiOperation(value="超级课堂分类", notes="")
    public R classTypeList(){
        List<IrClassType> irClassTypes = classTypeService.selectList(new EntityWrapper<>());
        return R.ok().put("data",irClassTypes);
    }
}
