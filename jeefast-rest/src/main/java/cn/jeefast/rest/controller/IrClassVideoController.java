package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrClassVideo;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.entity.TbUser;
import cn.jeefast.rest.service.IrClassVideoService;
import cn.jeefast.rest.service.TbUserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-24
 */
@RestController
@RequestMapping("/api/classVideo")
@Api(tags ="视频资讯")
@CrossOrigin
public class IrClassVideoController {

    @Autowired
    private IrClassVideoService classVideoService;

    @Autowired
    private TbUserService userService;

    /**
     * 获取视频资讯列表
     * @param cpId
     * @param itId
     * @return
     */
    @GetMapping("/videoListByCpIdAndItId")
    @ApiOperation(value="获取视频资讯列表", notes="")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cpId", value = "保险公司ID", required = true, dataType="integer",paramType="query"),
            @ApiImplicitParam(name = "itId", value = "保险类别ID", required = true, dataType="integer",paramType="query")
    })
    public R videoListByType(Long cpId, Long itId, Integer limit, Integer page, HttpServletRequest request){
        limit = limit == null? 10 : limit;
        page = page == null? 0 : (page-1)*limit;
        Long userId = (Long) request.getAttribute("userId");
        TbUser tbUser = userService.selectById(userId);
        List<Map> irClassVideos = classVideoService.selectListByPage(cpId, itId, limit, page,tbUser.getProvinceBank(),userId);
        Integer number= classVideoService.getNumber(cpId, itId, tbUser.getProvinceBank());
        Map map=new HashMap();
        map.put("list",irClassVideos);
        map.put("count",number);
        return R.ok("success").put("data",map);
    }
}
