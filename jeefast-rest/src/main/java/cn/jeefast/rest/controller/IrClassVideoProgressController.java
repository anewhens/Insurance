package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrClassVideo;
import cn.jeefast.common.entity.IrClassVideoProgress;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.service.IrClassVideoProgressService;
import cn.jeefast.rest.service.IrClassVideoService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-24
 */
@RestController
@RequestMapping("/api/classVideoProgress")
@Api(tags ="视频资讯")
@CrossOrigin
public class IrClassVideoProgressController {

    @Autowired
    private IrClassVideoProgressService classVideoProgressService;

    @Autowired
    private IrClassVideoService classVideoService;

    @PostMapping("/updateProgress")
    @ApiOperation(value="更新视频播放进度", notes="")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cvId", value = "视频ID", required = true, dataType="integer",paramType="query"),
            @ApiImplicitParam(name = "status", value = "播放状态：0：未开始 1：学习中 2：已完成", required = true, dataType="integer",paramType="query"),
            @ApiImplicitParam(name = "progress", value = "播放进度", required = true, dataType="String",paramType="query")
    })
    public R updateProgress(HttpServletRequest request,Long cvId,String progress,Integer status){
        //查询视频ID和用户ID是否有记录
        EntityWrapper ew = new EntityWrapper();
        Long userId = (Long) request.getAttribute("userId");
        ew.eq("user_id",userId);
        ew.eq("cv_id",cvId);
        List<IrClassVideoProgress> list = classVideoProgressService.selectList(ew);
        if(list.size()==0){
            //新建
            IrClassVideoProgress classVideoProgress = new IrClassVideoProgress();
            classVideoProgress.setCvId(cvId);
            classVideoProgress.setCvpStatus(status);
            classVideoProgress.setUserId(userId);
            classVideoProgress.setCvpProgress(progress);
            classVideoProgressService.insert(classVideoProgress);
            return R.ok("success").put("data",null);
        }else{
            IrClassVideoProgress classVideoProgress = list.get(0);
            if(classVideoProgress.getCvpStatus()==2){
                //已完成，不做更改
                return R.ok("success").put("data",null);
            }else{
                classVideoProgressService.updateById(classVideoProgress);
            }

            classVideoProgress.setCvpStatus(status);
            classVideoProgress.setCvpProgress(progress);
            classVideoProgressService.updateById(classVideoProgress);
            return R.ok("success").put("data",null);
        }
    }

    @GetMapping("getProgress")
    @ApiOperation(value="视频详情+播放进度 ", notes="")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cvId", value = "视频ID", required = true, dataType="integer",paramType="query")
    })
    public R getProgress(HttpServletRequest request,Long cvId){
        EntityWrapper ew = new EntityWrapper();
        Long userId = (Long) request.getAttribute("userId");
        ew.eq("user_id",userId);
        ew.eq("cv_id",cvId);
        List<IrClassVideoProgress> list = classVideoProgressService.selectList(ew);
        IrClassVideo irClassVideo = classVideoService.selectById(cvId);
        Map result = new HashMap();
        result.put("video",irClassVideo);
        if(list.size()==0){
            IrClassVideoProgress classVideoProgress = new IrClassVideoProgress();
            classVideoProgress.setCvpProgress("0");
            result.put("progress",classVideoProgress);
        }else{
            result.put("progress",list.get(0));
        }
        return R.ok("success").put("data",result);
    }

    @GetMapping("getProgressList")
    @ApiOperation(value="待办事项 ", notes="")
    public R list(HttpServletRequest request,Integer page,Integer limit){
        limit = limit == null? 10:limit;
        page = page==null?0:(page-1)*limit;
        Long userId = (Long) request.getAttribute("userId");
        List<Map> maps = classVideoProgressService.selectPage(userId, page, limit);
        return R.ok("成功").put("data",maps);
    }
}
