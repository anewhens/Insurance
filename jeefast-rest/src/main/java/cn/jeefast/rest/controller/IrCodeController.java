package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrCode;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.annotation.AuthIgnore;
import cn.jeefast.rest.service.IrCodeService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
@RestController
@RequestMapping("/api/code")
@Api(tags ="验证码")
@CrossOrigin
public class IrCodeController {

    @Autowired
    private IrCodeService codeService;


    @GetMapping("/getSMSCode")
    @AuthIgnore
    @ApiOperation(value="获取验证码", notes="")
    @ApiImplicitParam(name = "phone", value = "手机号", required = true, dataType="integer",paramType="query")
    public R getCode(String phone){
        if(phone==null ||phone.length()!=11){
            return R.error("手机号长度异常").put("data",null);
        }
        return codeService.sendCode(phone);
    }

    @PostMapping("/checkCode")
    @AuthIgnore
    @ApiOperation(value="验证验证码", notes="")
    public R checkCode(String phone,String code){
        return codeService.checkCode(phone,code);
    }
}
