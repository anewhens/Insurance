package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrCompany;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.service.IrCompanyService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiOperation;
import net.sourceforge.pinyin4j.PinyinHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;


import java.util.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-20
 */
@RestController
@RequestMapping("/api/company")
@Api(tags ="保险公司")
@CrossOrigin
public class IrCompanyController {

    @Autowired
    private IrCompanyService companyService;

    @GetMapping("/companyList")
    @ApiOperation(value="保险公司列表", notes="")
    public R companyList(){
        //查询保险公司结果集
        List<IrCompany> irCompanies = companyService.selectList(new EntityWrapper<>());
        for (IrCompany irc:irCompanies) {
            //追加首字母排序
            int index = getIndex(irc);
            irc.setIndex(index);
        }
        //整体排序
        irCompanies.sort(Comparator.comparing(IrCompany::getIndex));

        //按照规定四组进行分组
        List<IrCompany> AtoF = new ArrayList<>();
        List<IrCompany> GtoL = new ArrayList<>();
        List<IrCompany> MtoR = new ArrayList<>();
        List<IrCompany> StoZ = new ArrayList<>();

        //循环插入
        for (IrCompany irc:irCompanies) {
            //获取首字母拼音并返回所在组别索引
            int index = getPinYinHeadChar(irc.getIcTitle());
            if(index==1){
                AtoF.add(irc);
            }else if(index==2){
                GtoL.add(irc);
            }else if(index==3){
                MtoR.add(irc);
            }else{
                StoZ.add(irc);
            }
        }

        //获取推荐信息，追加到recommend中
        List<IrCompany> recommend = new ArrayList<>();
        for (IrCompany irc:irCompanies) {
            if(irc.getIcRecommend()==1){
                recommend.add(irc);
            }
        }

        //封装结果集并返回
        Map map = new HashMap();
        map.put("AtoF",AtoF);
        map.put("GtoL",GtoL);
        map.put("MtoR",MtoR);
        map.put("StoZ",StoZ);
        map.put("recommend",recommend);

        return R.ok("成功").put("data",map);
    }

    /**
     * 根据汉字首字母获取所在的四组索引值
     * @param str
     * @return index索引 1-4
     */
    public static int getPinYinHeadChar(String str) {

        String convert = "";
        for (int j = 0; j < str.length(); j++) {
            char word = str.charAt(j);
            String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
            if (pinyinArray != null) {
                convert += pinyinArray[0].charAt(0);
            } else {
                convert += word;
            }
        }

        //截取首字母
        String substring = convert.substring(0, 1);
        //目标串
        String AtoF = "abcdef";
        String GtoL = "ghijkl";
        String MtoR = "mnopqr";
        String StoZ = "stuvwxyz";

        //返回索引
        if(AtoF.indexOf(substring)!=-1){
            return 1;
        }else if(GtoL.indexOf(substring)!=-1){
            return 2;
        }else if(MtoR.indexOf(substring)!=-1){
            return 3;
        }else{
            return 4;
        }

    }

    /**
     * 获取汉字首字母所在的英文字母排序索引，追加排序值
     * @param company
     * @return int 排序值
     */
    public static int getIndex(IrCompany company){
        String convert = "";
        //获取拼音
        for (int j = 0; j < company.getIcTitle().length(); j++) {
            char word = company.getIcTitle().charAt(j);
            String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
            if (pinyinArray != null) {
                convert += pinyinArray[0].charAt(0);
            } else {
                convert += word;
            }
        }
        //截取首字母
        String substring = convert.substring(0, 1);
        String indexStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toLowerCase();
        //返回索引
        int i = indexStr.indexOf(substring);
        return i;
    }
}
