package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrDict;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.annotation.AuthIgnore;
import cn.jeefast.rest.service.IrDictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *     字典
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
@RestController
@RequestMapping("/api/dict")
@Api(tags ="通用信息接口")
@CrossOrigin
public class IrDictController {

    @Autowired
    private IrDictService dictService;

    @ApiOperation(value="获取LOGO地址", notes="")
    @GetMapping("/logoPath")
    @AuthIgnore
    public R logoPath(){
        IrDict irDict = dictService.selectById(1);
        return R.ok("成功").put("data",irDict);
    }

    /**
     * 登录页1广告图
     * @return
     */
    @ApiOperation(value="登录页广告图", notes="")
    @GetMapping("/loginAdBanner")
    @AuthIgnore
    private R loginAdBanner(){
        IrDict irDict = dictService.selectById(2);
        return R.ok("成功").put("data",irDict);
    }

    /**
     * 登录页2广告图
     * @return
     */
    @ApiOperation(value="登录页2广告图", notes="")
    @GetMapping("/loginAd2Banner")
    @AuthIgnore
    private R loginAd2Banner(){
        IrDict irDict = dictService.selectById(3);
        return R.ok("成功").put("data",irDict);
    }


	
}
