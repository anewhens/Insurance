package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrEduExam;
import cn.jeefast.common.entity.IrEduTime;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.service.IrEduExamService;
import cn.jeefast.rest.service.IrEduTimeService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-18
 */
@RestController
@RequestMapping("/api/eduExam")
@Api(tags ="继续教育")
@CrossOrigin
public class IrEduExamController {

    @Autowired
    private IrEduExamService eduExamService;


    /**
     * 考试列表
     * @param typeId
     * @return
     */
    @GetMapping("/exmaList")
    @ApiOperation(value="根据考试类别ID获取考试列表", notes="")
    @ApiImplicitParam(name = "typeId", value = "类别ID", required = true, dataType="String",paramType="query")
    public R examList(Integer typeId, Integer page, Integer limit, HttpServletRequest request){
        Long userId = (Long)request.getAttribute("userId");
        limit = limit == null ? 10 : limit;
        page = page == null ? 0 : (page-1)*limit;
        return eduExamService.examList(typeId,page,limit,userId);
    }

    @GetMapping("/detail")
    @ApiOperation(value="根据题目ID获取题目详情", notes="")
    @ApiImplicitParam(name = "eeId", value = "题目 ID", required = true, dataType="String",paramType="query")
    public R examDetail(Long eeId){
        IrEduExam irEduExam = eduExamService.selectById(eeId);
        return R.ok("success").put("data",irEduExam);
    }
}
