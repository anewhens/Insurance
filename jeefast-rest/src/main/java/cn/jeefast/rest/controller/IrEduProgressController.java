package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrEduProgress;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.service.IrEduProgressService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-18
 */
@RestController
@RequestMapping("/api/eduProgress")
@Api(tags ="继续教育")
@CrossOrigin
public class IrEduProgressController {

    @Autowired
    private IrEduProgressService eduProgressService;

    @PostMapping("update")
    @ApiOperation(value="更新视频播放进度及分数提交", notes="")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "etId", value = "批次ID", required = true, dataType="integer",paramType="query"),
            @ApiImplicitParam(name = "eeId", value = "题目ID", required = true, dataType="integer",paramType="query"),
            @ApiImplicitParam(name = "videoProgress", value = "视频播放进度", required = true, dataType="string",paramType="query"),
            @ApiImplicitParam(name = "score", value = "答题分数，不更新则无需填写", required = true, dataType="integer",paramType="query"),
            @ApiImplicitParam(name = "isReExam", value = "前端判断此题是否满分，满分传1，标识不可以重做，否则传0，标识可以重做", required = true, dataType="integer",paramType="query")
    })
    public R callProgress(Long etId,Long eeId,String videoProgress,Integer score,Integer isReExam,HttpServletRequest request){
        //查询是否存在进度记录
        EntityWrapper ew = new EntityWrapper();
        ew.eq("user_id",(Long)request.getAttribute("userId"));
        ew.eq("et_id",etId);
        ew.eq("ee_id",eeId);
        List<IrEduProgress> progresList = eduProgressService.selectList(ew);
        if(progresList.size()==0){
            //第一次做题，创建进度记录。
            IrEduProgress progress = new IrEduProgress();
            progress.setEeId(eeId);
            progress.setEtId(etId);
            progress.setUserId((Long)request.getAttribute("userId"));
            progress.setEpTime(new Date());
            progress.setEpVideoProgress(videoProgress);
            progress.setEpScore(score);
            progress.setEpReExam(isReExam);
            eduProgressService.insert(progress);
            return R.ok("记录成功").put("data",null);
        }else if(progresList.size()==1){
            IrEduProgress progress = progresList.get(0);
            progress.setEpTime(new Date());
            progress.setEpVideoProgress(videoProgress);
            progress.setEpScore(score);
            progress.setEpReExam(isReExam);
            eduProgressService.updateById(progress);
            return R.ok("更新成功").put("data",null);
        }else{
            return R.error("数据错误").put("data",null);
        }
    }


    @GetMapping("/examResult")
    @ApiOperation(value="查询考试结果列表", notes="")
    public R selectExamResult(HttpServletRequest request) {
        return eduProgressService.selectExamResult((Long)request.getAttribute("userId"));
    }

    @GetMapping("/examResultPc")
    @ApiOperation(value="查询考试结果列表PC", notes="")
    public R selectExamResultPc(HttpServletRequest request,Integer page,Integer limit) {
        limit = limit == null? 10:limit;
        page = page == null?0:(page-1)*limit;
        return eduProgressService.selectExamResultPc((Long)request.getAttribute("userId"),page,limit);
    }

    /**
     * 查询成绩详细
     * @param etId
     * @return
     */
    @GetMapping("/selectExamScore")
    @ApiOperation(value="考试结果详情", notes="")
    @ApiImplicitParam(name = "etId", value = "批次ID", required = true, dataType="integer",paramType="query")
    public R selectExamScore(Long etId,HttpServletRequest request){
        return eduProgressService.selectExamScore((Long)request.getAttribute("userId"),etId);
    }

    @GetMapping("/examTask")
    @ApiOperation(value="考试任务", notes="0:未完成  1:已完成")
    public R examTask(Integer page,Integer limit,HttpServletRequest request){
        limit = limit == null ? 10 : limit;
        page = page == null ? 0 : (page-1)*limit;
        Long userId = (Long) request.getAttribute("userId");
        List<Map<String, Object>> maps = eduProgressService.examTask(userId, page, limit);
        return R.ok("成功").put("data",maps);
    }

    @GetMapping("/examTaskPc")
    @ApiOperation(value="考试任务PC", notes="0:未完成  1:已完成")
    public R examTaskPc(Integer page,Integer limit,HttpServletRequest request){
        limit = limit == null ? 10 : limit;
        page = page == null ? 0 : (page-1)*limit;
        Long userId = (Long) request.getAttribute("userId");
        List<Map<String, Object>> maps = eduProgressService.examTask(userId, page, limit);
        List<Map<String, Object>> count = eduProgressService.examTask(userId, 0, 10000);

        Map map = new HashMap();
        map.put("list",maps);
        map.put("count",count.size());
        return R.ok("成功").put("data",map);
    }

    @GetMapping("/canExam")
    public R canExam(HttpServletRequest request){
        return eduProgressService.canExam((Long) request.getAttribute("userId"));
    }
}
