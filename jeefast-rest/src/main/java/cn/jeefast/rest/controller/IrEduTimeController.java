package cn.jeefast.rest.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-17
 */
@RestController
@RequestMapping("/api/eduTime")
@Api(tags ="继续教育")
@CrossOrigin
public class IrEduTimeController {

}
