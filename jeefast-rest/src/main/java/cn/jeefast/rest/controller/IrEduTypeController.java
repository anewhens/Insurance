package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrEduType;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.service.IrEduTypeService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-18
 */
@RestController
@RequestMapping("/api/eduType")
@Api(tags ="继续教育")
@CrossOrigin
public class IrEduTypeController {
    @Autowired
	private IrEduTypeService eduTypeService;

    @GetMapping("/list")
    @ApiOperation(value="获取考试视频类别", notes="")
    public R typeList(){
        List<IrEduType> irEduTypes = eduTypeService.selectList(new EntityWrapper<>());

        return R.ok().put("data",irEduTypes);

    }
}
