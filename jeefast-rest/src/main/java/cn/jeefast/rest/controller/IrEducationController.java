package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrEducation;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.annotation.AuthIgnore;
import cn.jeefast.rest.service.IrEduTimeService;
import cn.jeefast.rest.service.IrEducationService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-16
 */
@RestController
@RequestMapping("/api/education")
@Api(tags ="继续教育")
@CrossOrigin
public class IrEducationController {

    @Autowired
    private IrEducationService educationService;
    @Autowired
    private IrEduTimeService timeService;

    /**
     * 报名
     * @param education
     * @return
     */
    @PostMapping("/addEducation")
    @ApiOperation(value="继续教育考试报名（最新一期）", notes="")
    public R addEducation(IrEducation education, HttpServletRequest request){
        //获取当前登录用户ID
        Long userId = (Long) request.getAttribute("userId");
        education.setUserId(userId);
        return educationService.signInExam(education);
    }

    @PostMapping("/checkRegister")
    @ApiOperation(value="检查是否报名", notes="")
    public R checkRegister(HttpServletRequest request){
        Long userId = (Long) request.getAttribute("userId");
        Long aLong = timeService.lastSignId();
        EntityWrapper ew = new EntityWrapper();
        ew.eq("user_id",userId);
        ew.eq("et_id",aLong);
        List list = educationService.selectList(ew);
        if(list.size()!=0){
            return R.error("您已报名，请按时考试").put("data",null);
        }
        return R.ok("允许报名").put("data",null);
    }
	
}
