package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrFeedback;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.service.IrFeedbackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *     用户反馈
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
@RestController
@RequestMapping("/api/feedBack")
@Api(tags ="用户反馈相关接口")
@CrossOrigin
public class IrFeedbackController {
    @Autowired
	private IrFeedbackService feedbackService;

    /**
     * 添加用户反馈
     * @param feedback
     * @return
     */
    @PostMapping("/addFeedback")
    @ApiOperation(value="添加用户反馈", notes="H5-意见反馈")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "fbContent", value = "反馈内容", required = true, dataType="String",paramType="body"),
//    })//            @ApiImplicitParam(name = "fbContent", value = "反馈内容", required = true, dataType="String",paramType="body")
    public R addFeedBack(IrFeedback feedback){
        boolean res = feedbackService.insert(feedback);
        if(res){
            return R.ok("提交成功").put("data",null);
        }else{
            return  R.error();
        }

    }
}
