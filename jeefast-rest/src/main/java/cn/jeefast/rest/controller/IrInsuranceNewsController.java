package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrInsuranceNews;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.service.IrInsuranceNewsService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-03-09
 */
@RestController
@RequestMapping("/api/insuranceNews")
@Api(tags ="保险金融资讯")
@CrossOrigin
public class IrInsuranceNewsController {

    @Autowired
    private IrInsuranceNewsService newsService;

    @PostMapping("newsList")
    @ApiOperation(value="列表", notes="")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "desc", value = "传1 desc排序 传0默认排序", required = true, dataType="integer",paramType="query")
    })
    public R newsList(Integer desc,Integer page,Integer limit){
        limit = limit ==null?10:limit;
        page = page ==null?0:(page-1)*limit;
        List<IrInsuranceNews> irInsuranceNews = newsService.newsList(desc, page, limit);
        return R.ok("成功").put("data",irInsuranceNews);
    }

    @PostMapping("newsDetail")
    @ApiOperation(value="详情", notes="")
    public R newsDetail(Long newsId){
        IrInsuranceNews irInsuranceNews = newsService.selectById(newsId);
        return R.ok("成功").put("data",irInsuranceNews);
    }
}
