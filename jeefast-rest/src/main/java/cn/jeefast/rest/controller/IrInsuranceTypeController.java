package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrInsuranceType;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.service.IrInsuranceTypeService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-20
 */
@RestController
@RequestMapping("/api/insuranceType")
@Api(tags ="保险")
@CrossOrigin
public class IrInsuranceTypeController {

    @Autowired
    private IrInsuranceTypeService insuranceTypeService;

    @GetMapping("/typeList")
    @ApiOperation(value="保险分类列表", notes="")
    public R typeList(){
        List<IrInsuranceType> irInsuranceTypes = insuranceTypeService.selectList(new EntityWrapper<>());
        return R.ok().put("data",irInsuranceTypes);
    }
	
}
