package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrLow;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.service.IrLowService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
@RestController
@RequestMapping("/api/low")
@Api(tags ="政策法规文章")
@CrossOrigin
public class IrLowController {
    @Autowired
    private IrLowService lowService;

    /**
     * 分页获取法律法规文章列表
     */
    @GetMapping("/lowList")
    @ApiOperation(value="法律法规列表，分页", notes="时间降序")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "page", required = true, dataType="integer",paramType="query"),
            @ApiImplicitParam(name = "limit", value = "page", required = true, dataType="integer",paramType="query")
    })
    public R listByPage(Integer page,Integer limit){

        if(page==null||limit==null){
            return R.error("分页参数有误").put("data",null);
        }

        page = (page-1)*limit;
        List<IrLow> irLows = lowService.listByPage(page, limit);
        List<IrLow> lowList = lowService.selectList(new EntityWrapper<>());
        Map map=new HashMap();
        map.put("list",irLows);
        map.put("count",lowList.size());
        return R.ok().put("data",map);
    }

    /**
     * 获取法律法规详情内容
     * @param lowId
     * @return
     */
    @GetMapping("/lowDetail")
    @ApiOperation(value="根据ID获取法律法规详情", notes="")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "lowId", value = "lowId", required = true, dataType="integer",paramType="query")
    })
    public R lowDetail(Long lowId){
        if(lowId==null){
            return R.error("lowId不能为空").put("data",null);
        }
        IrLow irLow = lowService.selectById(lowId);
        if(irLow==null){
            return R.error("lowId不存在").put("data",null);
        }

        return  R.ok("成功").put("data",irLow);
    }
}
