package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrMsgLunbo;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.annotation.AuthIgnore;
import cn.jeefast.rest.service.IrMsgLunboService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.text.html.parser.Entity;
import java.util.List;

/**
 * <p>
 * 首页消息轮播 前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-16
 */
@RestController
@RequestMapping("/api/irMsgLunbo")
@Api(tags ="首页轮播新闻")
@CrossOrigin
public class IrMsgLunboController {

    @Autowired
    private IrMsgLunboService msgLunboService;

    @AuthIgnore
    @GetMapping("/list")
    @ApiOperation(value="首页轮播列表", notes="")
    public R msgLunBoList(){
        EntityWrapper ew =  new EntityWrapper();
        ew.orderBy("ml_time",true);
        List list = msgLunboService.selectList(ew);

        return R.ok("成功！").put("data",list);

    }

    /**
     * 获取新闻详情
     * @param msgLunboId
     * @return
     */
    @AuthIgnore
    @GetMapping("/detail")
    @ApiImplicitParam(name = "msgLunboId", value = "轮播详情ID", required = true, dataType="integer",paramType="query")
    @ApiOperation(value="根据新闻ID获取新闻详情", notes="")
    public R msgLunBoDetail(Long msgLunboId){
        IrMsgLunbo irMsgLunbo = msgLunboService.selectById(msgLunboId);

        return R.ok("成功").put("data",irMsgLunbo);
    }
	
}
