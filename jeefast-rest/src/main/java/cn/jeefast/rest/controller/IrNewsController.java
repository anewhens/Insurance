package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrNews;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.annotation.AuthIgnore;
import cn.jeefast.rest.service.IrNewsService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
@RestController
@RequestMapping("/api/news")
@Api(tags ="首页资讯")
@CrossOrigin
public class IrNewsController {

    @Autowired
    private IrNewsService newsService;

    /**
     * 获取新闻列表=
     * @return
     */
    @AuthIgnore
    @GetMapping("/list")
    @ApiOperation(value="获取新闻列表", notes="")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "page", required = true, dataType="integer",paramType="query"),
            @ApiImplicitParam(name = "limit", value = "page", required = true, dataType="integer",paramType="query")
    })
    public R newsList(Integer page,Integer limit){
        page=(page-1)*limit;
        if(page==null||limit==null){
            return R.error("分页参数有误").put("data",null);
        }
        EntityWrapper ew = new EntityWrapper();
        ew.orderBy("news_time",false);
        List<IrNews> newsList = newsService.selectByPage(page, limit);
        List<IrNews> irNews = newsService.selectList(ew);
        Map map=new HashMap();
        map.put("list",newsList);
        map.put("count",irNews.size());
        return R.ok("成功").put("data",map);
    }

    /**
     * 获取新闻详情（ID）
     * @param newsId
     * @return
     */
    @AuthIgnore
    @GetMapping("/detail")
    @ApiImplicitParam(name = "newsId", value = "新闻ID", required = true, dataType="integer",paramType="query")
    @ApiOperation(value="根据新闻ID获取新闻详情", notes="")
    public R newsDetail(Long newsId){
        IrNews irNews = newsService.selectById(newsId);

        return R.ok().put("data",irNews);
    }
	
}
