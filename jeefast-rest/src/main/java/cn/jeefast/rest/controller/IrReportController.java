package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrReport;
import cn.jeefast.common.entity.IrReportUpload;
import cn.jeefast.common.utils.R;
import cn.jeefast.common.validator.Assert;
import cn.jeefast.rest.annotation.AuthIgnore;
import cn.jeefast.rest.entity.TbUser;
import cn.jeefast.rest.service.IrReportService;
import cn.jeefast.rest.service.IrReportUploadService;
import cn.jeefast.rest.service.TbUserService;
import cn.jeefast.rest.upload.FileUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-22
 */
@RestController
@RequestMapping("/api/report")
@Api(tags ="中介协会管理")
@CrossOrigin
public class IrReportController {

    @Autowired
    private IrReportService reportService;

    @Autowired
    private IrReportUploadService uploadService;

    @Autowired
    private TbUserService tbUserService;

    @GetMapping("/reportList")
    @ApiOperation(value="模板列表", notes="")
    public R reportList(Integer page,Integer limit){
        limit = limit == null ? 10 : limit;
        page = page == null ? 0 : (page-1)*limit;
        List<IrReport> irReports = reportService.reportListByPage(page, limit);
        List<IrReport> irReports1 = reportService.selectList(new EntityWrapper<>());
        Map map = new HashMap();
        map.put("list",irReports);
        map.put("count",irReports1.size());
        return R.ok("成功").put("data",map);
    }

//    @PostMapping("uploadReport")
//    public R uploadReport(Long irrId, String path, HttpServletRequest request){
//        IrReportUpload irReportUpload = new IrReportUpload();
//        irReportUpload.setUploadIrrId(irrId);
//        irReportUpload.setUploadCreationTime(new Date());
//        irReportUpload.setUploadPath(path);
//        irReportUpload.setUploadUserId( (Long) request.getAttribute("userId"));
//
//        return R.ok("成功").put("data",null);
//    }
    @PostMapping("uploadReport")
    public R uploadReport(Long irrId, MultipartFile file,HttpServletRequest request){
        IrReportUpload irReportUpload = new IrReportUpload();
        irReportUpload.setUploadIrrId(irrId);
        irReportUpload.setUploadCreationTime(new Date());
        String path= FileUtil.oneFileByFileName(file, "irReport"+irrId);
        irReportUpload.setUploadPath(path);
        irReportUpload.setUploadUserId( (Long) request.getAttribute("userId"));
        uploadService.insert(irReportUpload);
        return R.ok("成功").put("data",null);
    }
    /**
     * @Title: 下载行业协会报表
     * @author: lzl
     * @Description:
     * @param
     * @return
     */
    @GetMapping("/downLoadReport")
    @ApiOperation(value="下载行业协会报表", notes="")
    public R downLoadModel(String iirId,HttpServletResponse response) {
        Assert.isBlank(iirId,"id为空");
        IrReport irReport = reportService.selectById(iirId);
        return R.ok("成功").put("data",irReport);
    }

    @GetMapping("getPower")
    @ApiOperation(value="检查是否有权限", notes="")
    public R power(HttpServletRequest request){
        Long userId = (Long) request.getAttribute("userId");
        TbUser tbUser = tbUserService.selectById(userId);
        if(tbUser.getPower()==0)
            return R.error("无权进入").put("data",null);
        else
            return R.ok("允许").put("data",null);
    }
}