package cn.jeefast.rest.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 模板上传记录表 前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-03-12
 */
@RestController
@RequestMapping("/irReportUpload")
@Api(tags ="自动生成信息")
public class IrReportUploadController {
	
}
