package cn.jeefast.rest.controller;


import cn.jeefast.common.entity.IrWheel;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.service.IrWheelService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
@RestController
@RequestMapping("/api/wheel")
@Api(tags ="轮播图")
@CrossOrigin
public class IrWheelController {
    @Autowired
	private IrWheelService wheelService;

    /**
     * 根据分类获取轮播图片
     * @param typeId
     * @return
     */
    @GetMapping("/wheelList")
    @ApiOperation(value="根据分类获取轮播图片集合", notes="1:首页轮播图 2：监管政策法规轮播图 3：保险超级课堂轮播图  4：保险公司热销产品轮播图")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "1:首页轮播图 2：监管政策法规轮播图 3：保险超级课堂轮播图  4：保险公司热销产品轮播图", required = true, dataType="integer",paramType="query")
    })
    public R wheelList(Integer typeId){
        if(typeId==null){
            return  R.error("类别ID不能为空").put("data",null);
        }

        EntityWrapper ew = new EntityWrapper();
        ew.eq("wheel_tyle",typeId);
        ew.orderBy("wheel_index");
        List<IrWheel> list = wheelService.selectList(ew);
        return R.ok("成功").put("data",list);
    }
}
