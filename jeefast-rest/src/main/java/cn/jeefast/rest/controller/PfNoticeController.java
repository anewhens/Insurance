package cn.jeefast.rest.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统通知 前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
@RestController
@RequestMapping("/pfNotice")
@CrossOrigin
public class PfNoticeController {
	
}
