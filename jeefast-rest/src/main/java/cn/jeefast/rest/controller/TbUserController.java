package cn.jeefast.rest.controller;

import cn.jeefast.common.utils.R;
import cn.jeefast.rest.entity.TbUser;
import cn.jeefast.rest.service.TbUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户相关接口
 */

@RestController
@RequestMapping("/api/user")
@Api(tags ="用户个人相关接口")
@CrossOrigin
public class TbUserController {

    @Autowired
    private TbUserService userService;

    /**
     * 更新用户密码
     * @param oldPass
     * @param newPass
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "oldPass", value = "旧密码", required = true, dataType="string",paramType="query"),
            @ApiImplicitParam(name = "newPass", value = "新密码，需要前端验证两遍是否一致", required = true, dataType="string",paramType="query")
    })
    @PostMapping("/updatePass")
    @ApiOperation(value="修改密码（原密码）", notes="H5-修改个人密码")
    public R updatePass(String oldPass, String newPass, HttpServletRequest request){
        Long userId = (Long) request.getAttribute("userId");
        return userService.updatePass(oldPass, newPass, userId);
    }


    /**
     * 继续教育报名个人信息获取
     * @param request
     * @return
     */
    @GetMapping("/userInfoForEdu")
    @ApiOperation(value="继续教育报名获取个人基本信息", notes="")
    public R userInfoForEdu(HttpServletRequest request){
        Long userId = (Long)request.getAttribute("userId");
        TbUser tbUser = userService.selectById(userId);

        tbUser.setPassword(null);
        return R.ok("OK").put("data",tbUser);

    }

    @PostMapping("/updateInfo")
    @ApiOperation(value="更新个人信息", notes="")
    public R update(TbUser user,HttpServletRequest request){
        Long userId = (Long)request.getAttribute("userId");
        user.setUserId(userId);
        userService.updateById(user);
        return R.ok("更新成功").put("data",null);
    }
}
