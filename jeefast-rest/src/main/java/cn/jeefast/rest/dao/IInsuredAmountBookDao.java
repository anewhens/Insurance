package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IInsuredAmountBook;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
public interface IInsuredAmountBookDao extends BaseMapper<IInsuredAmountBook> {
    List<Map> selectPage(@Param("userId")Long userId, @Param("page")Integer page, @Param("limit")Integer limit);
}