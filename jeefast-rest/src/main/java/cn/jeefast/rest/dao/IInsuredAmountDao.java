package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IInsuredAmount;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
public interface IInsuredAmountDao extends BaseMapper<IInsuredAmount> {

    /**
     * 分页
     * @param iInsuredAmount
     * @return
     */
    List<Map> irList(@Param("iiaComp")Long iiaComp,@Param("iiaType")Integer iiaType,@Param("iiaTitle")String iiaTitle,@Param("page")Integer page,@Param("limit")Integer limit);
}