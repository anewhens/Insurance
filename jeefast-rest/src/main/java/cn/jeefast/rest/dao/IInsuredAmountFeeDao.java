package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IInsuredAmountFee;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
public interface IInsuredAmountFeeDao extends BaseMapper<IInsuredAmountFee> {
}