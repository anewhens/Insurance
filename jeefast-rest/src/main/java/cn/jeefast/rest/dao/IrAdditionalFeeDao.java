package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrAdditionalFee;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
public interface IrAdditionalFeeDao extends BaseMapper<IrAdditionalFee> {

}