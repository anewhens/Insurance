package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrBank;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-13
 */
public interface IrBankDao extends BaseMapper<IrBank> {
    /**
     *  获取省行信息
     * @param
     * @author lzl
     */
    List<IrBank> selectByPage(@Param("page") Integer page, @Param("limit")Integer limit);
    /**
     *  获取某个省行信息
     * @param
     * @author lzl
     */
    List<IrBank> selectByPage2(@Param("parentId") Integer parentId,@Param("page") Integer page, @Param("limit")Integer limit);

}