package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrClassNews;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
public interface IrClassNewsDao extends BaseMapper<IrClassNews> {


    /**
     * 分页
     * @param page
     * @param limit
     * @return
     */
    List<IrClassNews> classNewsByPage(@Param("page")Integer page,@Param("limit")Integer limit,@Param("title")String title,@Param("type")Integer type);

    /**
     * 获取阅读总人数
     * @return
     */
    Integer clssNewsClickCount(@Param("title")String title,@Param("type")Integer type);
}