package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrClassVideo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-24
 */
public interface IrClassVideoDao extends BaseMapper<IrClassVideo> {
    /**
     * 分页列表
     * @param cpId
     * @param itId
     * @return
     */
    List<Map> selectListByPage(@Param("cpId")Long cpId, @Param("itId")Long itId, @Param("limit")Integer limit, @Param("page")Integer page, @Param("bankId")Long bankId, @Param("userId")Long userId);
    /**
     * 获取总个数
     * @param cpId
     * @param itId
     * @return
     */
    Integer getNumber(@Param("cpId")Long cpId, @Param("itId")Long itId, @Param("bankId")Long provinceBank);
}