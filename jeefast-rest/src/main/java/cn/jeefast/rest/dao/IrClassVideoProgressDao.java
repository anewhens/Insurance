package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrClassVideoProgress;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-24
 */
public interface IrClassVideoProgressDao extends BaseMapper<IrClassVideoProgress> {

    List<Map> selectPage(@Param("userId")Long userId,@Param("page")Integer page,@Param("limit")Integer limit);
}