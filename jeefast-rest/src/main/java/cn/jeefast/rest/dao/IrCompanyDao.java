package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrCompany;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-20
 */
public interface IrCompanyDao extends BaseMapper<IrCompany> {

}