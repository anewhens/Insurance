package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrDict;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
public interface IrDictDao extends BaseMapper<IrDict> {

}