package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrEduExam;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.sun.corba.se.spi.ior.ObjectKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-18
 */
public interface IrEduExamDao extends BaseMapper<IrEduExam> {

    List<Map<String, Object>> listByPage(@Param("userId")Long userId,@Param("page")Integer page, @Param("limit")Integer limit, @Param("typeId")Integer typeid, @Param("etId")Long etId, @Param("isDt")Integer isDt);
    /**
     *  获取总个数
     * @param
     * @author lzl
     */
    Integer getNumber( @Param("typeId")Integer typeid,
                       @Param("etId")Long etId,
                       @Param("isDt")Integer isDt);
}