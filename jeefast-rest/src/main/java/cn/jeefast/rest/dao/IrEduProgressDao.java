package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrEduProgress;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-18
 */
public interface IrEduProgressDao extends BaseMapper<IrEduProgress> {

    /**
     * 查询用户成绩
     * @param userId
     * @return
     */
    List<Map<String,Object>> selectExamResult(@Param("userId")Long userId);

    /**
     * 查询用户成绩
     * @param userId
     * @return
     */
    List<Map<String,Object>> selectExamResultPc(@Param("userId")Long userId,@Param("page") Integer page,@Param("limit") Integer limit);

    /**
     * 根据批次查询成绩详情
     * @param userId
     * @param etId
     * @return
     */
    List<Map<String,Object>> selectExamScore(@Param("userId")Long userId,@Param("etId")Long etId);

    /**
     * 考试任务列表+分页
     * @param page
     * @param limit
     * @return
     */
    List<Map<String,Object>> examTask(@Param("userId")Long userId,@Param("page")Integer page,@Param("limit")Integer limit);
}