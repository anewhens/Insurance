package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrEduTime;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-17
 */
public interface IrEduTimeDao extends BaseMapper<IrEduTime> {

}