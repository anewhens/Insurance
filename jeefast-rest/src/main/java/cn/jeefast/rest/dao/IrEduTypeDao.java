package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrEduType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-18
 */
public interface IrEduTypeDao extends BaseMapper<IrEduType> {

}