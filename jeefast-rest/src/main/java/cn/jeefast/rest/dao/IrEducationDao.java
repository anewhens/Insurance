package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrEducation;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-16
 */
public interface IrEducationDao extends BaseMapper<IrEducation> {

}