package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrInsuranceNews;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-03-09
 */
public interface IrInsuranceNewsDao extends BaseMapper<IrInsuranceNews> {

    List<IrInsuranceNews> newsList(@Param("desc")Integer desc,@Param("page")Integer page,@Param("limit")Integer limit);
}