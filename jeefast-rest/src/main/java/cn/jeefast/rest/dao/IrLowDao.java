package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrLow;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
public interface IrLowDao extends BaseMapper<IrLow> {


    /**
     * 分页获取法律文章
     * @param page
     * @param limit
     * @return
     */
    List<IrLow> listByPage(@Param("page")Integer page,@Param("limit")Integer limit);
}