package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrMsgLunbo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 首页消息轮播 Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-16
 */
public interface IrMsgLunboDao extends BaseMapper<IrMsgLunbo> {

}