package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrNews;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
public interface IrNewsDao extends BaseMapper<IrNews> {
    /**
     * 获取信息
     * @param
     * @author lzl
     */
    List<IrNews> selectByPage(@Param("page") Integer page, @Param("limit") Integer limit);
}