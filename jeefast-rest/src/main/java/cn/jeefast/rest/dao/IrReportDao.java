package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrReport;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-22
 */
public interface IrReportDao extends BaseMapper<IrReport> {

    /**
     * 分页
     * @param page
     * @param limit
     * @return
     */
    List<IrReport> reportListByPage(@Param("page")Integer page,@Param("limit")Integer limit);
}