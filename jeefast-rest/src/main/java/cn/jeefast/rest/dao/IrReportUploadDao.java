package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrReportUpload;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 模板上传记录表 Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-03-12
 */
public interface IrReportUploadDao extends BaseMapper<IrReportUpload> {

}