package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.IrWheel;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
public interface IrWheelDao extends BaseMapper<IrWheel> {

}