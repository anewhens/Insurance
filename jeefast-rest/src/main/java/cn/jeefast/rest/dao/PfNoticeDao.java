package cn.jeefast.rest.dao;

import cn.jeefast.common.entity.PfNotice;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 系统通知 Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
public interface PfNoticeDao extends BaseMapper<PfNotice> {

}