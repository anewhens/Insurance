package cn.jeefast.rest.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 用户
 * </p>
 *
 * @author theodo
 * @since 2017-10-28
 */
@ApiModel(value = "tbUser", description = "用户")
@TableName("tb_user")
public class TbUser extends Model<TbUser> {

    private static final long serialVersionUID = 1L;

	@TableId(value="user_id", type= IdType.AUTO)
	@ApiModelProperty(name="userId", value="用户ID")
	private Long userId;
    /**
     * 用户名
     */
	@ApiModelProperty(name="username", value="用户姓名")
	private String username;
    /**
     * 手机号
     */
	@ApiModelProperty(name="mobile", value="手机号")
	private String mobile;
    /**
     * 密码
     */
	@ApiModelProperty(name="password", value="密码")
	private String password;
    /**
     * 创建时间
     */
	@TableField("create_time")
	@ApiModelProperty(name="createTime", value="创建时间，不用填写")
	private Date createTime;

	/**
	 * 性别
	 */
	@ApiModelProperty(name="sex", value="性别 1：男 0：女")
	private Integer sex;

	@TableField("id_card")
	@ApiModelProperty(name="idCard", value="18位身份证号")
	private String idCard;

	@TableField("province_bank")
	@ApiModelProperty(name="provinceBank", value="省行（ID）")
	private Long provinceBank;

	@TableField("city_bank")
	@ApiModelProperty(name="cityBank", value="市行（ID）")
	private Long cityBank;

	@TableField("sub_bank")
	@ApiModelProperty(name="subBank", value="支行名称")
	private String subBank;

	@ApiModelProperty(name="location", value="网点名称")
	private String location;

	@ApiModelProperty(name="head", value="头像")
	private String head;

	@ApiModelProperty(name="birth", value="生日")
	private String birth;


	private Integer power;

	public Integer getPower() {
		return power;
	}

	public void setPower(Integer power) {
		this.power = power;
	}

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public Long getProvinceBank() {
		return provinceBank;
	}

	public void setProvinceBank(Long provinceBank) {
		this.provinceBank = provinceBank;
	}

	public Long getCityBank() {
		return cityBank;
	}

	public void setCityBank(Long cityBank) {
		this.cityBank = cityBank;
	}

	public String getSubBank() {
		return subBank;
	}

	public void setSubBank(String subBank) {
		this.subBank = subBank;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	protected Serializable pkVal() {
		return this.userId;
	}

	@Override
	public String toString() {
		return "TbUser{" +
			", userId=" + userId +
			", username=" + username +
			", mobile=" + mobile +
			", password=" + password +
			", createTime=" + createTime +
			"}";
	}
}
