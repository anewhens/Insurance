package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IInsuredAmountBook;
import com.baomidou.mybatisplus.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
public interface IInsuredAmountBookService extends IService<IInsuredAmountBook> {


    List<Map> selectPage(Long userId, Integer page, Integer limit);
}
