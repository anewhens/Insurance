package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IInsuredAmount;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
public interface IInsuredAmountService extends IService<IInsuredAmount> {
    /**
     * 分页
     * @param
     * @return
     */
    List<Map> irList(Long iiaComp,Integer iiaType,String iiaTitle,Integer page,Integer limit);
}
