package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrAdditionalFee;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
public interface IrAdditionalFeeService extends IService<IrAdditionalFee> {
	
}
