package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrAdditional;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
public interface IrAdditionalService extends IService<IrAdditional> {
	
}
