package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrBank;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-13
 */
public interface IrBankService extends IService<IrBank> {
    /**
     * 获取银行信息
     * @param
     * @author lzl
     */
    List<IrBank> selectByPage(Integer page, Integer limit);
    /**
     * 获取某个省行信息
     * @param
     * @author lzl
     */
    List<IrBank> selectByPage2(Integer parentId,Integer page, Integer limit);
	
}
