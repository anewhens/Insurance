package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrClassNews;
import com.baomidou.mybatisplus.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
public interface IrClassNewsService extends IService<IrClassNews> {
    /**
     * 分页
     * @param page
     * @param limit
     * @return
     */
    List<IrClassNews> classNewsByPage(Integer page, Integer limit,String title,Integer type);

    /**
     * 获取阅读总人数
     * @return
     */
    Integer clssNewsClickCount(String title,Integer type);
}
