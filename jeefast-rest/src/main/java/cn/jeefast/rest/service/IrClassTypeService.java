package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrClassType;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-20
 */
public interface IrClassTypeService extends IService<IrClassType> {
	
}
