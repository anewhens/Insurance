package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrClassVideoProgress;
import com.baomidou.mybatisplus.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-24
 */
public interface IrClassVideoProgressService extends IService<IrClassVideoProgress> {
    List<Map> selectPage(Long userId, Integer page, Integer limit);
}
