package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrClassVideo;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-24
 */
public interface IrClassVideoService extends IService<IrClassVideo> {
    /**
     * 分页列表
     * @param cpId
     * @param itId
     * @return
     */
    List<Map> selectListByPage(Long cpId, Long itId, Integer limit, Integer page, Long bankId, Long userId);
    /**
     * 获取总个数
     * @param cpId
     * @param itId
     * @return
     */
    Integer getNumber(Long cpId, Long itId, Long provinceBank);
}
