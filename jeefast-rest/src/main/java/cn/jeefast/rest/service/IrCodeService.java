package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrCode;
import cn.jeefast.common.utils.R;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
public interface IrCodeService extends IService<IrCode> {

    /**
     * 发送验证码
     * @param phone
     * @return
     */
    R sendCode(String phone);

    /**
     * 验证验证码
     * @param phone
     * @return
     */
    R checkCode(String phone,String code);
}
