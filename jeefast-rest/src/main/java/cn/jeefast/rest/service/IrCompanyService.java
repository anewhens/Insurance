package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrCompany;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-20
 */
public interface IrCompanyService extends IService<IrCompany> {
	
}
