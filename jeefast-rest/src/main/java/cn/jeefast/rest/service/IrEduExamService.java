package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrEduExam;
import cn.jeefast.common.utils.R;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-18
 */
public interface IrEduExamService extends IService<IrEduExam> {

    /**
     * 根据类别ID获取最新一期的考试列表
     * @param typeId
     * @param page
     * @param limit
     * @return
     */
    R examList(Integer typeId,Integer page,Integer limit,Long userId);
}
