package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrEduProgress;
import cn.jeefast.common.utils.R;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-18
 */
public interface IrEduProgressService extends IService<IrEduProgress> {
    /**
     * 查询用户成绩
     * @param userId
     * @return
     */
    R selectExamResult(Long userId);

    R selectExamResultPc(Long userId,Integer page,Integer limit);


    /**
     * 查询批次下用户成绩
     * @param userId
     * @param etId
     * @return
     */
    R selectExamScore(Long userId,Long etId);

    /**
     * 考试任务列表+分页
     * @param page
     * @param limit
     * @return
     */
    List<Map<String,Object>> examTask(Long userId,Integer page, Integer limit);

    /**
     * 是否可以进入考试
     * @param userId
     * @return
     */
    R canExam(Long userId);
}
