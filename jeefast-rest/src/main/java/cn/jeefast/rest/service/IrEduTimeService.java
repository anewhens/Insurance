package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrEduTime;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-17
 */
public interface IrEduTimeService extends IService<IrEduTime> {
    /**
     * 查询最新期的ID
     * @return
     */
    Long  lastSignId();
}
