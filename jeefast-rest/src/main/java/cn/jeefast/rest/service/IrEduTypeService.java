package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrEduType;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-18
 */
public interface IrEduTypeService extends IService<IrEduType> {
	
}
