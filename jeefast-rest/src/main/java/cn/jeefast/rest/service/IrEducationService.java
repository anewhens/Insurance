package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrEducation;
import cn.jeefast.common.utils.R;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-16
 */
public interface IrEducationService extends IService<IrEducation> {

    /**
     * 报名
     * @param education
     * @return
     */
    R signInExam(IrEducation education);
	
}
