package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrFeedback;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
public interface IrFeedbackService extends IService<IrFeedback> {
	
}
