package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrInsuranceNews;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-03-09
 */
public interface IrInsuranceNewsService extends IService<IrInsuranceNews> {


    List<IrInsuranceNews> newsList(Integer desc, Integer page, Integer limit);
}
