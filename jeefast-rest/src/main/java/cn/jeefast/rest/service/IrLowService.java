package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrLow;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
public interface IrLowService extends IService<IrLow> {
    /**
     * 分页获取法律文章
     * @param page
     * @param limit
     * @return
     */
    List<IrLow> listByPage(Integer page, Integer limit);
}
