package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrMsgLunbo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 首页消息轮播 服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-16
 */
public interface IrMsgLunboService extends IService<IrMsgLunbo> {
	
}
