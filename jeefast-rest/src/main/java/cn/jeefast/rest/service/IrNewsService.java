package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrNews;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
public interface IrNewsService extends IService<IrNews> {
    /**
     *  获取信息
     * @param
     * @author lzl
     */
    List<IrNews> selectByPage(Integer page, Integer limit);
}
