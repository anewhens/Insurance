package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrReport;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-22
 */
public interface IrReportService extends IService<IrReport> {

    /**
     * 分页
     * @param page
     * @param limit
     * @return
     */
    List<IrReport> reportListByPage(Integer page, Integer limit);
}
