package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrReportUpload;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 模板上传记录表 服务类
 * </p>
 *
 * @author theodo
 * @since 2020-03-12
 */
public interface IrReportUploadService extends IService<IrReportUpload> {
	
}
