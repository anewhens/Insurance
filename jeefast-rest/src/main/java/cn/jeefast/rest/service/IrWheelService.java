package cn.jeefast.rest.service;

import cn.jeefast.common.entity.IrWheel;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
public interface IrWheelService extends IService<IrWheel> {
	
}
