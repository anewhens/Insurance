package cn.jeefast.rest.service;

import cn.jeefast.common.entity.PfNotice;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 系统通知 服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
public interface PfNoticeService extends IService<PfNotice> {
	
}
