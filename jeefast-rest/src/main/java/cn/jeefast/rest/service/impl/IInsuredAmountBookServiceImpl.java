package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IInsuredAmountBook;
import cn.jeefast.rest.dao.IInsuredAmountBookDao;
import cn.jeefast.rest.service.IInsuredAmountBookService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
@Service
public class IInsuredAmountBookServiceImpl extends ServiceImpl<IInsuredAmountBookDao, IInsuredAmountBook> implements IInsuredAmountBookService {

    @Autowired
    private IInsuredAmountBookDao iInsuredAmountBookDao;

    @Override
    public List<Map> selectPage(Long userId, Integer page, Integer limit) {
        return iInsuredAmountBookDao.selectPage(userId,page,limit);
    }
}
