package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IInsuredAmountFee;
import cn.jeefast.rest.dao.IInsuredAmountFeeDao;
import cn.jeefast.rest.service.IInsuredAmountFeeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
@Service
public class IInsuredAmountFeeServiceImpl extends ServiceImpl<IInsuredAmountFeeDao, IInsuredAmountFee> implements IInsuredAmountFeeService {
	
}
