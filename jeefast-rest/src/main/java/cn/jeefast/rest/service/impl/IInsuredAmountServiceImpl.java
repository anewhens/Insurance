package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IInsuredAmount;
import cn.jeefast.common.entity.IrCompany;
import cn.jeefast.rest.dao.IInsuredAmountDao;
import cn.jeefast.rest.service.IInsuredAmountService;
import cn.jeefast.rest.service.IrCompanyService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
@Service
public class IInsuredAmountServiceImpl extends ServiceImpl<IInsuredAmountDao, IInsuredAmount> implements IInsuredAmountService {

    @Autowired
    private IInsuredAmountDao iInsuredAmountDao;

    @Autowired
    private IrCompanyService companyService;

    @Override
    public List<Map> irList(Long iiaComp,Integer iiaType,String iiaTitle,Integer page,Integer limit) {
        List<Map> maps = iInsuredAmountDao.irList(iiaComp, iiaType, iiaTitle, page, limit);
        for (Map m:maps) {
            IrCompany iia_comp = companyService.selectById((Long) m.get("iia_comp"));
            if(iia_comp!=null) {
                m.put("iia_comp_name", iia_comp.getIcTitle());
            }
        }
        return maps;
    }
}
