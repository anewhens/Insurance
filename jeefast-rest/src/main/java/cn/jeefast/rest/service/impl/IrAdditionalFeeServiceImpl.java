package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrAdditionalFee;
import cn.jeefast.rest.dao.IrAdditionalFeeDao;
import cn.jeefast.rest.service.IrAdditionalFeeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
@Service
public class IrAdditionalFeeServiceImpl extends ServiceImpl<IrAdditionalFeeDao, IrAdditionalFee> implements IrAdditionalFeeService {
	
}
