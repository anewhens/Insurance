package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrAdditional;
import cn.jeefast.rest.dao.IrAdditionalDao;
import cn.jeefast.rest.service.IrAdditionalService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-03-07
 */
@Service
public class IrAdditionalServiceImpl extends ServiceImpl<IrAdditionalDao, IrAdditional> implements IrAdditionalService {
	
}
