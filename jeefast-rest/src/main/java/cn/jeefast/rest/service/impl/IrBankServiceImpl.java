package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrBank;
import cn.jeefast.rest.dao.IrBankDao;
import cn.jeefast.rest.service.IrBankService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-13
 */
@Service
public class IrBankServiceImpl extends ServiceImpl<IrBankDao, IrBank> implements IrBankService {
    @Resource
    private IrBankDao irBankDao;

    @Override
    public List<IrBank> selectByPage(Integer page, Integer limit) {
        return irBankDao.selectByPage(page,limit);
    }
    @Override
    public List<IrBank> selectByPage2(Integer parentId,Integer page, Integer limit) {
        return irBankDao.selectByPage2(parentId,page,limit);
    }
	
}
