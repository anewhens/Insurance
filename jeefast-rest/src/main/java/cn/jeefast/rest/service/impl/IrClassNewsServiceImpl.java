package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrClassNews;
import cn.jeefast.rest.dao.IrClassNewsDao;
import cn.jeefast.rest.service.IrClassNewsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
@Service
public class IrClassNewsServiceImpl extends ServiceImpl<IrClassNewsDao, IrClassNews> implements IrClassNewsService {

    @Autowired
    private IrClassNewsDao classNewsDao;

    @Override
    public List<IrClassNews> classNewsByPage(Integer page, Integer limit,String title,Integer type) {
        return classNewsDao.classNewsByPage(page,limit,title,type);
    }

    @Override
    public Integer clssNewsClickCount(String title,Integer type) {
        return classNewsDao.clssNewsClickCount(title,type);
    }
}
