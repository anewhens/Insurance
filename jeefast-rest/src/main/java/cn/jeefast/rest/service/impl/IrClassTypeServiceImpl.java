package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrClassType;
import cn.jeefast.rest.dao.IrClassTypeDao;
import cn.jeefast.rest.service.IrClassTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-20
 */
@Service
public class IrClassTypeServiceImpl extends ServiceImpl<IrClassTypeDao, IrClassType> implements IrClassTypeService {
	
}
