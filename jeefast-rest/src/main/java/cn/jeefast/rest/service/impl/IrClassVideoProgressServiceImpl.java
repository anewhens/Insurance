package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrClassVideoProgress;
import cn.jeefast.rest.dao.IrClassVideoProgressDao;
import cn.jeefast.rest.service.IrClassVideoProgressService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-24
 */
@Service
public class IrClassVideoProgressServiceImpl extends ServiceImpl<IrClassVideoProgressDao, IrClassVideoProgress> implements IrClassVideoProgressService {

    @Autowired
    private IrClassVideoProgressDao irClassVideoProgressDao;

    @Override
    public List<Map> selectPage(Long userId, Integer page, Integer limit) {
        return irClassVideoProgressDao.selectPage(userId, page, limit);
    }
}
