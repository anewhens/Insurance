package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrClassVideo;
import cn.jeefast.common.entity.IrClassVideoProgress;
import cn.jeefast.rest.dao.IrClassVideoDao;
import cn.jeefast.rest.service.IrClassVideoProgressService;
import cn.jeefast.rest.service.IrClassVideoService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-24
 */
@Service
public class IrClassVideoServiceImpl extends ServiceImpl<IrClassVideoDao, IrClassVideo> implements IrClassVideoService {

    @Autowired
    private IrClassVideoDao classVideoDao;
    @Autowired
    private IrClassVideoProgressService classVideoProgressService;

    @Override
    public List<Map> selectListByPage(Long cpId, Long itId, Integer limit, Integer page, Long bankId, Long userId) {
        List<Map> maps = classVideoDao.selectListByPage(cpId, itId, limit, page, bankId, userId);
        for (Map m:maps) {
            EntityWrapper ew = new EntityWrapper();
            ew.eq("user_id",userId);
            ew.eq("cv_id",m.get("cv_id"));
            List<IrClassVideoProgress> list = classVideoProgressService.selectList(ew);
            if(list.size()==0){
                m.put("cvp_status",0);
            }else{
                m.put("cvp_status",list.get(0).getCvpStatus());
            }
        }
        return maps;
    }

    @Override
    public Integer getNumber(Long cpId, Long itId, Long provinceBank) {
        return classVideoDao.getNumber(cpId,itId,provinceBank);
    }
}
