package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrCompany;
import cn.jeefast.rest.dao.IrCompanyDao;
import cn.jeefast.rest.service.IrCompanyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-20
 */
@Service
public class IrCompanyServiceImpl extends ServiceImpl<IrCompanyDao, IrCompany> implements IrCompanyService {
	
}
