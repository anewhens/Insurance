package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrDict;
import cn.jeefast.rest.dao.IrDictDao;
import cn.jeefast.rest.service.IrDictService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
@Service
public class IrDictServiceImpl extends ServiceImpl<IrDictDao, IrDict> implements IrDictService {
	
}
