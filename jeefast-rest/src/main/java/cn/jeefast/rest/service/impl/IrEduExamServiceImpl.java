package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrEduExam;
import cn.jeefast.common.entity.IrEduTime;
import cn.jeefast.common.entity.IrEducation;
import cn.jeefast.rest.dao.IrEducationDao;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.dao.IrEduExamDao;
import cn.jeefast.rest.service.IrEduExamService;
import cn.jeefast.rest.service.IrEduTimeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-18
 */
@Service
public class IrEduExamServiceImpl extends ServiceImpl<IrEduExamDao, IrEduExam> implements IrEduExamService {

    @Autowired
    private IrEduTimeService eduTimeService;
    @Autowired
    private IrEduExamDao eduExamDao;
    @Autowired
    private IrEducationDao educationDao;

    @Override
    public R examList(Integer typeId,Integer page,Integer limit,Long userId) {
        Long aLong = eduTimeService.lastSignId();
        if(aLong==-1){
            return R.error("无考试信息，请联系管理员创建").put("data",null);
        }

        //判断是否报名
        EntityWrapper ew = new EntityWrapper();
        ew.eq("et_id",aLong);
        ew.eq("user_id",userId);
        List<IrEducation> eduList = educationDao.selectList(ew);
        if(eduList.size()!=1){
            return R.error("未报名").put("data",null);
        }

        //判断是否在考试时间内

        IrEduTime irEduTime = eduTimeService.selectById(aLong);

        long now = new Date().getTime();

        //判断是否到达考试 时间
        if(now<irEduTime.getEtExamStart().getTime()||now>irEduTime.getEtExamEnd().getTime()){
            return R.error("当前不在考试时间范围").put("data",null);
        }
        //查询试题
        List<Map<String,Object>> irEduExams = eduExamDao.listByPage(userId,page, limit, typeId, aLong,eduList.get(0).getIeIsDt());
        for (Map m:irEduExams) {
            if((Long)m.get("reExam")!=1L){
                //允许重做，判断是否播放完成
                if(((String)(m.get("videoProgress"))).equals("-1")){
                    m.put("canDo",true);
                }else{
                    m.put("canDo",false);
                }
            }else{
                m.put("canDo",false);
            }
        }
        Integer number = eduExamDao.getNumber(typeId, aLong,eduList.get(0).getIeIsDt());
        Map map=new HashMap();
        map.put("list",irEduExams);
        map.put("count",number);
        return R.ok("success").put("data",map);
    }
}
