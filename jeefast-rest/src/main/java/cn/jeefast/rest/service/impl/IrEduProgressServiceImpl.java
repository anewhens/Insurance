package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrEduExam;
import cn.jeefast.common.entity.IrEduProgress;
import cn.jeefast.common.entity.IrEduTime;
import cn.jeefast.common.entity.IrEducation;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.dao.IrEduExamDao;
import cn.jeefast.rest.dao.IrEduProgressDao;
import cn.jeefast.rest.dao.IrEduTimeDao;
import cn.jeefast.rest.dao.IrEducationDao;
import cn.jeefast.rest.service.IrEduProgressService;
import cn.jeefast.rest.service.IrEduTimeService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-18
 */
@Service
public class IrEduProgressServiceImpl extends ServiceImpl<IrEduProgressDao, IrEduProgress> implements IrEduProgressService {

    @Autowired
    private IrEduProgressDao eduProgressDao;

    @Autowired
    private IrEducationDao educationDao;

    @Autowired
    private IrEduExamDao eduExamDao;

    @Autowired
    private IrEduTimeDao eduTimeDao;

    @Autowired
    private IrEduTimeService eduTimeService;

    @Autowired
    private IrEduProgressService eduProgressService;

    @Override
    public R selectExamResult(Long userId) {
        //结果集
        List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();

        List<Map<String, Object>> maps = eduProgressDao.selectExamResult(userId);

        for (Map m:maps) {
            //结果Map
            Map res = new HashMap();
            //查询报名信息
            EntityWrapper ew = new EntityWrapper();
            ew.eq("et_id",m.get("et_id"));
            ew.eq("user_id",userId);
            List<IrEducation> list = educationDao.selectList(ew);
            if(list.size()==0){
                return R.error("无法获取报名信息").put("data",null);
            }
            IrEducation irEducation = list.get(0);
            IrEduTime et_id = eduTimeDao.selectById(irEducation.getEtId());
            if(et_id!=null)
                res.put("endTime",et_id.getEtExamEnd());
            else
                res.put("endTime","");
            res.put("etId",m.get("et_id"));
            res.put("score",m.get("sum_score"));
//            if((Integer) m.get("isEnd")==0){
                //标识考试已过期
                //判断是否为投连考试
                if(irEducation.getIeIsDt()==0){
                    //非投连考试，满分100分
                    if(((BigDecimal)m.get("sum_score")).intValue()>=60){
                        //判断是否把视频全部看完
                        if(isFinish((Long)m.get("et_id"),0,userId)){
                            res.put("result","已通过");
                        }else{
                            res.put("result","未通过");
                        }
                    }else{
                        res.put("result","未通过");
                    }
                }else{
                    //投连考试 满分110分
                    if(((BigDecimal)m.get("sum_score")).intValue()>=70){
                        if(isFinish((Long)m.get("et_id"),1,userId)){
                            res.put("result","已通过");
                        }else{
                            res.put("result","未通过");
                        }
                    }else{
                        res.put("result","未通过");
                    }
                }
//            }else{
//                res.put("result","进行中");
//            }
            result.add(res);
        }

        return R.ok("查询成功").put("data",result);
    }

    @Override
    public R selectExamResultPc(Long userId, Integer page, Integer limit) {
        //结果集
        List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();

        List<Map<String, Object>> maps = eduProgressDao.selectExamResultPc(userId,page,limit);


        List<Map<String, Object>> count = eduProgressDao.selectExamResultPc(userId,0,10000);



        for (Map m:maps) {
            //结果Map
            Map res = new HashMap();
            //查询报名信息
            EntityWrapper ew = new EntityWrapper();
            ew.eq("et_id",m.get("et_id"));
            ew.eq("user_id",userId);
            List<IrEducation> list = educationDao.selectList(ew);
            if(list.size()==0){
                return R.error("无法获取报名信息").put("data",null);
            }
            IrEducation irEducation = list.get(0);
            res.put("endTime",m.get("et_exam_end"));
            res.put("etId",m.get("et_id"));
            res.put("score",m.get("sum_score"));
                //标识考试已过期
                //判断是否为投连考试
                if(irEducation.getIeIsDt()==0){
                    //非投连考试，满分100分
                    if(((BigDecimal)m.get("sum_score")).intValue()>=60){
                        if(isFinish((Long)m.get("et_id"),0,userId)){
                            res.put("result","已通过");
                        }else{
                            res.put("result","未通过");
                        }
                    }else{
                        res.put("result","未通过");
                    }
                }else{
                    //投连考试 满分110分
                    if(((BigDecimal)m.get("sum_score")).intValue()>=70){
                        if(isFinish((Long)m.get("et_id"),1,userId)){
                            res.put("result","已通过");
                        }else{
                            res.put("result","未通过");
                        }
                    }else{
                        res.put("result","未通过");
                    }
                }
            result.add(res);
        }

        Map map = new HashMap();
        map.put("list",result);
        map.put("count",count.size());
        return R.ok("查询成功").put("data",map);
    }

    @Override
    public R selectExamScore(Long userId, Long etId) {
        List<Map<String, Object>> maps = eduProgressDao.selectExamScore(userId, etId);
        return R.ok("查询成功").put("data",maps);
    }

    @Override
    public List<Map<String, Object>> examTask(Long userId,Integer page, Integer limit) {
        List<Map<String, Object>> maps = eduProgressDao.examTask(userId,page, limit);
        return maps;
    }

    @Override
    public R canExam(Long userId) {
        //查询最新的的批次
        Long aLong = eduTimeService.lastSignId();
        //根据用户ID查询考试结果集合
        R r = eduProgressService.selectExamResultPc(userId,0,1);
        Map data = (Map) r.get("data");
        if(data==null){
            return R.ok("允许").put("canExam",true);
        }
        List<Map> maps = (List<Map>) data.get("list");
        if(maps==null){
            return R.ok("允许").put("canExam",true);
        }
        if(maps.size()==0){
            //没有考试，则允许
            return R.ok("允许").put("canExam",true);
        }else{
            Long etId = (Long) maps.get(0).get("etId");
            if(aLong==etId){
                String result = (String)maps.get(0).get("result");
                if("已通过".equals(result)){
                    return R.error("不允许").put("canExam",false);
                }else{
                    return R.ok("允许").put("canExam",true);
                }
            }else{
                return R.ok("允许").put("canExam",true);
            }
        }
    }

    /**
     * 检查所在批次视频是否都观看完
     * @return
     */
    public boolean isFinish(Long etId,Integer isDt,Long userId){
        //查询当前批次所有视频
        EntityWrapper ew = new EntityWrapper();
        ew.eq("et_id",etId);
        //如果非投联，只查询非投联试题
        if(isDt==0){
            ew.ne("ee_is_dt",1);
        }
        //查询试题
        List<IrEduExam> list = eduExamDao.selectList(ew);
        //投联用户判断满分是否够了10分。
        if(isDt==1){
            int num =0;
            for (IrEduExam ee:list) {
                if(ee.getEeIsDt()==1){
                    EntityWrapper ew0 = new EntityWrapper();
                    ew0.eq("user_id",userId);
                    ew0.eq("ee_id",ee.getEeId());
                    List<IrEduProgress> list1 = eduProgressDao.selectList(ew0);
                    if(list1.size()==1){
                        num += list1.get(0).getEpScore();
                    }
                }
            }
            if(num<10){
                return false;
            }
        }
        for (IrEduExam ie:list
             ) {
            EntityWrapper ew0 = new EntityWrapper();
            ew0.eq("ee_id",ie.getEeId());
            ew0.eq("user_id",userId);
            List<IrEduProgress> list1 = eduProgressDao.selectList(ew0);
            if(list1.size()==0){
                return false;
            }
            if(!list1.get(0).getEpVideoProgress().equals("-1")){
                return false;
            }
        }
        return true;
    }
}
