package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrEduTime;
import cn.jeefast.rest.dao.IrEduTimeDao;
import cn.jeefast.rest.service.IrEduTimeService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-17
 */
@Service
public class IrEduTimeServiceImpl extends ServiceImpl<IrEduTimeDao, IrEduTime> implements IrEduTimeService {

    @Autowired
    private IrEduTimeDao eduTimeDao;

    /**
     * 获取最新的培训 ID
     * @return
     */
    @Override
    public Long lastSignId() {
        EntityWrapper ew = new EntityWrapper();
        ew.orderBy("et_id",false);
        List<IrEduTime> list = eduTimeDao.selectList(ew);
        if(list.size()==0){
            return -1L;
        }
        return list.get(0).getEtId();
    }
}
