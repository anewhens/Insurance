package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrEduType;
import cn.jeefast.rest.dao.IrEduTypeDao;
import cn.jeefast.rest.service.IrEduTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-18
 */
@Service
public class IrEduTypeServiceImpl extends ServiceImpl<IrEduTypeDao, IrEduType> implements IrEduTypeService {
	
}
