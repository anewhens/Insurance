package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrEduTime;
import cn.jeefast.common.entity.IrEducation;
import cn.jeefast.common.utils.R;
import cn.jeefast.rest.dao.IrEducationDao;
import cn.jeefast.rest.service.IrEduTimeService;
import cn.jeefast.rest.service.IrEducationService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-16
 */
@Service
public class IrEducationServiceImpl extends ServiceImpl<IrEducationDao, IrEducation> implements IrEducationService {

    @Autowired
    private IrEducationService educationService;

    @Autowired
    private IrEduTimeService eduTimeService;

    @Override
    public R signInExam(IrEducation education) {
        //查询最新批次的批次ID
        Long aLong = eduTimeService.lastSignId();
        //判断是否存在批次
        if(aLong==-1){
            return R.error("没有可用的继续教育报名批次").put("data",null);
        }
        education.setEtId(aLong);
        //判断最新批次是否是在报名时间之内
        IrEduTime irEduTime = eduTimeService.selectById(aLong);
        Long now = new Date().getTime();
        //当前时间戳要大于报名开始时间且小于报名结束时间
        if(now<irEduTime.getEtSignStart().getTime()||now>irEduTime.getEtSignEnd().getTime()){
            return R.error("当前不在可报名时间段").put("data",null);
        }
        //查询是否已经报名
        EntityWrapper ew = new EntityWrapper();
        ew.eq("et_id",aLong);
        ew.eq("user_id",education.getUserId());
        List list = educationService.selectList(ew);
        //同一批次不允许重复报名
        if(list.size()!=0){
            return R.error("已报名，无需重复报名").put("data",null);
        }
        educationService.insert(education);
        return R.ok("报名成功").put("data",null);
    }
}
