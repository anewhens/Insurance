package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrFeedback;
import cn.jeefast.rest.dao.IrFeedbackDao;
import cn.jeefast.rest.service.IrFeedbackService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
@Service
public class IrFeedbackServiceImpl extends ServiceImpl<IrFeedbackDao, IrFeedback> implements IrFeedbackService {
	
}
