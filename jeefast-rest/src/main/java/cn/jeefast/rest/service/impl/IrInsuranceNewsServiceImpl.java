package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrInsuranceNews;
import cn.jeefast.rest.dao.IrInsuranceNewsDao;
import cn.jeefast.rest.service.IrInsuranceNewsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-03-09
 */
@Service
public class IrInsuranceNewsServiceImpl extends ServiceImpl<IrInsuranceNewsDao, IrInsuranceNews> implements IrInsuranceNewsService {

    @Autowired
    private IrInsuranceNewsDao irInsuranceNewsDao;

    @Override
    public List<IrInsuranceNews> newsList(Integer desc, Integer page, Integer limit) {
        return irInsuranceNewsDao.newsList(desc,page,limit);
    }
}
