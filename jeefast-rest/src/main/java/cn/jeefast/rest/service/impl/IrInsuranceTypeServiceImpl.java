package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrInsuranceType;
import cn.jeefast.rest.dao.IrInsuranceTypeDao;
import cn.jeefast.rest.service.IrInsuranceTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-20
 */
@Service
public class IrInsuranceTypeServiceImpl extends ServiceImpl<IrInsuranceTypeDao, IrInsuranceType> implements IrInsuranceTypeService {
	
}
