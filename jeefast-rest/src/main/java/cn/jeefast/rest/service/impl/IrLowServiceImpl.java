package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrLow;
import cn.jeefast.rest.dao.IrLowDao;
import cn.jeefast.rest.service.IrLowService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
@Service
public class IrLowServiceImpl extends ServiceImpl<IrLowDao, IrLow> implements IrLowService {

    @Autowired
    private IrLowDao lowDao;

    @Override
    public List<IrLow> listByPage(Integer page, Integer limit) {
        return lowDao.listByPage(page,limit);
    }
}
