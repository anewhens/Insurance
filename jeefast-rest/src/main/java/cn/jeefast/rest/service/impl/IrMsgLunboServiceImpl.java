package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrMsgLunbo;
import cn.jeefast.rest.dao.IrMsgLunboDao;
import cn.jeefast.rest.service.IrMsgLunboService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 首页消息轮播 服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-16
 */
@Service
public class IrMsgLunboServiceImpl extends ServiceImpl<IrMsgLunboDao, IrMsgLunbo> implements IrMsgLunboService {
	
}
