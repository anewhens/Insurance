package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrNews;
import cn.jeefast.rest.dao.IrNewsDao;
import cn.jeefast.rest.service.IrNewsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
@Service
public class IrNewsServiceImpl extends ServiceImpl<IrNewsDao, IrNews> implements IrNewsService {
    @Resource
    private IrNewsDao irNewsDao;

    @Override
    public List<IrNews> selectByPage(Integer page, Integer limit) {
        List<IrNews> list=irNewsDao.selectByPage(page,limit);
        return list;
    }
	
}
