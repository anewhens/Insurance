package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrReport;
import cn.jeefast.rest.dao.IrReportDao;
import cn.jeefast.rest.service.IrReportService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-22
 */
@Service
public class IrReportServiceImpl extends ServiceImpl<IrReportDao, IrReport> implements IrReportService {

    @Autowired
    private IrReportDao reportDao;

    @Override
    public List<IrReport> reportListByPage(Integer page, Integer limit) {
        return reportDao.reportListByPage(page,limit);
    }
}
