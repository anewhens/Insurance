package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.IrWheel;
import cn.jeefast.rest.dao.IrWheelDao;
import cn.jeefast.rest.service.IrWheelService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-08
 */
@Service
public class IrWheelServiceImpl extends ServiceImpl<IrWheelDao, IrWheel> implements IrWheelService {
	
}
