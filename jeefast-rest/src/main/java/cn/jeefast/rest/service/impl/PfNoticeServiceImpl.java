package cn.jeefast.rest.service.impl;

import cn.jeefast.common.entity.PfNotice;
import cn.jeefast.rest.dao.PfNoticeDao;
import cn.jeefast.rest.service.PfNoticeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统通知 服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-07
 */
@Service
public class PfNoticeServiceImpl extends ServiceImpl<PfNoticeDao, PfNotice> implements PfNoticeService {
	
}
