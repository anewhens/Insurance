package cn.jeefast.rest.upload.service;

import cn.jeefast.common.utils.R;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * FileUploadService
 *
 * @author WY
 * @date 2018-08-29 0029
 */
public interface FileUploadService {

  String oneUpload(MultipartFile file, String folder);
  /**
   * @Title: 上传图片并保存缩略图
   * @author: lzl
   * @Description:
   * @param
   * @return
   */
  R oneUpload2(MultipartFile file, String folder);

  boolean delete(List<String> delPicList, String folder);

  String uploadByFileName(MultipartFile file, String folder);
}
