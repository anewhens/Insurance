package cn.jeefast.rest.upload.service.impl;

import cn.jeefast.common.utils.R;
import cn.jeefast.rest.upload.FileUtil;
import cn.jeefast.rest.upload.service.FileUploadService;
import org.apache.commons.lang.StringUtils;
import cn.jeefast.rest.upload.FileUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * FileUploadServiceImpl
 *
 * @author WY
 * @date 2018-08-29 0029
 */
@Service
public class FileUploadServiceImpl implements FileUploadService {

  @Override
  public String oneUpload(MultipartFile file, String folder) {
    return FileUtil.oneFile(file, folder);
  }

  @Override
  public boolean delete(List<String> delPicList, String folder) {
    boolean b = false;
    for (int i = 0; i < delPicList.size(); i++) {
      String filename = StringUtils
              .substring(delPicList.get(i), delPicList.get(i).lastIndexOf("/") + 1);
      b = cn.jeefast.rest.upload.FileUtil.deleteFile(filename, folder);
    }
    return b;
  }

  @Override
  public String uploadByFileName(MultipartFile file, String folder) {
    return null;
  }


  @Override
  public R oneUpload2(MultipartFile file, String folder) {
    String path= cn.jeefast.rest.upload.FileUtil.oneFile(file, folder);
    String smallPath= cn.jeefast.rest.upload.FileUtil.saveSmallPicture(file,folder);
    return R.ok().put("path",path).put("smallPath",smallPath);
  }

}
