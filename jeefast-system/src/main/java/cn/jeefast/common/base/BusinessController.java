package cn.jeefast.common.base;

import cn.jeefast.system.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * BusinessController
 *
 * @author WY
 * @date 2019/2/28 0028
 */
public abstract class BusinessController extends BaseController {

  @Autowired
  private SysRoleService sysRoleService;

}
