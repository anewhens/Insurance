package cn.jeefast.common.utils;

/**
 * @Author:psw
 * @Description:获取视频宽高大小时间工具类
 */


import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.MultimediaInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;

public class VoidUtils {
    public static Long getVdLength(String url){
        File source = new File(url);
        Encoder encoder = new Encoder();
        FileChannel fc= null;
        String size = "";
        try {
            MultimediaInfo m = encoder.getInfo(source);
            long ls = m.getDuration();
            return ls;
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (null!=fc){
                try {
                    fc.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return 0L;
    }

    /**
     * @param path
     * @return Map
     */
    public static Map<String, Object> getVoideMsg(String path){

        Map<String, Object> map = new HashMap<String, Object>();
        File file = new File(path);
        Encoder encoder = new Encoder();
        FileChannel fc= null;
        String size = "";

        if(file != null){
            try {
                MultimediaInfo m = encoder.getInfo(file);
                long ls = m.getDuration();

                FileInputStream fis = new FileInputStream(file);
                fc= fis.getChannel();
                BigDecimal fileSize = new BigDecimal(fc.size());
                size = fileSize.divide(new BigDecimal(1048576), 2, RoundingMode.HALF_UP) + "MB";

                map.put("height", m.getVideo().getSize().getHeight());
                map.put("width", m.getVideo().getSize().getWidth());
                map.put("format", m.getFormat());
            }catch (Exception e) {
                e.printStackTrace();
            }finally {
                if (null!=fc){
                    try {
                        fc.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return map;
    }
}