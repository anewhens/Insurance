package cn.jeefast.config;

import com.baomidou.mybatisplus.mapper.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * MetaObjectHandlerConfig
 *
 * @author WY
 * @date 2019/1/23 0023
 */
@Slf4j
@Component
public class MetaObjectHandlerConfig extends MetaObjectHandler {

  @Override
  public void insertFill(MetaObject metaObject) {
    log.error("start insert fill ....");
    //版本号3.0.6以及之前的版本
    this.setFieldValByName("createTime", LocalDateTime.now(), metaObject);
    this.setFieldValByName("modifyTime", LocalDateTime.now(), metaObject);
    //@since 快照：3.0.7.2-SNAPSHOT， @since 正式版暂未发布3.0.7
//    this.setInsertFieldValByName("createTime", LocalDateTime.now(), metaObject);
  }

  @Override
  public void updateFill(MetaObject metaObject) {
    log.error("start update fill ....");
    this.setFieldValByName("modifyTime", LocalDateTime.now(), metaObject);
    //@since 快照：3.0.7.2-SNAPSHOT， @since 正式版暂未发布3.0.7
//    this.setUpdateFieldValByName("modifyTime", LocalDateTime.now(), metaObject);
  }
}
