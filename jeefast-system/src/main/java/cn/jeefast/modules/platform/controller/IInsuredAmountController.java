package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.base.BaseController;
import cn.jeefast.common.entity.IInsuredAmount;
import cn.jeefast.common.entity.IInsuredAmountFee;
import cn.jeefast.common.utils.PoiUtil;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IInsuredAmountFeeService;
import cn.jeefast.modules.platform.service.IInsuredAmountService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-03-06
 */
@RestController
@RequestMapping("/iInsuredAmount")
@CrossOrigin
public class IInsuredAmountController extends BaseController {

    @Autowired
    private IInsuredAmountService iInsuredAmountService;

    @Autowired
    private IInsuredAmountFeeService iInsuredAmountFeeService;

    @RequestMapping("/addPlan")
    public R addPlan(IInsuredAmount iInsuredAmount, MultipartFile file){

        if(iInsuredAmount.getIiaId()!=null){
            iInsuredAmountService.updateById(iInsuredAmount);
            if(file!=null){
                EntityWrapper ew = new EntityWrapper();
                ew.eq("iia_id",iInsuredAmount.getIiaId());
                List<IInsuredAmountFee> list = iInsuredAmountFeeService.selectList(ew);
                for (IInsuredAmountFee iiaf:list) {
                    iInsuredAmountFeeService.deleteById(iiaf.getIiafId());
                }
            }
        }else{ iInsuredAmountService.insert(iInsuredAmount);}


        if(file!=null){
            Long iaId = iInsuredAmount.getIiaId();

            List<String[]> excelData = null;
            try {
                excelData = PoiUtil.getExcelData(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            /**
             *  开始分解信息
             */

            //获取数据纵列数
            int length = excelData.get(0).length-1;
            System.out.println("+++列数"+length);

            //获取第一行，保险期间
            String[] timeStrs = excelData.get(0);
            List<String> time = new ArrayList<>();
            for (int i = 1; i < timeStrs.length; i++) {
                time.add(timeStrs[i]);
            }

            //获取第二行，缴费期间
            String[] payStrs = excelData.get(1);
            List<String> pay = new ArrayList<>();
            for (int i = 1; i <payStrs.length; i++) {
                pay.add(payStrs[i]);
            }
            //获取第二行，缴费期间
            String[] sexStrs = excelData.get(2);
            List<String> sex = new ArrayList<>();
            for (int i = 1; i <sexStrs.length; i++) {
                sex.add(sexStrs[i]);
            }


            System.out.println("++++产生实体数据++++");
            //循环取数据
            for (int i = 3; i < excelData.size(); i++) {
                //获取数据行第一行数据
                String[] strings = excelData.get(i);
                //length是金额列，+1列年龄列。
                for (int j = 0; j < length; j++) {
                    IInsuredAmountFee iInsuredAmountFee = new IInsuredAmountFee();
                    iInsuredAmountFee.setIiafAge(Integer.parseInt(strings[0]));
                    iInsuredAmountFee.setIiafFeeTime(pay.get(j));
                    iInsuredAmountFee.setIiafTime(time.get(j));
                    iInsuredAmountFee.setIiafSex(sex.get(j));
                    iInsuredAmountFee.setIiafMoney(strings[j+1]);
                    iInsuredAmountFee.setIiaId(iaId);
                    iInsuredAmountFeeService.insert(iInsuredAmountFee);
                }
            }

            System.out.println("--------打印保险期间--------");
            System.out.println(time);
            System.out.println("--------打印缴费期间--------");
            System.out.println(pay);
            System.out.println("--------打印性别--------");
            System.out.println(sex);
        }
        return R.ok("成功").put("data",null);
    }

    @GetMapping("detail")
    public R detail(Long id){
        IInsuredAmount iInsuredAmount = iInsuredAmountService.selectById(id);
        return R.ok("").put("data",iInsuredAmount);
    }

    @GetMapping("list")
    public R list(String title, Long type,Long comp, Integer page, Integer limit){
        limit = limit == null? 10:limit;
        page = page ==null?0:(page-1)*limit;
        List<Map> maps = iInsuredAmountService.listPage(title, type, comp, page, limit);
        List<Map> maps1 = iInsuredAmountService.listCount(title, type, comp);
        return R.ok("成功").put("data",maps).put("count",maps1.size());
    }

    @GetMapping("delete")
    public R delete(String ids){
        if(ids==null||ids.length()==0){
            return R.error("未选中数据");
        }
        String[] id = ids.split(",");
        for (String s:id) {
            IInsuredAmount iInsuredAmount = new IInsuredAmount();
            iInsuredAmount.setIiaId(Long.parseLong(s));
            iInsuredAmount.setDeletes(1);
            iInsuredAmountService.updateById(iInsuredAmount);
        }
        return R.ok("删除成功").put("data",null);
    }
}
