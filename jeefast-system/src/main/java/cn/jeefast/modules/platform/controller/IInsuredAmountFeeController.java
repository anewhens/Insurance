package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.base.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-03-06
 */
@RestController
@RequestMapping("/iInsuredAmountFee")
public class IInsuredAmountFeeController extends BaseController {
	
}
