package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.entity.IrAdditional;
import cn.jeefast.common.entity.IrAdditionalFee;
import cn.jeefast.common.utils.PoiUtil;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IrAdditionalFeeService;
import cn.jeefast.modules.platform.service.IrAdditionalService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.jeefast.common.base.BaseController;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-03-05
 */
@RestController
@RequestMapping("/irAdditional")
@CrossOrigin
public class IrAdditionalController extends BaseController {

    @Autowired
    private IrAdditionalFeeService irAdditionalFeeService;
    @Autowired
    private IrAdditionalService irAdditionalService;

    @RequestMapping("/additional/add")
    @ResponseBody
    public R upload(IrAdditional additional, MultipartFile file) throws Exception{

        if(additional.getIaId()!=null){
            irAdditionalService.updateById(additional);
            if(file!=null){
                //删除原始费率
                EntityWrapper ew = new EntityWrapper();
                ew.eq("ia_id",additional.getIaId());
                List<IrAdditionalFee> list = irAdditionalFeeService.selectList(ew);
                for (IrAdditionalFee af:list) {
                    irAdditionalFeeService.deleteById(af.getIafId());
                }
            }
        }else{
            irAdditionalService.insert(additional);
        }

       if(file!=null){
           Long iaId = additional.getIaId();

           List<String[]> excelData = PoiUtil.getExcelData(file);

           /**
            *  开始分解信息
            */

           //获取数据纵列数
           int length = excelData.get(0).length-1;
           System.out.println("+++列数"+length);

           //获取第一行，保险期间
           String[] timeStrs = excelData.get(0);
           List<String> time = new ArrayList<>();
           for (int i = 1; i < timeStrs.length; i++) {
               time.add(timeStrs[i]);
           }

           //获取第二行，缴费期间
           String[] payStrs = excelData.get(1);
           List<String> pay = new ArrayList<>();
           for (int i = 1; i <payStrs.length; i++) {
               pay.add(payStrs[i]);
           }
           //获取第二行，缴费期间
           String[] sexStrs = excelData.get(2);
           List<String> sex = new ArrayList<>();
           for (int i = 1; i <sexStrs.length; i++) {
               sex.add(sexStrs[i]);
           }


           System.out.println("++++产生实体数据++++");
           //循环取数据
           for (int i = 3; i < excelData.size(); i++) {
               //获取数据行第一行数据
               String[] strings = excelData.get(i);
               //length是金额列，+1列年龄列。
               for (int j = 0; j < length; j++) {
                   IrAdditionalFee iaf = new IrAdditionalFee();
                   iaf.setIaAge(Integer.parseInt(strings[0]));
                   iaf.setIaFeeTime(pay.get(j));
                   iaf.setIaTime(time.get(j));
                   iaf.setIaSex(sex.get(j));
                   iaf.setIaMoney(strings[j+1]);
                   iaf.setIaId(iaId);
                   irAdditionalFeeService.insert(iaf);
               }
           }

           System.out.println("--------打印保险期间--------");
           System.out.println(time);
           System.out.println("--------打印缴费期间--------");
           System.out.println(pay);
           System.out.println("--------打印性别--------");
           System.out.println(sex);
       }
        return R.ok();
    }

    @GetMapping("/additional/list")
    public R additionalList(Integer page,Integer limit,String title ,Long com){
        limit = limit == null?10:limit;
        page = page==null?0:(page-1)*limit;
        List<IrAdditional> irAdditionals = irAdditionalService.additionalListByPage(page, limit,title,com);
        return R.ok("成功").put("data",irAdditionals);
    }

    @GetMapping("/additional/detail")
    public R detail(Long id){
        IrAdditional irAdditional = irAdditionalService.selectById(id);
        return R.ok("成功").put("data",irAdditional);
    }

    @PostMapping("/additional/update")
    public R update(IrAdditional additional){
        irAdditionalService.updateById(additional);
        return  R.ok("成功").put("data",null);
    }

    @GetMapping("/additional/delete")
    public R delete(String ids){
        String[] split = ids.split(",");
        for (String s:split) {
            irAdditionalService.deleteById(Long.parseLong(s));
        }
        return R.ok("删除成功").put("data",null);
    }
}
