package cn.jeefast.modules.platform.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cn.jeefast.common.base.BaseController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-03-05
 */
@RestController
@RequestMapping("/irAdditionalFee")
public class IrAdditionalFeeController extends BaseController {
	
}
