package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.entity.IrBank;
import cn.jeefast.common.entity.IrNews;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IrBankService;
import cn.jeefast.modules.platform.service.IrNewsService;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.jeefast.common.base.BaseController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irBank")
@CrossOrigin
public class IrBankController extends BaseController {

    @Autowired
    private IrBankService irBankService;

    @PostMapping("/addProvienceBank")
    public R addProvienceBank(String bankName){
        IrBank bank = new IrBank();
        bank.setBackParent(null);
        bank.setBankName(bankName);
        bank.setBankType(1);
        irBankService.insert(bank);
        return R.ok("添加成功！").put("data",null);
    }

    @PostMapping("/addCityBank")
    public R addCityBank(Long provienceId,String bankName){
        IrBank bank = new IrBank();
        bank.setBankType(2);
        bank.setBankName(bankName);
        bank.setBackParent(provienceId);
        irBankService.insert(bank);
        return R.ok("添加成功").put("data",null);
    }



    @GetMapping("/provinceBankList")
    public R provinceBankList(Integer page,Integer limit,String bankName){
        limit = limit == null?10:limit;
        page = page==null?0:(page-1)*limit;
        List<IrBank> irBanks = irBankService.selectBankList(1, bankName, null, page, limit);
        return R.ok("查询成功").put("data",irBanks);
    }


    @GetMapping("/cityBankList")
    public R cityBankList(Integer page,Integer limit,String bankName,Long provienceId){
        limit = limit == null?10:limit;
        page = page==null?0:(page-1)*limit;
        List<IrBank> irBanks = irBankService.selectBankList(2, bankName, provienceId, page, limit);
        return R.ok("查询成功").put("data",irBanks);
    }


    @GetMapping("/delete")
    public R delete(Long id){
        IrBank irBank = new IrBank();
        irBank.setBankId(id);
        irBank.setDeletes(1);
        irBankService.updateById(irBank);
        return R.ok("成功").put("data",null);
    }

    /**
     * @Title: 获取省行
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    @RequestMapping("/getBankList")
    public R getBankList(@RequestBody Map<String, Object> params){
        Query query = new Query(params);
        params.put("deletes",0);
        Page<IrBank> p = new Page(query.getPage(), query.getLimit());
        Page<IrBank> page = irBankService.selectByPage(p, params);
        return R.ok("查询成功").put("page",page);
    }
    /**
     * @Title: 获取市行
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    @RequestMapping("/getBankList2")
    public R getBankList2(@RequestBody Map<String, Object> params){
        Query query = new Query(params);
        Page<IrBank> p = new Page(query.getPage(), query.getLimit());
        Page<IrBank> page = irBankService.selectByPage2(p, params);
        return R.ok("查询成功").put("page",page);
    }
    /**
     * @Title: 删除银行
     * @author: lzl
     * @Description:
     * @param ids
     * @return R
     */
    @RequestMapping("doDeleteBank")
    public R doDeleteNews(@RequestBody Long[] ids){
        try {
            for (int i = 0; i < ids.length; i++) {
                irBankService.deleteById(ids[i]);
            }
            return R.ok("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.ok("删除失败");
        }
    }
    /**
     * @param irBank
     * @return R
     * @Title: 修改省行
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doUpdateBank")
    public R doUpdateBank(@RequestBody IrBank irBank){
        irBankService.updateById(irBank);
        return R.ok("修改成功");
    }
    /**
     * @param irBank
     * @return R
     * @Title: 添加省行
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doAddBank")
    public R doAddBank(@RequestBody IrBank irBank){
        irBank.setBankType(1);// 1 表示省行
        irBankService.insert(irBank);
        return R.ok("添加成功");
    }
    /**
     * @param irBank
     * @return R
     * @Title: 添加市行
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doAddBank2")
    public R doAddBank2(@RequestBody IrBank irBank){
        irBank.setBankType(2);// 2 表示市行
        irBankService.insert(irBank);
        return R.ok("添加成功");
    }


}
