package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.entity.IrClassNews;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IrClassNewsService;
import cn.jeefast.modules.platform.service.IrClassNewsService;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cn.jeefast.common.base.BaseController;

import java.util.Date;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irClassNews")
public class IrClassNewsController extends BaseController {

    @Autowired
    private IrClassNewsService irClassNewsService;

    /**
     * @Title: 获取资讯
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    @RequestMapping("/getNewsList")
    public R getNewsList(@RequestBody Map<String, Object> params){
        Query query = new Query(params);
        Page<IrClassNews> p = new Page(query.getPage(), query.getLimit());
        Page<IrClassNews> page = irClassNewsService.selectByPage(p, params);
        return R.ok("查询成功").put("page",page);
    }
    /**
     * @Title: 删除资讯
     * @author: lzl
     * @Description:
     * @param ids
     * @return R
     */
    @RequestMapping("doDeleteNews")
    public R doDeleteNews(@RequestBody Long[] ids){
        try {
            for (int i = 0; i < ids.length; i++) {
                irClassNewsService.deleteById(ids[i]);
            }
            return R.ok("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.ok("删除失败");
        }
    }
    /**
     * @param irClassNews
     * @return R
     * @Title: 修改资讯
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doUpdateNews")
    public R doUpdateNews(@RequestBody IrClassNews irClassNews){
        irClassNewsService.updateById(irClassNews);
        return R.ok("修改成功");
    }
    /**
     * @param irClassNews
     * @return R
     * @Title: 添加资讯
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doAddNews")
    public R doAddNews(@RequestBody IrClassNews irClassNews){
        irClassNews.setCnTime(new Date());
        irClassNewsService.insert(irClassNews);
        return R.ok("添加成功");
    }
	
}
