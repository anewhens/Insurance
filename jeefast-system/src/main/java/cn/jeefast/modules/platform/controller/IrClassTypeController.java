package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.entity.IrClassType;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IrClassTypeService;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cn.jeefast.common.base.BaseController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irClassType")
public class IrClassTypeController extends BaseController {
    @Autowired
    private IrClassTypeService irClassTypeService;

    /**
     * @Title: 获取所有类型
     * @author: lzl
     * @Description:
     * @param
     * @return R
     */
    @RequestMapping("/getTypeList")
    public R getTypeList(){
        List<IrClassType> typeList = irClassTypeService.selectList(null);
        return R.ok("查询成功").put("typeList",typeList);
    }
    /**
     * @Title: 获取类型信息
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    @RequestMapping("/getTypeList2")
    public R getTypeList(@RequestBody Map<String, Object> params){
        Query query = new Query(params);
        Page<IrClassType> p = new Page(query.getPage(), query.getLimit());
        Page<IrClassType> page = irClassTypeService.selectByPage(p, params);
        return R.ok("查询成功").put("page",page);
    }
    /**
     * @Title: 删除类型
     * @author: lzl
     * @Description:
     * @param ids
     * @return R
     */
    @RequestMapping("doDeleteType")
    public R doDeleteType(@RequestBody Long[] ids){
        try {
            for (int i = 0; i < ids.length; i++) {
                irClassTypeService.deleteById(ids[i]);
            }
            return R.ok("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.ok("删除失败");
        }
    }
    /**
     * @param irClassType
     * @return R
     * @Title: 修改类型
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doUpdateType")
    public R doUpdateType(@RequestBody IrClassType irClassType){
        irClassTypeService.updateById(irClassType);
        return R.ok("修改成功");
    }
    /**
     * @param irClassType
     * @return R
     * @Title: 添加类型
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doAddType")
    public R doAddType(@RequestBody IrClassType irClassType){
        irClassTypeService.insert(irClassType);
        return R.ok("添加成功");
    }


}
