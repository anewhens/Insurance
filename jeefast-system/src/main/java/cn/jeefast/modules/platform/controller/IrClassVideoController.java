package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.entity.*;
import cn.jeefast.common.entity.IrClassVideo;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.common.utils.VoidUtils;
import cn.jeefast.modules.platform.service.IrBankService;
import cn.jeefast.modules.platform.service.IrClassVideoService;
import cn.jeefast.modules.platform.service.IrCompanyService;
import cn.jeefast.modules.platform.service.IrInsuranceTypeService;
import cn.jeefast.modules.upload.FileUtil;
import cn.jeefast.system.entity.SysUser;
import cn.jeefast.system.service.SysUserService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.jeefast.common.base.BaseController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irClassVideo")
@CrossOrigin
public class IrClassVideoController extends BaseController {

    @Autowired
    private IrClassVideoService irClassVideoService;
    @Autowired
    private IrCompanyService irCompanyService;
    @Autowired
    private IrInsuranceTypeService irInsuranceTypeService;
    @Autowired
    private IrBankService irBankService;
    @Autowired
    private SysUserService userService;
    /**
     * @Title: 添加视频
     * @author: lzl
     * @Description:
     * @param
     * @return
     */
    @RequestMapping("/doAddVideo")
    public R addVideo(@RequestBody IrClassVideo video){
        Long vdLength1 = VoidUtils.getVdLength(FileUtil.getPath2()+video.getCvUrl());
        video.setCvLegth(Double.parseDouble(vdLength1.toString())/1000);
        video.setCvUpTime(new Date());
        irClassVideoService.insert(video);
        return R.ok("成功").put("data",null);
    }

    /**
     * @Title: 获取视频
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    @RequestMapping("/getVideoList")
    public R getVideoList(@RequestBody Map<String, Object> params){
        List<Long> power = userService.getPower(super.getUser());
        Query query = new Query(params);
        query.put("power",power);
        Page<Map> p = new Page(query.getPage(), query.getLimit());
        Page<Map> page = irClassVideoService.selectByPage(p, params,power);
        return R.ok("查询成功").put("page",page);
    }
    /**
     * @Title: 获取所有保险公司
     * @author: lzl
     * @Description:
     * @param
     * @return R
     */
    @RequestMapping("/getCompanyList")
    public R getCompanyList(){
        List<IrCompany> companyList = irCompanyService.selectList(null);
        return R.ok("查询成功").put("companyList",companyList);
    }
    /**
     * @Title: 获取所有保险类型
     * @author: lzl
     * @Description:
     * @param
     * @return R
     */
    @RequestMapping("/getTypeList")
    public R getTypeList(){
        List<IrInsuranceType> typeList = irInsuranceTypeService.selectList(null);
        return R.ok("查询成功").put("typeList",typeList);
    }
    /**
     * @Title: 获取所有省行
     * @author: lzl
     * @Description:
     * @param
     * @return R
     */
    @RequestMapping("/getBankList")
    public R getBankList(){
        EntityWrapper ew = new EntityWrapper();
        ew.eq("bank_type",1);
        List<IrBank> bankList = irBankService.selectList(ew);
        return R.ok("查询成功").put("bankList",bankList);
    }
    /**
     * @Title: 删除视频
     * @author: lzl
     * @Description:
     * @param ids
     * @return R
     */
    @RequestMapping("doDeleteVideo")
    public R doDeleteVideo(@RequestBody Long[] ids){
        try {
            for (int i = 0; i < ids.length; i++) {
                irClassVideoService.deleteById(ids[i]);
            }
            return R.ok("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.ok("删除失败");
        }
    }
    /**
     * @param irClassVideo
     * @return R
     * @Title: 修改视频
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doUpdateVideo")
    public R doUpdateVideo(@RequestBody IrClassVideo irClassVideo){
        Long vdLength1 = VoidUtils.getVdLength(FileUtil.getPath2()+irClassVideo.getCvUrl());
        irClassVideo.setCvLegth(Double.parseDouble(vdLength1.toString())/1000);
        irClassVideoService.updateById(irClassVideo);
        return R.ok("修改成功");
    }

}
