package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.excel.ExcelUtil;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.model.ProgressVO;
import cn.jeefast.modules.platform.service.IrClassVideoProgressService;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import cn.jeefast.common.base.BaseController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irClassVideoProgress")
public class IrClassVideoProgressController extends BaseController {
    @Autowired
    private IrClassVideoProgressService irClassVideoProgressService;
    /**
     * @Title: 获取学习进度
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    @RequestMapping("/getProgressList")
    public R getProgressList(@RequestBody Map<String, Object> params){
        Query query = new Query(params);
        Page<Map> p = new Page(query.getPage(), query.getLimit());
        Page<Map> page = irClassVideoProgressService.selectByPage(p, params,super.getUser());
        return R.ok("查询成功").put("page",page);
    }
    /**
     * @param response,params
     * @return
     * @Title: 导出学习进度
     * @author: lzl
     * @Description:
     */
    @RequestMapping("/downloadExcel")
    public void downloadExcel(@RequestParam(value = "cvpIdList") List<Long> cvpIdList, HttpServletResponse response) {
        try {
            List<ProgressVO> progressList = irClassVideoProgressService.getProgressList(cvpIdList);
            ExcelUtil.setInfoAndExport(response, "学习进度表", "学习进度表.xls",
                    "progress-template.xls", "cn.jeefast.model.ProgressVO", progressList);
        } catch (Exception e) {
            logger.error("【导出失败，原因 --> {}】", e.toString());
        }
    }

    @RequestMapping("/downloadExcelAll")
    public void downloadExcelAll(Long cvId, HttpServletResponse response) {
        try {
            Map params = new HashMap();
            params.put("videoId",cvId);
            params.put("page",1);
            params.put("limit",999999);
            Query query = new Query(params);
            Page<Map> p = new Page(query.getPage(), query.getLimit());
            Page<Map> page = irClassVideoProgressService.selectByPage(p, params,super.getUser());
            List<Long> ids = new ArrayList<>();
            List<Map> records = page.getRecords();
            for (Map m:records) {
                ids.add((Long) m.get("cvpId"));
            }
            List<ProgressVO> progressList = irClassVideoProgressService.getProgressList(ids);
            ExcelUtil.setInfoAndExport(response, "学习进度表", "学习进度表.xls",
                    "progress-template.xls", "cn.jeefast.model.ProgressVO", progressList);
        } catch (Exception e) {
            logger.error("【导出失败，原因 --> {}】", e.toString());
        }
    }
	
}
