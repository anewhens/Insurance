package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.entity.IrCompany;
import cn.jeefast.common.entity.IrCompany;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IrCompanyService;
import cn.jeefast.modules.platform.service.IrCompanyService;
import com.baomidou.mybatisplus.plugins.Page;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.jeefast.common.base.BaseController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irCompany")
@CrossOrigin
public class IrCompanyController extends BaseController {
    @Autowired
	private IrCompanyService companyService;

    @GetMapping("/companyList")
    public R comList(String icTitle, Integer page,Integer limit){
        limit = limit == null?10:limit;
        page = page==null?0:(page-1)*limit;
        List<IrCompany> irCompanies = companyService.selectCompList(icTitle, page, limit);
        return R.ok("获取成功").put("data",irCompanies);
    }

    @PostMapping("/addOrSaveCompany")
    public R addCom(IrCompany company){
        if(company.getIcId()!=null){
            companyService.updateById(company);
        }else{
            companyService.insert(company);
        }

        return R.ok("成功！").put("data",null);
    }

    @GetMapping("compById")
    public R compById(Long comId){
        IrCompany irCompany = companyService.selectById(comId);
        return R.ok("ok").put("data",irCompany);
    }

    /**
     * @Title: 获取保险公司名称
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    @RequestMapping("/getCompanyList")
    public R getCompanyList(@RequestBody Map<String, Object> params){
        Query query = new Query(params);
        Page<IrCompany> p = new Page(query.getPage(), query.getLimit());
        Page<IrCompany> page = companyService.selectByPage(p, params);
        return R.ok("查询成功").put("page",page);
    }
    /**
     * @Title: 删除保险公司
     * @author: lzl
     * @Description:
     * @param ids
     * @return R
     */
    @RequestMapping("doDeleteCompany")
    public R doDeleteCompany(@RequestBody Long[] ids){
        try {
            for (int i = 0; i < ids.length; i++) {
                companyService.deleteById(ids[i]);
            }
            return R.ok("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.ok("删除失败");
        }
    }
    /**
     * @param irCompany
     * @return R
     * @Title: 修改保险公司
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doUpdateCompany")
    public R doUpdateCompany(@RequestBody IrCompany irCompany){
        companyService.updateById(irCompany);
        return R.ok("修改成功");
    }
    /**
     * @param irCompany
     * @return R
     * @Title: 添加保险公司
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doAddCompany")
    public R doAddCompany(@RequestBody IrCompany irCompany){
        companyService.insert(irCompany);
        return R.ok("添加成功");
    }



}
