package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.entity.IrDict;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IrDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cn.jeefast.common.base.BaseController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irDict")
public class IrDictController extends BaseController {

    @Autowired
    private IrDictService dictService;
    /**
     * @param irDict
     * @return R
     * @Title: 修改广告
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doUpdateDict")
    public R doUpdateDict(@RequestBody IrDict irDict){
        dictService.updateById(irDict);
        return R.ok("更新成功");
    }
    /**
     * @param
     * @return R
     * @Title: 获取广告信息
     * @author: lzl
     * @Description:
     */
    @RequestMapping("/getDictDetail")
    public R getDictDetail() {
        return dictService.getDictDetail();
    }
    /**
     * @param dictId
     * @return
     * @Title: 删除广告
     * @author: lzl
     * @Description:
     */
    @RequestMapping("/doDeleteDict")
    public R doDeleteDict(Long dictId) {
        try {
            dictService.deleteById(dictId);
            return R.ok("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.ok("删除失败");
        }
    }
}
