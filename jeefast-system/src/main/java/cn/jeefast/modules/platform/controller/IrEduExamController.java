package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.base.BaseController;
import cn.jeefast.common.entity.IrEduExam;
import cn.jeefast.common.entity.IrEduTime;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IrEduExamService;
import cn.jeefast.modules.platform.service.IrEduTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irEduExam")
@CrossOrigin
public class IrEduExamController extends BaseController {

    @Autowired
    private IrEduExamService eduExamService;

    @Autowired
    private IrEduTimeService irEduTimeService;


    @GetMapping("examList")
    public R selectList(Integer etId, Integer page, Integer limit,Long typeId,String title){
        page = (page-1)* limit;
        return eduExamService.examList(etId,page,limit,typeId,title);
    }


    @PostMapping("/addExam")
    public R addExam(IrEduExam eduExam, Integer comm, Integer dt){
        if(comm==null){
            comm = 0;
        }
        if(dt==null){
            dt = 0;
        }
        IrEduTime irEduTime = irEduTimeService.selectById(eduExam.getEtId());
        if(irEduTime==null){
            return  R.error("批次不存在").put("data",null);
        }
//        if(irEduTime.getEtCommScore()+comm>100){
//            return R.error("普通分数总分大于100");
//        }
//        if(irEduTime.getEtDtScore()+dt>10){
//            return R.error("投连分数总分大于10");
//        }
        irEduTime.setEtCommScore(irEduTime.getEtCommScore()+comm);
        irEduTime.setEtDtScore(irEduTime.getEtDtScore()+dt);
        irEduTimeService.updateById(irEduTime);
        eduExam.setEeTime(new Date());
        if(eduExam.getEeId()==null){
            eduExamService.insert(eduExam);
        }else{
            eduExamService.updateById(eduExam);
        }
        return R.ok("添加成功").put("data",null);
    }


    @GetMapping("/examDetail")
    public R detail(Long id){
        IrEduExam irEduExam = eduExamService.selectById(id);
        return R.ok("OK").put("data",irEduExam);
    }

    @GetMapping("/deleteExam")
    public R delete(Long id){
        eduExamService.deleteById(id);
        return R.ok("ok").put("data",null);
    }
}
