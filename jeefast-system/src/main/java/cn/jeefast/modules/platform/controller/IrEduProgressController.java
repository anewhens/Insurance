package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.base.BaseController;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.entity.TbUser;
import cn.jeefast.modules.platform.service.IrEduProgressService;
import cn.jeefast.modules.platform.service.TbUserService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irEduProgress")
@CrossOrigin
public class IrEduProgressController extends BaseController {

    @Autowired
    private IrEduProgressService eduProgressService;

    @Autowired
    private TbUserService tbUserService;

    @GetMapping("examList")
    public R examList(Integer isPass, String mobile, String uname, Integer page, Integer limit,Long etId){
        limit = limit == null?10:limit;
        page = page ==null? 0:(page-1)*limit;

        return eduProgressService.selectExamList(isPass,mobile,uname,page,limit,super.getUser(),etId);
    }

    @GetMapping("/eduInfoDetail")
    public R eduInfoDetail(String mobile,Long etId){
        EntityWrapper ew = new EntityWrapper();
        ew.eq("mobile",mobile);
        List<TbUser> list = tbUserService.selectList(ew);
        if(list.size()==0){
            return R.ok("").put("data",null);
        }

        return eduProgressService.eduInfoDetail(list.get(0).getUserId(), etId);
    }
}
