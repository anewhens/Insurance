package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.base.BaseController;
import cn.jeefast.common.entity.IrEduTime;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IrEduTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irEduTime")
@CrossOrigin
public class IrEduTimeController extends BaseController {
	@Autowired
    private IrEduTimeService eduTimeService;

	@GetMapping("/timeList")
	public R selectTimeList(Integer page , Integer limit){
	    page = (page-1)*limit;
        return eduTimeService.selectList1(page,limit);
    }

    @PostMapping("/addTime")
    public R addTime(IrEduTime eduTime, Long time1, Long time2, Long time3, Long time4){
		eduTime.setEtSignStart(new Date(time1));
		eduTime.setEtSignEnd(new Date(time2));
		eduTime.setEtExamStart(new Date(time3));
		eduTime.setEtExamEnd(new Date(time4));
	    if(eduTime.getEtId()==null){
	        eduTimeService.insert(eduTime);
	    }else{
	        eduTimeService.updateById(eduTime);
        }
	    return R.ok("添加成功！").put("data",null);
    }

    @GetMapping("/deleteTime")
    public R delete(String ids){
		String[] split = ids.split(",");
		for (String s:split) {
			eduTimeService.deleteById(Long.parseLong(s));
		}
		return R.ok("删除成功").put("data",null);
	}
}
