package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.base.BaseController;
import cn.jeefast.common.entity.IrEduType;
import cn.jeefast.common.entity.IrEduType;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IrEduTypeService;
import cn.jeefast.modules.platform.service.IrEduTypeService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irEduType")
@CrossOrigin
public class IrEduTypeController extends BaseController {

    @Autowired
    private IrEduTypeService eduTypeService;

    @GetMapping("/eduTypeList")
	private R eduTypeList(){
        List<IrEduType> irEduTypes = eduTypeService.selectList(new EntityWrapper<>());
        return R.ok("成功！").put("data",irEduTypes);
    }
    /**
     * @Title: 获取类别信息
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    @RequestMapping("/getTypeList")
    public R getTypeList(@RequestBody Map<String, Object> params){
        Query query = new Query(params);
        Page<IrEduType> p = new Page(query.getPage(), query.getLimit());
        Page<IrEduType> page = eduTypeService.selectByPage(p, params);
        return R.ok("查询成功").put("page",page);
    }
    /**
     * @Title: 删除类别
     * @author: lzl
     * @Description:
     * @param ids
     * @return R
     */
    @RequestMapping("doDeleteType")
    public R doDeleteType(@RequestBody Long[] ids){
        try {
            for (int i = 0; i < ids.length; i++) {
                eduTypeService.deleteById(ids[i]);
            }
            return R.ok("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.ok("删除失败");
        }
    }
    /**
     * @param irEduType
     * @return R
     * @Title: 修改类型
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doUpdateType")
    public R doUpdateType(@RequestBody IrEduType irEduType){
        eduTypeService.updateById(irEduType);
        return R.ok("修改成功");
    }
    /**
     * @param irEduType
     * @return R
     * @Title: 添加类型
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doAddType")
    public R doAddType(@RequestBody IrEduType irEduType){
        eduTypeService.insert(irEduType);
        return R.ok("添加成功");
    }
}
