package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.entity.IrFeedback;
import cn.jeefast.common.entity.IrNews;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IrFeedbackService;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cn.jeefast.common.base.BaseController;

import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irFeedback")
public class IrFeedbackController extends BaseController {

    @Autowired
    private IrFeedbackService feedbackService;
    /**
     * @Title: 获取消息
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    @RequestMapping("/getNewsList")
    public R getNewsList(@RequestBody Map<String, Object> params){
        Query query = new Query(params);
        Page<IrFeedback> p = new Page(query.getPage(), query.getLimit());
        Page<IrFeedback> page = feedbackService.selectByPage(p, params);
        return R.ok("查询成功").put("page",page);
    }

    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        for (Long id:ids
             ) {
            feedbackService.deleteById(id);
        }
        return R.ok("成功").put("data",null);
    }
}
