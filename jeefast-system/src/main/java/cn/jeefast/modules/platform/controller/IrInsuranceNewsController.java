package cn.jeefast.modules.platform.controller;

import cn.jeefast.common.base.BaseController;
import cn.jeefast.common.entity.IrInsuranceNews;
import cn.jeefast.common.entity.IrInsuranceNews;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IrInsuranceNewsService;
import cn.jeefast.modules.platform.service.IrInsuranceNewsService;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irInsuranceNews")
public class IrInsuranceNewsController extends BaseController {

    @Autowired
    private IrInsuranceNewsService irInsuranceNewsService;

    /**
     * @Title: 获取消息
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    @RequestMapping("/getInsuranceNewsList")
    public R getNewsList(@RequestBody Map<String, Object> params){
        Query query = new Query(params);
        Page<IrInsuranceNews> p = new Page(query.getPage(), query.getLimit());
        Page<IrInsuranceNews> page = irInsuranceNewsService.selectByPage(p, params);
        return R.ok("查询成功").put("page",page);
    }
    /**
     * @Title: 删除资讯
     * @author: lzl
     * @Description:
     * @param ids
     * @return R
     */
    @RequestMapping("doDeleteInsuranceNews")
    public R doDeleteInsuranceNews(@RequestBody Long[] ids){
        try {
            for (int i = 0; i < ids.length; i++) {
                irInsuranceNewsService.deleteById(ids[i]);
            }
            return R.ok("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.ok("删除失败");
        }
    }
    /**
     * @param irNews
     * @return R
     * @Title: 修改资讯
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doUpdateInsuranceNews")
    public R doUpdateInsuranceNews(@RequestBody IrInsuranceNews irNews){
        irInsuranceNewsService.updateById(irNews);
        return R.ok("修改成功");
    }
    /**
     * @param irInsuranceNews
     * @return R
     * @Title: 添加资讯
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doAddInsuranceNews")
    public R doAddNews(@RequestBody IrInsuranceNews irInsuranceNews){
        irInsuranceNews.setIinTime(new Date());
        irInsuranceNewsService.insert(irInsuranceNews);
        return R.ok("添加成功");
    }
	
}
