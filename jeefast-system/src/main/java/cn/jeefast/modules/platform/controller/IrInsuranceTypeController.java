package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.entity.IrInsuranceType;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IrInsuranceTypeService;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.jeefast.common.base.BaseController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irInsuranceType")
@CrossOrigin
public class IrInsuranceTypeController extends BaseController {

    @Autowired
    private IrInsuranceTypeService irInsuranceTypeService;

    @GetMapping("/insuranceTypeList")
    public R typeList(Integer page,Integer limit){
        limit = limit == null? 10:limit;
        page = page == null? 0:(page-1)*limit;
        List<IrInsuranceType> irInsuranceTypes = irInsuranceTypeService.typeListByPaye(page, limit);
        return R.ok("成功").put("data",irInsuranceTypes);
    }
    /**
     * @Title: 获取保险种类
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    @RequestMapping("/getTypeList")
    public R getTypeList(@RequestBody Map<String, Object> params){
        Query query = new Query(params);
        Page<IrInsuranceType> p = new Page(query.getPage(), query.getLimit());
        Page<IrInsuranceType> page = irInsuranceTypeService.selectByPage(p, params);
        return R.ok("查询成功").put("page",page);
    }
    /**
     * @Title: 删除保险种类
     * @author: lzl
     * @Description:
     * @param ids
     * @return R
     */
    @RequestMapping("doDeleteType")
    public R doDeleteType(@RequestBody Long[] ids){
        try {
            for (int i = 0; i < ids.length; i++) {
                irInsuranceTypeService.deleteById(ids[i]);
            }
            return R.ok("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.ok("删除失败");
        }
    }
    /**
     * @param irInsuranceType
     * @return R
     * @Title: 修改保险种类
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doUpdateType")
    public R doUpdateType(@RequestBody IrInsuranceType irInsuranceType){
        irInsuranceTypeService.updateById(irInsuranceType);
        return R.ok("修改成功");
    }
    /**
     * @param irInsuranceType
     * @return R
     * @Title: 添加保险种类
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doAddType")
    public R doAddType(@RequestBody IrInsuranceType irInsuranceType){
        irInsuranceTypeService.insert(irInsuranceType);
        return R.ok("添加成功");
    }



}
