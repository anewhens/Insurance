package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.entity.IrLow;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IrLowService;
import cn.jeefast.modules.platform.service.IrLowService;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cn.jeefast.common.base.BaseController;

import java.util.Date;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irLow")
public class IrLowController extends BaseController {

    @Autowired
    private IrLowService irLowService;

    /**
     * @Title: 获取消息
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    @RequestMapping("/getLowList")
    public R getLowList(@RequestBody Map<String, Object> params){
        Query query = new Query(params);
        Page<IrLow> p = new Page(query.getPage(), query.getLimit());
        Page<IrLow> page = irLowService.selectByPage(p, params);
        return R.ok("查询成功").put("page",page);
    }
    /**
     * @Title: 删除消息
     * @author: lzl
     * @Description:
     * @param ids
     * @return R
     */
    @RequestMapping("doDeleteLow")
    public R doDeleteLow(@RequestBody Long[] ids){
        try {
            for (int i = 0; i < ids.length; i++) {
                irLowService.deleteById(ids[i]);
            }
            return R.ok("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.ok("删除失败");
        }
    }
    /**
     * @param irLow
     * @return R
     * @Title: 修改消息
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doUpdateLow")
    public R doUpdateLow(@RequestBody IrLow irLow){
        irLowService.updateById(irLow);
        return R.ok("修改成功");
    }
    /**
     * @param irLow
     * @return R
     * @Title: 添加轮播图
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doAddLow")
    public R doAddLow(@RequestBody IrLow irLow){
        irLow.setLowTime(new Date());
        irLowService.insert(irLow);
        return R.ok("添加成功");
    }
	
}
