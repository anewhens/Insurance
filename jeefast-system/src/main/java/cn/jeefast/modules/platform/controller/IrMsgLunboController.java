package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.entity.IrMsgLunbo;
import cn.jeefast.common.entity.IrWheel;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IrMsgLunboService;
import cn.jeefast.modules.platform.service.IrWheelService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cn.jeefast.common.base.BaseController;

import java.util.Date;
import java.util.Map;

/**
 * <p>
 * 首页消息轮播 前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irMsgLunbo")
public class IrMsgLunboController extends BaseController {

    @Autowired
    private IrMsgLunboService irMsgLunboService;

    /**
     * @Title: 获取消息
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    @RequestMapping("/getMsgList")
    public R getMsgList(@RequestBody Map<String, Object> params){
        Query query = new Query(params);
        Page<IrMsgLunbo> p = new Page(query.getPage(), query.getLimit());
        Page<IrMsgLunbo> page = irMsgLunboService.selectByPage(p, params);
        return R.ok("查询成功").put("page",page);
    }
    /**
     * @Title: 删除消息
     * @author: lzl
     * @Description:
     * @param ids
     * @return R
     */
    @RequestMapping("doDeleteMsg")
    public R doDeleteMsg(@RequestBody Long[] ids){
        try {
            for (int i = 0; i < ids.length; i++) {
                irMsgLunboService.deleteById(ids[i]);
            }
            return R.ok("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.ok("删除失败");
        }
    }
    /**
     * @param irMsgLunbo
     * @return R
     * @Title: 修改消息
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doUpdateMsg")
    public R doUpdateMsg(@RequestBody IrMsgLunbo irMsgLunbo){
        irMsgLunboService.updateById(irMsgLunbo);
        return R.ok("修改成功");
    }
    /**
     * @param irMsgLunbo
     * @return R
     * @Title: 添加轮播图
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doAddMsg")
    public R doAddMsg(@RequestBody IrMsgLunbo irMsgLunbo){
        irMsgLunbo.setMlTime(new Date());
        irMsgLunboService.insert(irMsgLunbo);
        return R.ok("添加成功");
    }

	
}
