package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.entity.IrReport;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.common.validator.Assert;
import cn.jeefast.modules.platform.service.IrReportService;
import cn.jeefast.modules.platform.service.IrReportService;
import cn.jeefast.modules.upload.FileUtil;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cn.jeefast.common.base.BaseController;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irReport")
public class IrReportController extends BaseController {
    @Autowired
    private IrReportService irReportService;

    /**
     * @Title: 获取报表
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    @RequestMapping("/getReportList")
    public R getReportList(@RequestBody Map<String, Object> params){
        Query query = new Query(params);
        Page<IrReport> p = new Page(query.getPage(), query.getLimit());
        Page<IrReport> page = irReportService.selectByPage(p, params);
        return R.ok("查询成功").put("page",page);
    }
    /**
     * @Title: 删除报表
     * @author: lzl
     * @Description:
     * @param ids
     * @return R
     */
    @RequestMapping("doDeleteReport")
    public R doDeleteReport(@RequestBody Long[] ids){
        try {
            for (int i = 0; i < ids.length; i++) {
                irReportService.deleteById(ids[i]);
            }
            return R.ok("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.ok("删除失败");
        }
    }
    /**
     * @param irReport
     * @return R
     * @Title: 修改报表
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doUpdateReport")
    public R doUpdateReport(@RequestBody IrReport irReport){
        irReport.setIrrSize("");
        irReportService.updateById(irReport);
        return R.ok("修改成功");
    }
    /**
     * @param irReport
     * @return R
     * @Title: 添加报表
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doAddReport")
    public R doAddReport(@RequestBody IrReport irReport){
        irReport.setIrrTime(new Date());
        //添加上传者
        irReport.setIrrUploader(getUser().getUsername()+getUser().getMobile());
        irReportService.insert(irReport);
        return R.ok("添加成功");
    }
    /**
     * @Title: 下载某个行业报表
     * @author: lzl
     * @Description:
     * @param
     * @return
     */
    @RequestMapping("/downLoadZip")
    public R downLoadZip(String irrId,HttpServletResponse response) throws Exception {
        Assert.isBlank(irrId,"id为空");
        return FileUtil.downLoadZip("irReport"+irrId,response);
    }
    /**
     * @Title: 下载某个行业报表
     * @author: lzl
     * @Description:
     * @param
     * @return
     */
    @RequestMapping("/downLoadZip2")
    public void downLoadZip2(String irrId,HttpServletResponse response) throws Exception {
        Assert.isBlank(irrId,"id为空");
        FileUtil.downLoadZip2("irReport"+irrId,response);
    }

}
