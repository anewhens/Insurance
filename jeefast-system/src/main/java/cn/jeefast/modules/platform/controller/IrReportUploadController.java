package cn.jeefast.modules.platform.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import cn.jeefast.common.base.BaseController;

/**
 * <p>
 * 模板上传记录表 前端控制器
 * </p>
 *
 * @author lzl
 * @since 2020-03-12
 */
@RestController
@RequestMapping("/irReportUpload")
public class IrReportUploadController extends BaseController {
	
}
