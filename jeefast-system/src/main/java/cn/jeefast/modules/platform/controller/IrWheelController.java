package cn.jeefast.modules.platform.controller;


import cn.jeefast.common.entity.IrDict;
import cn.jeefast.common.entity.IrWheel;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.service.IrWheelService;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.jeefast.common.base.BaseController;

import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/irWheel")
public class IrWheelController extends BaseController {

    @Autowired
    private IrWheelService wheelService;

    /**
     * @Title: 获取轮播分类的数量
     * @author: lzl
     * @Description:
     * @param
     * @return
     */
    @RequestMapping("/list")
    public R wheelList(){
        return wheelService.selectWheelList();
    }
    /**
     * @Title: 获取某个类别内的轮播图
     * @author: lzl
     * @Description:
     * @param
     * @return
     */
    @RequestMapping("/getWheelList")
    public R detailList(@RequestBody Map<String, Object> params){
        Query query = new Query(params);
        Page<IrWheel> p = new Page(query.getPage(), query.getLimit());
        Page<IrWheel> page = wheelService.selectByPage(p, params);
        return R.ok("查询成功").put("page",page);
    }

    @RequestMapping("doDeleteWheel")
    public R doDeleteWheel(@RequestBody Long[] ids){
        try {
            for (int i = 0; i < ids.length; i++) {
                wheelService.deleteById(ids[i]);
            }
            return R.ok("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.ok("删除失败");
        }
    }
    /**
     * @param irWheel
     * @return R
     * @Title: 修改轮播图
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doUpdateWheel")
    public R doUpdateWheel(@RequestBody IrWheel irWheel){
        wheelService.updateById(irWheel);
        return R.ok("更新成功");
    }
    /**
     * @param irWheel
     * @return R
     * @Title: 添加轮播图
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doAddWheel")
    public R doAddWheel(@RequestBody IrWheel irWheel){
        wheelService.insert(irWheel);
        return R.ok("添加成功");
    }
}
