package cn.jeefast.modules.platform.controller;

import cn.jeefast.common.base.BaseController;
import cn.jeefast.common.excel.ExcelUtil;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.model.ProgressVO;
import cn.jeefast.model.TbUserVO;
import cn.jeefast.modules.platform.entity.TbUser;
import cn.jeefast.modules.platform.service.TbUserService;
import cn.jeefast.system.entity.SysUser;
import cn.jeefast.system.service.SysUserService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@RestController
@RequestMapping("/tbUser")
public class TbUserController extends BaseController {

    @Autowired
    private TbUserService tbUserService;
    @Autowired
    private SysUserService sysUserService;

    /**
     *
     * @param user
     * @return
     */
    @PostMapping("/addUser")
    public R addUser(TbUser user){
        if(user.getUserId()!=null){
            tbUserService.updateById(user);
            return R.ok("成功").put("data",null);
        }
        EntityWrapper ew = new EntityWrapper();
        ew.eq("mobile",user.getMobile());
        List list = tbUserService.selectList(ew);
        if(list.size()!=0){
            return R.error("已注册").put("data",null);
        }
        user.setPassword(DigestUtils.sha256Hex(user.getPassword()));
        tbUserService.insert(user);
        return R.ok("成功").put("data",null);
    }

    /**
     * @Title: 获取用户
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    @RequestMapping("/getUserList")
    public R getUserList(@RequestBody Map<String, Object> params){
        List<Long> power = sysUserService.getPower(super.getUser());
        Query query = new Query(params);
        query.put("list",power);
        Page<TbUser> p = new Page(query.getPage(), query.getLimit());
        Page<TbUser> page = tbUserService.selectByPage(p, params,power);
        return R.ok("查询成功").put("page",page);
    }
    /**
     * @Title: 删除用户
     * @author: lzl
     * @Description:
     * @param ids
     * @return R
     */
    @RequestMapping("doDeleteUser")
    public R doDeleteUser(@RequestBody Long[] ids){
        try {
            for (int i = 0; i < ids.length; i++) {
                tbUserService.deleteById(ids[i]);
            }
            return R.ok("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.ok("删除失败");
        }
    }
    /**
     * @param tbUser
     * @return R
     * @Title: 修改用户
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doUpdateUser")
    public R doUpdateUser(@RequestBody TbUser tbUser){
        tbUserService.updateById(tbUser);
        return R.ok("修改成功");
    }
    /**
     * @param tbUser
     * @return R
     * @Title: 添加用户
     * @author: lzl
     * @Description:
     */
    @RequestMapping(value = "/doAddUser")
    public R doAddUser(@RequestBody TbUser tbUser){
        tbUser.setCreateTime(new Date());
        tbUserService.insert(tbUser);
        return R.ok("添加成功");
    }
    /**
     * @param response,params
     * @return
     * @Title: 导出前台用户表
     * @author: lzl
     * @Description:
     */
    @RequestMapping("/downloadExcel")
    public void downloadExcel(@RequestParam(value = "userIdList") List<Long> userIdList, HttpServletResponse response) {
        try {
            List<TbUserVO> userList = tbUserService.getUserList(userIdList);
            ExcelUtil.setInfoAndExport(response, "前台用户表", "前台用户表.xls",
                    "tb-user-template.xls", "cn.jeefast.model.TbUserVO", userList);
        } catch (Exception e) {
            logger.error("【导出失败，原因 --> {}】", e.toString());
        }
    }

    /**
     * @param response,params
     * @return
     * @Title: 导出前台用户表
     * @author: lzl
     * @Description:
     */
    @RequestMapping("/downloadExcelAll")
    public void downloadExcelAll(HttpServletResponse response) {
        try {
            List<Long> power = sysUserService.getPower(super.getUser());
            List<TbUser> tbUsers = tbUserService.selectByExport(power);
            List<Long> userIdList = new ArrayList<>();
            for (TbUser user:tbUsers) {
                userIdList.add(user.getUserId());
            }
            List<TbUserVO> userList = tbUserService.getUserList(userIdList);
            ExcelUtil.setInfoAndExport(response, "前台用户表", "前台用户表.xls",
                    "tb-user-template.xls", "cn.jeefast.model.TbUserVO", userList);
        } catch (Exception e) {
            logger.error("【导出失败，原因 --> {}】", e.toString());
        }
    }

    @GetMapping("/changePower")
    public R changePower(Long id){
        TbUser tbUser = tbUserService.selectById(id);
        if(tbUser==null)
            return R.error("无此用户");

        if(tbUser.getPower()==0)
            tbUser.setPower(1);
        else
            tbUser.setPower(0);

        tbUserService.updateById(tbUser);
        return R.ok("成功").put("data",null);
    }

    @GetMapping("/detail")
    public R userDetail(Long userId){
        TbUser tbUser = tbUserService.selectById(userId);
        return R.ok("成功").put("data",tbUser);
    }

    @RequestMapping("/delete")
    public R delete(String ids){
        String[] split = ids.split(",");
        for (String id:split) {
            tbUserService.deleteById(Long.parseLong(id));
        }
        return R.ok("删除成功").put("data",null);

    }
	
}
