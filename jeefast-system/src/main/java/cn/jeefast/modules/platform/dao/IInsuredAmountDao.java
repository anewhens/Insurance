package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IInsuredAmount;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-03-06
 */
public interface IInsuredAmountDao extends BaseMapper<IInsuredAmount> {


    List<Map> listPage(@Param("title")String title, @Param("type")Long type, @Param("comp")Long comp, @Param("page")Integer page, @Param("limit")Integer limit);

    List<Map> listCount(@Param("title")String title, @Param("type")Long type, @Param("comp")Long comp);
}