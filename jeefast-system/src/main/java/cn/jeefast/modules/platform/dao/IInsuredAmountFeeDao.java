package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IInsuredAmountFee;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-03-06
 */
public interface IInsuredAmountFeeDao extends BaseMapper<IInsuredAmountFee> {

}