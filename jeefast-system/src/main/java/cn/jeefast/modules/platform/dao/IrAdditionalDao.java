package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrAdditional;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-03-05
 */
public interface IrAdditionalDao extends BaseMapper<IrAdditional> {

    /**
     * 附加险列表+分页
     * @param page
     * @param limit
     * @return
     */
    List<IrAdditional> additionalListByPage(@Param("page")Integer page,@Param("limit")Integer limit,@Param("title") String title ,@Param("com") Long com);
}