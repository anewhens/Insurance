package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrAdditionalFee;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-03-05
 */
public interface IrAdditionalFeeDao extends BaseMapper<IrAdditionalFee> {

}