package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrBank;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrBankDao extends BaseMapper<IrBank> {
    List<IrBank> selectBankList(@Param("bankType")Integer bankType,@Param("bankName")String bankName,@Param("backParent")Long backParent,@Param("page")Integer page,@Param("limit")Integer limit);
    /**
     * @Title: 获取省行
     * @author: lzl
     * @Description:
     * @param
     * @return
     */
    List<IrBank> selectByPage(Page<IrBank> p, @Param("params") Map<String, Object> params);
    /**
     * @Title: 获取市行
     * @author: lzl
     * @Description:
     * @param
     * @return
     */
    List<IrBank> selectByPage2(Page<IrBank> p,@Param("params")  Map<String, Object> params);
}