package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrClassNews;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrClassNewsDao extends BaseMapper<IrClassNews> {
    /**
     * @Title: 获取资讯
     * @author: lzl
     * @Description:
     * @param params
     * @return  List<IrClassNews>
     */
    List<IrClassNews> selectByPage(Page<IrClassNews> p, @Param("params") Map<String, Object> params);
}