package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrClassType;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrClassTypeDao extends BaseMapper<IrClassType> {
    /**
     * @Title: 获取类别信息
     * @author: lzl
     * @Description:
     * @param p
     * @param params
     * @return  List<IrClassType>
     */
    List<IrClassType> selectByPage(Page<IrClassType> p, @Param("params") Map<String, Object> params);
}