package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrClassVideo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrClassVideoDao extends BaseMapper<IrClassVideo> {
    /**
     * @Title: 获取视频
     * @author: lzl
     * @Description:
     * @param params
     * @return  List<IrClassVideo>
     */
    List<Map> selectByPage(Page<Map> p, @Param("params") Map<String, Object> params,@Param("list")List<Long> list);
}