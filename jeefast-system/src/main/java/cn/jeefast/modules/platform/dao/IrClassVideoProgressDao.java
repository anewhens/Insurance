package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrClassVideoProgress;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrClassVideoProgressDao extends BaseMapper<IrClassVideoProgress> {
    /**
     * @Title: 得到某个视频的学习进度的时间和个数
     * @author: lzl
     * @Description:
     * @param cvId
     * @return List<IrClassVideoProgress>
     */
    Map getListByVideo(@Param("cvId") String cvId);
    /**
     * @Title: 获取某个视频的学习进度
     * @author: lzl
     * @Description:
     * @param p
     * @param params
     * @return List<Map>
     */
    List<IrClassVideoProgress> selectByPage(Page<Map> p, @Param("params") Map<String, Object> params,@Param("list")List<Long> list);
    /**
     * @Title: 得到某个视频的已学习时长
     * @author: lzl
     * @Description:
     * @param cvId
     * @return Double
     */
    Double getStudyTime(@Param("cvId")String cvId);
}