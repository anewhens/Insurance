package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrCode;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrCodeDao extends BaseMapper<IrCode> {

}