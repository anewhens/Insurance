package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrCompany;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrCompanyDao extends BaseMapper<IrCompany> {
    List<IrCompany> selectCompList(@Param("icTitle")String icTitle,@Param("page")Integer page,@Param("limit")Integer limit);
    /**
     * @Title: 获取保险公司
     * @author: lzl
     * @Description:
     * @param p
     * @param params
     * @return List<IrCompany>
     */
    List<IrCompany> selectByPage(Page<IrCompany> p, @Param("params") Map<String, Object> params);
}