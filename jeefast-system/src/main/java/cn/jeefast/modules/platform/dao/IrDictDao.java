package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrDict;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrDictDao extends BaseMapper<IrDict> {
    /**
     * @Title: 获取广告信息
     * @author: lzl
     * @Description:
     * @param
     * @return  List<IrDict>
     */
    List<IrDict> getDictDetail();
}