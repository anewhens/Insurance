package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrEduExam;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrEduExamDao extends BaseMapper<IrEduExam> {
    /**
     * 试题详情
     * @param etId
     * @param page
     * @param limit
     * @return
     */
    List<IrEduExam> selectList1(@Param("etId")Integer etId,@Param("page")Integer page,@Param("limit")Integer limit,@Param("typeId")Long typeId,@Param("title")String title);
}