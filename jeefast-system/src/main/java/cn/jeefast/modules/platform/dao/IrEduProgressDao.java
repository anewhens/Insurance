package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrEduProgress;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrEduProgressDao extends BaseMapper<IrEduProgress> {

    /**
     * 条件查询
     * @param mobile
     * @param username
     * @return
     */
    List<Map> selectExamList(@Param("mobile")String mobile,@Param("username")String username,@Param("list")List<Long> list,@Param("etId") Long etId);

    /**
     * 获取详情
     * @param userId
     * @param etId
     * @return
     */
    List<Map> eduInfoDetail(@Param("userId")Long userId,@Param("etId")Long etId);

    /**
     * 获取分数
     * @param etId
     * @param userId
     * @return
     */
    Integer getSocByUserAndEtId(@Param("etId")Long etId,@Param("userId")Long userId);

    Integer getSumScore(@Param("userId")Long userId,@Param("etId")Long etId);
}