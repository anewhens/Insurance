package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrEduTime;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrEduTimeDao extends BaseMapper<IrEduTime> {

    /**
     * 列表分页
     * @param page
     * @param limit
     * @return
     */
    List<IrEduTime> selectList1(@Param("page")Integer page,@Param("limit")Integer limit);
}