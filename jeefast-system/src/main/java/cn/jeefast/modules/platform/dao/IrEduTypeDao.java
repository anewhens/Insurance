package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrEduType;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrEduTypeDao extends BaseMapper<IrEduType> {
    /**
     * @Title: 获取类别信息
     * @author: lzl
     * @Description:
     * @param p
     * @param params
     * @return  List<IrEduType>
     */
    List<IrEduType> selectByPage(Page<IrEduType> p, @Param("params") Map<String, Object> params);
}