package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrFeedback;
import cn.jeefast.common.entity.IrNews;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrFeedbackDao extends BaseMapper<IrFeedback> {
    /**
     * @Title: 获取反馈
     * @author: lzl
     * @Description:
     * @param params
     * @return  List<IrNews>
     */
    List<IrFeedback> selectByPage(Page<IrFeedback> p, @Param("params") Map<String, Object> params);
}