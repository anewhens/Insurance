package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrInsuranceNews;
import cn.jeefast.common.entity.IrNews;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrInsuranceNewsDao extends BaseMapper<IrInsuranceNews> {
    /**
     * @Title: 获取消息
     * @author: lzl
     * @Description:
     * @param params
     * @return  List<IrInsuranceNews>
     */
    List<IrInsuranceNews> selectByPage(Page<IrInsuranceNews> p, @Param("params") Map<String, Object> params);
}