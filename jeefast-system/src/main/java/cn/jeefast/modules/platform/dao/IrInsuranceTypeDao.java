package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrInsuranceType;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrInsuranceTypeDao extends BaseMapper<IrInsuranceType> {


    List<IrInsuranceType> typeListByPaye(@Param("page")Integer page,@Param("limit")Integer limit);
    /**
     * @Title: 获取保险种类
     * @author: lzl
     * @Description:
     * @param p
     * @param params
     * @return List<IrInsuranceType>
     */
    List<IrInsuranceType> selectByPage(Page<IrInsuranceType> p,@Param("params") Map<String, Object> params);
}