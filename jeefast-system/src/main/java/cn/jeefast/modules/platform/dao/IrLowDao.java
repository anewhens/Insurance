package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrLow;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrLowDao extends BaseMapper<IrLow> {
    /**
     * @Title: 获取消息
     * @author: lzl
     * @Description:
     * @param params
     * @return List<IrLow>
     */
    List<IrLow> selectByPage(Page<IrLow> p, @Param("params") Map<String, Object> params);
}