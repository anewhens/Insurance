package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrMsgLunbo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

/**
 * <p>
  * 首页消息轮播 Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrMsgLunboDao extends BaseMapper<IrMsgLunbo> {
    /**
     * @Title: 获取消息
     * @author: lzl
     * @Description:
     * @param params
     * @return  List<IrMsgLunbo>
     */
    List<IrMsgLunbo> selectByPage(Page<IrMsgLunbo> p, @Param("params") Map<String, Object> params);
}