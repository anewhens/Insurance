package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrReport;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrReportDao extends BaseMapper<IrReport> {
    /**
     * @Title: 得到行业报表
     * @author: lzl
     * @Description:
     * @param p
     * @param params
     * @return List<IrReport>
     */
    List<IrReport> selectByPage(Page<IrReport> p, @Param("params") Map<String, Object> params);
}