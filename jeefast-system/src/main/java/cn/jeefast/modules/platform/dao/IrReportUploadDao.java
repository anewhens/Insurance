package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrReportUpload;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
  * 模板上传记录表 Mapper 接口
 * </p>
 *
 * @author lzl
 * @since 2020-03-12
 */
public interface IrReportUploadDao extends BaseMapper<IrReportUpload> {
    /**
     * @Title: 得到某个报表的上传个数
     * @author: lzl
     * @Description:
     * @param irrId
     * @return
     */
    Integer getNumber(@Param("irrId") Long irrId);
}