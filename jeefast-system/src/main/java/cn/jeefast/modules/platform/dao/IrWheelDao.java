package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrWheel;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrWheelDao extends BaseMapper<IrWheel> {

    /**
     * 轮播列表
     * @return
     */
    List<Map> selectList();

    /**
     * 根据类别查询列表
     * @param typeId
     * @param page
     * @param limit
     * @return
     */
    List<IrWheel> selectDetailList(@Param("typeId")Integer typeId,@Param("page")Integer page,@Param("limit")Integer limit);
    /**
     * 根据类别查询列表
     * @param p
     * @param params
     * @param
     * @return List<IrWheel>
     */
    List<IrWheel> selectByPage(Page<IrWheel> p, @Param("params")Map<String, Object> params);
    /**
     * 得到某类轮播图个数
     * @param
     * @param type
     * @param
     * @return Integer
     */
    Integer getNumber(@Param("type") String type);
}