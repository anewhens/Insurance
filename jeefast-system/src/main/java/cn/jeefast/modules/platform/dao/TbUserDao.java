package cn.jeefast.modules.platform.dao;

import cn.jeefast.common.entity.IrNews;
import cn.jeefast.modules.platform.entity.TbUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface TbUserDao extends BaseMapper<TbUser> {
    /**
     * @Title: 获取用户
     * @author: lzl
     * @Description:
     * @param params
     * @return  List<IrNews>
     */
    List<TbUser> selectByPage(Page<TbUser> p, @Param("params") Map<String, Object> params,@Param("list")List<Long> list);

    /**
     * 导出全部用户
     * @param list
     * @return
     */
    List<TbUser> selectByExport(@Param("list")List<Long> list);
    /**
     * @Title: 得到某行的总人数
     * @author: lzl
     * @Description:
     * @param bankId
     * @return List<Long>
     */
    List<Long> getUserList(@Param("bankId")String bankId);
}