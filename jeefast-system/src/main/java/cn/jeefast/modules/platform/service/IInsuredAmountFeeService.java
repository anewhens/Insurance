package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IInsuredAmountFee;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-03-06
 */
public interface IInsuredAmountFeeService extends IService<IInsuredAmountFee> {
	
}
