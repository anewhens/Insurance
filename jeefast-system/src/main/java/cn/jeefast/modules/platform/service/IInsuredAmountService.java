package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IInsuredAmount;
import com.baomidou.mybatisplus.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-03-06
 */
public interface IInsuredAmountService extends IService<IInsuredAmount> {

    List<Map> listPage(String title, Long type,Long comp, Integer page, Integer limit);

    List<Map> listCount(String title, Long type,Long comp);
}
