package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrAdditional;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-03-05
 */
public interface IrAdditionalService extends IService<IrAdditional> {

    /**
     *附加险列表+分页
     * @param page
     * @param limit
     * @return
     */
    List<IrAdditional> additionalListByPage(Integer page,Integer limit,String title,Long com);
}
