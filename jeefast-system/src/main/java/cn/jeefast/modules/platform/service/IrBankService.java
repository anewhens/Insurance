package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrBank;
import cn.jeefast.common.entity.IrNews;
import cn.jeefast.modules.platform.dao.IrBankDao;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrBankService extends IService<IrBank> {


    List<IrBank> selectBankList(Integer bankType, String bankName, Long backParent, Integer page, Integer limit);
    /**
     * @Title: 获取省行
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    Page<IrBank> selectByPage(Page<IrBank> p, Map<String, Object> params);
    /**
     * @Title: 获取市行
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    Page<IrBank> selectByPage2(Page<IrBank> p, Map<String, Object> params);
}
