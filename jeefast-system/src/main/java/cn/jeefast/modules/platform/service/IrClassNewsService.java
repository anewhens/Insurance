package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrClassNews;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrClassNewsService extends IService<IrClassNews> {
    /**
     * @Title: 获取资讯
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    Page<IrClassNews> selectByPage(Page<IrClassNews> p, Map<String, Object> params);
}
