package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrClassType;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrClassTypeService extends IService<IrClassType> {
    /**
     * @Title: 获取类型信息
     * @author: lzl
     * @Description:
     * @param p
     * @param params
     * @return
     */
    Page<IrClassType> selectByPage(Page<IrClassType> p, Map<String, Object> params);
}
