package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrClassVideoProgress;
import cn.jeefast.model.ProgressVO;
import cn.jeefast.system.entity.SysUser;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrClassVideoProgressService extends IService<IrClassVideoProgress> {
    /**
     * @Title: 获取学习进度
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    Page<Map> selectByPage(Page<Map> p, Map<String, Object> params, SysUser user);
    /**
     * @Title: 获取导出的学习进度
     * @author: lzl
     * @Description:
     * @param cvpIdList
     * @return List<ProgressVO>
     */
    List<ProgressVO> getProgressList(List<Long> cvpIdList);
}
