package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrClassVideo;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrClassVideoService extends IService<IrClassVideo> {
    /**
     * @Title: 获取视频
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    Page<Map> selectByPage(Page<Map> p, Map<String, Object> params, List<Long> list);
}
