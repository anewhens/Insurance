package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrCode;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrCodeService extends IService<IrCode> {
	
}
