package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrCompany;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrCompanyService extends IService<IrCompany> {

    List<IrCompany> selectCompList(String icTitle, Integer page, Integer limit);
    /**
     * @Title: 获取保险公司
     * @author: lzl
     * @Description:
     * @param p
     * @param params
     * @return
     */
    Page<IrCompany> selectByPage(Page<IrCompany> p, Map<String, Object> params);
}
