package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrDict;
import cn.jeefast.common.utils.R;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrDictService extends IService<IrDict> {
    /**
     * @param
     * @return R
     * @Title: 获取广告信息
     * @author: lzl
     * @Description:
     */
    R getDictDetail();
}
