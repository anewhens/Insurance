package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrEduExam;
import cn.jeefast.common.utils.R;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrEduExamService extends IService<IrEduExam> {

    /**
     * 根据批次ID查询试题
     * @param etId
     * @param page
     * @param limit
     * @return
     */
    R examList(Integer etId,Integer page,Integer limit,Long typeId,String title);
}
