package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrEduProgress;
import cn.jeefast.common.utils.R;
import cn.jeefast.system.entity.SysUser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrEduProgressService extends IService<IrEduProgress> {

    /**
     * 查询考试 结果列表
     * @param isPass
     * @param mobile
     * @param username
     * @return
     */
    R selectExamList(Integer isPass, String mobile, String username, Integer page, Integer limit, SysUser user,Long etId);

    /**
     * 考试分数详情
     * @param userId
     * @param etId
     * @return
     */
    R eduInfoDetail(Long userId,Long etId);
}
