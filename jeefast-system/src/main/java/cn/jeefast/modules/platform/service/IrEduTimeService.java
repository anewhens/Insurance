package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrEduTime;
import cn.jeefast.common.utils.R;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrEduTimeService extends IService<IrEduTime> {

    /**
     * 查询列表
     * @param page
     * @param limit
     * @return
     */
    R selectList1(Integer page,Integer limit);

    /**
     * 查询最新期的ID
     * @return
     */
    Long  lastSignId();
}
