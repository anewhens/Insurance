package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrEduType;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrEduTypeService extends IService<IrEduType> {
    /**
     * @Title: 获取类别信息
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    Page<IrEduType> selectByPage(Page<IrEduType> p, Map<String, Object> params);
}
