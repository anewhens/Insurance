package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrFeedback;
import cn.jeefast.common.entity.IrNews;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrFeedbackService extends IService<IrFeedback> {
    /**
     * @Title: 获取反馈
     * @author: zdw
     * @Description:
     * @param params
     * @return R
     */
    Page<IrFeedback> selectByPage(Page<IrFeedback> p, Map<String, Object> params);
}
