package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrInsuranceNews;
import cn.jeefast.common.entity.IrNews;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrInsuranceNewsService extends IService<IrInsuranceNews> {
    /**
     * @Title: 获取消息
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    Page<IrInsuranceNews> selectByPage(Page<IrInsuranceNews> p, Map<String, Object> params);
}
