package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrInsuranceType;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrInsuranceTypeService extends IService<IrInsuranceType> {

    List<IrInsuranceType> typeListByPaye(Integer page, Integer limit);
    /**
     * @Title: 获取保险种类
     * @author: lzl
     * @Description:
     * @param p
     * @param params
     * @return
     */
    Page<IrInsuranceType> selectByPage(Page<IrInsuranceType> p, @Param("params") Map<String, Object> params);
}
