package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrLow;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrLowService extends IService<IrLow> {
    /**
     * @Title: 获取消息
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    Page<IrLow> selectByPage(Page<IrLow> p, Map<String, Object> params);
}
