package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrMsgLunbo;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 * 首页消息轮播 服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrMsgLunboService extends IService<IrMsgLunbo> {
    /**
     * @Title: 获取消息
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    Page<IrMsgLunbo> selectByPage(Page<IrMsgLunbo> p, Map<String, Object> params);
}
