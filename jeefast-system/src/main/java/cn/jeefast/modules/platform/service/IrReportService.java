package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrReport;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrReportService extends IService<IrReport> {
    /**
     * @Title: 获取报表
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    Page<IrReport> selectByPage(Page<IrReport> p, Map<String, Object> params);
}
