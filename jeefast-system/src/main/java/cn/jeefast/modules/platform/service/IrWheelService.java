package cn.jeefast.modules.platform.service;

import cn.jeefast.common.entity.IrWheel;
import cn.jeefast.common.utils.R;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface IrWheelService extends IService<IrWheel> {

    /**
     * 查询轮播列表
     * @return
     */
    R selectWheelList();

    /**
     * 根据类别详情列表
     * @return
     */
    R selectDetailList(Integer typeId, Integer page, Integer limit);
    /**
     * @Title: 查询某个分类的轮播图
     * @author: lzl
     * @Description:
     * @param p
     * @param params
     * @return Page<IrWheel>
     */
    Page<IrWheel> selectByPage(Page<IrWheel> p, Map<String, Object> params);
}
