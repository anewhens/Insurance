package cn.jeefast.modules.platform.service;

import cn.jeefast.model.TbUserVO;
import cn.jeefast.modules.platform.entity.TbUser;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
public interface TbUserService extends IService<TbUser> {
    /**
     * @Title: 获取用户
     * @author: lzl
     * @Description:
     * @param params
     * @return R
     */
    Page<TbUser> selectByPage(Page<TbUser> p, Map<String, Object> params,List<Long> list);
    /**
     * @param userIdList
     * @return List<TbUserVO>
     * @Title: 导出前台用户表
     * @author: lzl
     * @Description:
     */
    List<TbUserVO> getUserList(List<Long> userIdList);

    /**
     * 导出全部
     * @param list
     * @return
     */
    List<TbUser> selectByExport(List<Long> list);
}
