package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IInsuredAmountFee;
import cn.jeefast.modules.platform.dao.IInsuredAmountFeeDao;
import cn.jeefast.modules.platform.service.IInsuredAmountFeeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-03-06
 */
@Service
public class IInsuredAmountFeeServiceImpl extends ServiceImpl<IInsuredAmountFeeDao, IInsuredAmountFee> implements IInsuredAmountFeeService {
	
}
