package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IInsuredAmount;
import cn.jeefast.modules.platform.dao.IInsuredAmountDao;
import cn.jeefast.modules.platform.service.IInsuredAmountService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-03-06
 */
@Service
public class IInsuredAmountServiceImpl extends ServiceImpl<IInsuredAmountDao, IInsuredAmount> implements IInsuredAmountService {

    @Autowired
    private IInsuredAmountDao iInsuredAmountDao;

    @Override
    public List<Map> listPage(String title, Long type, Long comp, Integer page, Integer limit) {
        return iInsuredAmountDao.listPage(title,type,comp,page,limit);
    }

    @Override
    public List<Map> listCount(String title, Long type, Long comp) {
        return iInsuredAmountDao.listCount(title, type, comp);
    }
}
