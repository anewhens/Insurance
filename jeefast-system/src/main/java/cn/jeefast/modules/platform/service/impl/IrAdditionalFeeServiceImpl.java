package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrAdditionalFee;
import cn.jeefast.modules.platform.dao.IrAdditionalFeeDao;
import cn.jeefast.modules.platform.service.IrAdditionalFeeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-03-05
 */
@Service
public class IrAdditionalFeeServiceImpl extends ServiceImpl<IrAdditionalFeeDao, IrAdditionalFee> implements IrAdditionalFeeService {
	
}
