package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrAdditional;
import cn.jeefast.modules.platform.dao.IrAdditionalDao;
import cn.jeefast.modules.platform.service.IrAdditionalService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-03-05
 */
@Service
public class IrAdditionalServiceImpl extends ServiceImpl<IrAdditionalDao, IrAdditional> implements IrAdditionalService {

    @Autowired
    private IrAdditionalDao irAdditionalDao;

    @Override
    public List<IrAdditional> additionalListByPage(Integer page, Integer limit,String title ,Long com) {
        List<IrAdditional> irAdditionals = irAdditionalDao.additionalListByPage(page, limit,title,com);
        return irAdditionals;
    }
}
