package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrBank;
import cn.jeefast.common.entity.IrNews;
import cn.jeefast.modules.platform.dao.IrBankDao;
import cn.jeefast.modules.platform.service.IrBankService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrBankServiceImpl extends ServiceImpl<IrBankDao, IrBank> implements IrBankService {

    @Autowired
    private IrBankDao bankDao;

    @Override
    public List<IrBank> selectBankList(Integer bankType, String bankName, Long backParent, Integer page, Integer limit) {
        return bankDao.selectBankList(bankType,bankName,backParent,page,limit);
    }

    @Override
    public Page<IrBank> selectByPage(Page<IrBank> p, Map<String, Object> params) {
        List<IrBank> list=bankDao.selectByPage(p,params);
        return p.setRecords(list);
    }
    @Override
    public Page<IrBank> selectByPage2(Page<IrBank> p, Map<String, Object> params) {
        List<IrBank> list=bankDao.selectByPage2(p,params);
        return p.setRecords(list);
    }
}
