package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrClassNews;
import cn.jeefast.common.entity.IrClassType;
import cn.jeefast.modules.platform.dao.IrClassNewsDao;
import cn.jeefast.modules.platform.dao.IrClassTypeDao;
import cn.jeefast.modules.platform.service.IrClassNewsService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrClassNewsServiceImpl extends ServiceImpl<IrClassNewsDao, IrClassNews> implements IrClassNewsService {
    @Resource
    private IrClassNewsDao irClassNewsDao;
    @Resource
    private IrClassTypeDao irClassTypeDao;
    @Override
    public Page<IrClassNews> selectByPage(Page<IrClassNews> p, Map<String, Object> params) {
        List<IrClassNews> list=irClassNewsDao.selectByPage(p,params);
        for (IrClassNews irClassNews:list
             ) {
            IrClassType irClassType = irClassTypeDao.selectById(irClassNews.getCnType());
            irClassNews.setTypeName(irClassType==null?"":irClassType.getIctTitle());
        }
        return p.setRecords(list);
    }
}
