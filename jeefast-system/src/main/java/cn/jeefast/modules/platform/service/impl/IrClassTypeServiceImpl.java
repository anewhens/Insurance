package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrClassType;
import cn.jeefast.modules.platform.dao.IrClassTypeDao;
import cn.jeefast.modules.platform.service.IrClassTypeService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrClassTypeServiceImpl extends ServiceImpl<IrClassTypeDao, IrClassType> implements IrClassTypeService {
    @Resource
    private IrClassTypeDao irClassTypeDao;
    @Override
    public Page<IrClassType> selectByPage(Page<IrClassType> p, Map<String, Object> params) {
        List<IrClassType> list=irClassTypeDao.selectByPage(p,params);
        return p.setRecords(list);
    }
}
