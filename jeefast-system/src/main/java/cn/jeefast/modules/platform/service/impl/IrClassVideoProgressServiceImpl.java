package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrBank;
import cn.jeefast.common.entity.IrClassVideo;
import cn.jeefast.common.entity.IrClassVideoProgress;
import cn.jeefast.model.ProgressVO;
import cn.jeefast.modules.platform.dao.IrBankDao;
import cn.jeefast.modules.platform.dao.IrClassVideoProgressDao;
import cn.jeefast.modules.platform.dao.TbUserDao;
import cn.jeefast.modules.platform.entity.TbUser;
import cn.jeefast.modules.platform.service.IrClassVideoProgressService;
import cn.jeefast.modules.platform.service.IrClassVideoService;
import cn.jeefast.system.entity.SysUser;
import cn.jeefast.system.service.SysUserService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrClassVideoProgressServiceImpl extends ServiceImpl<IrClassVideoProgressDao, IrClassVideoProgress> implements IrClassVideoProgressService {
    @Resource
    private IrClassVideoProgressDao irClassVideoProgressDao;
    @Resource
    private TbUserDao tbUserDao;
    @Resource
    private IrBankDao irBankDao;
    @Resource
    private IrClassVideoService videoService;
    @Resource
    private SysUserService userService;

    @Override
    public Page<Map> selectByPage(Page<Map> p, Map<String, Object> params, SysUser user) {
        List<Long> power = userService.getPower(user);
        List<Map> result = new ArrayList<>();
        List<IrClassVideoProgress> list = irClassVideoProgressDao.selectByPage(p, params,power);
        for (IrClassVideoProgress irClassVideoProgress : list
        ) {
            Map map = new HashMap();
            map.put("cvpId",irClassVideoProgress.getCvpId());
            map.put("cvpStatus",irClassVideoProgress.getCvpStatus());
            IrClassVideo irClassVideo = videoService.selectById(irClassVideoProgress.getCvId());
            map.put("must",irClassVideo.getCvLegth());
            map.put("time",irClassVideoProgress.getCvpProgress());
            TbUser tbUser = tbUserDao.selectById(irClassVideoProgress.getUserId());
            if(tbUser!=null){
                map.put("userName",tbUser.getUsername());
                map.put("userMobile",tbUser.getMobile());
                IrBank irBank = irBankDao.selectById(tbUser.getProvinceBank());
                IrBank bank = irBankDao.selectById(tbUser.getCityBank());
                map.put("provinceBankName",irBank==null?"":irBank.getBankName());
                map.put("cityBankName",bank==null?"":bank.getBankName());
                map.put("subBank",tbUser.getSubBank());
                map.put("location",tbUser.getLocation());

            }else {
                map.put("userName","");
                map.put("userMobile","");
                map.put("provinceBankName","");
                map.put("cityBankName","");
                map.put("subBank","");
                map.put("location","");
            }
            result.add(map);
        }
        return p.setRecords(result);
    }

    @Override
    public List<ProgressVO> getProgressList(List<Long> cvpIdList) {
        List<ProgressVO> list=new ArrayList<>();
        for (Long id:cvpIdList
             ) {
            ProgressVO progressVO=new ProgressVO();
            IrClassVideoProgress irClassVideoProgress = irClassVideoProgressDao.selectById(id);
            progressVO.setCvpId(id+"");
            if(irClassVideoProgress!=null){
                if(irClassVideoProgress.getCvpStatus()==0){
                    progressVO.setStatus("未开始");
                }else if(irClassVideoProgress.getCvpStatus()==1){
                    progressVO.setStatus("学习中");
                }else {
                    progressVO.setStatus("已完成");
                }
                TbUser tbUser = tbUserDao.selectById(irClassVideoProgress.getUserId());
                IrBank irBank = irBankDao.selectById(tbUser==null?null:tbUser.getProvinceBank());
                IrBank bank = irBankDao.selectById(tbUser==null?null:tbUser.getCityBank());
                progressVO.setUserName(tbUser==null?"":tbUser.getUsername());
                progressVO.setUserMobile(tbUser==null?"":tbUser.getMobile());
                progressVO.setProvinceBankName(irBank==null?"":irBank.getBankName());
                progressVO.setCityBankName(bank==null?"":bank.getBankName());

            }else {
                progressVO.setStatus("");
                progressVO.setUserName("");
                progressVO.setUserMobile("");
                progressVO.setProvinceBankName("");
                progressVO.setUserMobile("");
            }
            list.add(progressVO);
        }
        return list;
    }
}
