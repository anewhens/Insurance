package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrClassVideo;
import cn.jeefast.common.entity.IrClassVideoProgress;
import cn.jeefast.common.entity.IrCompany;
import cn.jeefast.common.entity.IrInsuranceType;
import cn.jeefast.common.utils.DoubleUtils;
import cn.jeefast.common.utils.MapObjectUtils;
import cn.jeefast.modules.platform.dao.*;
import cn.jeefast.modules.platform.service.IrClassVideoService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrClassVideoServiceImpl extends ServiceImpl<IrClassVideoDao, IrClassVideo> implements IrClassVideoService {
    @Resource
    private IrClassVideoDao irClassVideoDao;
    @Resource
    private IrCompanyDao irCompanyDao;
    @Resource
    private TbUserDao tbUserDao;
    @Resource
    private IrInsuranceTypeDao irInsuranceTypeDao;
    @Resource
    private IrClassVideoProgressDao irClassVideoProgressDao;
    @Override
    public Page<Map> selectByPage(Page<Map> p, Map<String, Object> params,List<Long> l) {
        List<Map> list=irClassVideoDao.selectByPage(p,params,l);
        for (Map map:list
             ) {
            //添加保险公司名称和类别名称
            IrCompany irCompany = irCompanyDao.selectById(MapObjectUtils.getObject(map.get("cpId")));
            IrInsuranceType irInsuranceType = irInsuranceTypeDao.selectById(MapObjectUtils.getObject(map.get("itId")));
            map.put("companyName",irCompany==null?"":irCompany.getIcTitle());
            map.put("typeName",irInsuranceType==null?"":irInsuranceType.getIitTitle());
            //得到该视频应该观看的人数，先得到省行
            List<Long> userList=new ArrayList<>();
            for (String bankId:MapObjectUtils.getObject(map.get("cvBank")).split(",")
                 ) {
                //得到该行的总人数
                List<Long> idList=tbUserDao.getUserList(bankId);
                userList.addAll(idList);
            }
            //得到应学习人数
            Integer number=userList.size();
            //得到单个视频的时长
            IrClassVideo irClassVideo=irClassVideoDao.selectById(MapObjectUtils.getObject(map.get("cvId")));
            map.put("totalTime",irClassVideo==null?"0":irClassVideo.getCvLegth()==null?"0": DoubleUtils.getDouble(irClassVideo.getCvLegth()*number));
            //得到某个视频的已学习时长
            Double studyTime=irClassVideoProgressDao.getStudyTime(MapObjectUtils.getObject(map.get("cvId")));
            map.put("studyTime",DoubleUtils.getDouble(studyTime));//保留两位小数
        }
        return p.setRecords(list);
    }
}
