package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrCode;
import cn.jeefast.modules.platform.dao.IrCodeDao;
import cn.jeefast.modules.platform.service.IrCodeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrCodeServiceImpl extends ServiceImpl<IrCodeDao, IrCode> implements IrCodeService {
	
}
