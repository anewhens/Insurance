package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrCompany;
import cn.jeefast.modules.platform.dao.IrCompanyDao;
import cn.jeefast.modules.platform.service.IrCompanyService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrCompanyServiceImpl extends ServiceImpl<IrCompanyDao, IrCompany> implements IrCompanyService {

    @Autowired
    private IrCompanyDao companyDao;

    @Override
    public List<IrCompany> selectCompList(String icTitle, Integer page, Integer limit) {
        return companyDao.selectCompList(icTitle,page,limit);
    }

    @Override
    public Page<IrCompany> selectByPage(Page<IrCompany> p, Map<String, Object> params) {
        List<IrCompany> list=companyDao.selectByPage(p,params);
        return p.setRecords(list);
    }
}
