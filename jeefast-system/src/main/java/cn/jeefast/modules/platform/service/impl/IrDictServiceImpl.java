package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrDict;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.dao.IrDictDao;
import cn.jeefast.modules.platform.service.IrDictService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrDictServiceImpl extends ServiceImpl<IrDictDao, IrDict> implements IrDictService {
    @Resource
    private IrDictDao irDictDao;
    @Override
    public R getDictDetail() {
        List<IrDict> list=irDictDao.getDictDetail();
        return R.ok().put("list",list);
    }
}
