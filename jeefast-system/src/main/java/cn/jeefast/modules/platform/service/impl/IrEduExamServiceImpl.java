package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrEduExam;
import cn.jeefast.common.entity.IrEduType;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.dao.IrEduExamDao;
import cn.jeefast.modules.platform.service.IrEduExamService;
import cn.jeefast.modules.platform.service.IrEduTypeService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrEduExamServiceImpl extends ServiceImpl<IrEduExamDao, IrEduExam> implements IrEduExamService {

    @Autowired
    private IrEduExamDao eduExamDao;

    @Autowired
    private IrEduTypeService eduTypeService;

    @Override
    public R examList(Integer etId, Integer page, Integer limit,Long typeId,String title) {
        List<IrEduExam> irEduExams = eduExamDao.selectList1(etId, page, limit,typeId,title);
        for (IrEduExam iee:irEduExams) {
            IrEduType irEduType = eduTypeService.selectById(iee.getEduType());
            iee.setTypeName(irEduType.getEtypeTitle());
        }

        EntityWrapper ew = new EntityWrapper();
        ew.eq("et_id",etId);
        List<IrEduExam> irEduExams1 = eduExamDao.selectList(ew);
        Map map = new HashMap();
        map.put("list",irEduExams);
        map.put("count",irEduExams1.size());
        return R.ok("查询成功").put("data",map);
    }
}
