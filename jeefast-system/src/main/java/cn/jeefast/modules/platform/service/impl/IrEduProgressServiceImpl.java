package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrBank;
import cn.jeefast.common.entity.IrEduExam;
import cn.jeefast.common.entity.IrEduProgress;
import cn.jeefast.common.entity.IrEducation;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.dao.IrBankDao;
import cn.jeefast.modules.platform.dao.IrEduExamDao;
import cn.jeefast.modules.platform.dao.IrEduProgressDao;
import cn.jeefast.modules.platform.dao.IrEduTimeDao;
import cn.jeefast.modules.platform.service.IrEduProgressService;
import cn.jeefast.modules.platform.service.IrEduTimeService;
import cn.jeefast.modules.platform.service.IrEducationService;
import cn.jeefast.system.entity.SysUser;
import cn.jeefast.system.service.SysUserService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrEduProgressServiceImpl extends ServiceImpl<IrEduProgressDao, IrEduProgress> implements IrEduProgressService {

    @Autowired
    private IrEduProgressDao eduProgressDao;

    @Autowired
    private IrBankDao bankDao;

    @Autowired
    private SysUserService userService;

    @Autowired
    private IrEduTimeService timeService;

    @Autowired
    private IrEducationService educationService;

    @Autowired
    private IrEduExamDao eduExamDao;
    @Override
    public R selectExamList(Integer isPass, String mobile, String username, Integer page, Integer limit, SysUser user,Long etId) {
        List<Long> power = userService.getPower(user);
        //判断批次ID是否为空
        if(etId==null){
            etId = timeService.lastSignId();
        }
        List<Map> maps = eduProgressDao.selectExamList(mobile, username,power,etId);
        //查询分数
        for (Map m:maps) {
//            //查询总分
            Integer sumScore = eduProgressDao.getSumScore((Long) m.get("userId"), (Long) m.get("etId"));
            m.replace("score",sumScore);
//            //查询报名信息
            EntityWrapper ew = new EntityWrapper();
            ew.eq("et_id",(Long) m.get("etId"));
            ew.eq("user_id",(Long) m.get("userId"));
            List<IrEducation> list = educationService.selectList(ew);
            if(list.size()!=1){
                continue;
            }
            boolean finish = isFinish((Long) m.get("etId"), list.get(0).getIeIsDt(), (Long) m.get("userId"));
            if(finish){
                //判断是否够了60分,只判断普通，投联的在是否完成中已经判断了。
                if (sumScore>=60){
                    m.put("isPass",1);
                }else{
                    m.put("isPass",0);
                }
            }else{
                m.put("isPass",0);
            }
        }
        //符合条件总数
        int count = maps.size();
        List<Map> result = new ArrayList<>();
        //分页
        for (int i = page; i < (page+limit>count?count:page+limit ); i++) {
            result.add(maps.get(i));
        }
        for (Map m:result) {
            Long provinceBank = (Long) m.get("provinceBank");
            IrBank pb =bankDao.selectById(provinceBank);
            Long cityBank = (Long) m.get("cityBank");
            IrBank cb=bankDao.selectById(cityBank);
            if(pb!=null){
                m.put("province",pb.getBankName());
            }
            if(cb!=null){
                m.put("city",cb.getBankName());
            }
        }
        Map res = new HashMap();
        res.put("count",count);
        res.put("list",result);
        return R.ok("查询成功").put("data",res);
    }

    @Override
    public R eduInfoDetail(Long userId, Long etId) {
        List<Map> maps = eduProgressDao.eduInfoDetail(userId, etId);
        return R.ok("成功").put("data",maps);
    }

    /**
     * 检查所在批次视频是否都观看完
     * @return
     */
    public boolean isFinish(Long etId,Integer isDt,Long userId){
        //查询当前批次所有视频
        EntityWrapper ew = new EntityWrapper();
        ew.eq("et_id",etId);
        //如果非投联，只查询非投联试题
        if(isDt==0){
            ew.ne("ee_is_dt",1);
        }
        //查询试题
        List<IrEduExam> list = eduExamDao.selectList(ew);
        //投联用户判断满分是否够了10分。
        if(isDt==1){
            int num =0;
            for (IrEduExam ee:list) {
                if(ee.getEeIsDt()==1){
                    EntityWrapper ew0 = new EntityWrapper();
                    ew0.eq("user_id",userId);
                    ew0.eq("ee_id",ee.getEeId());
                    List<IrEduProgress> list1 = eduProgressDao.selectList(ew0);
                    if(list1.size()==1){
                        num += list1.get(0).getEpScore();
                    }
                }
            }
            if(num<10){
                return false;
            }
        }
        for (IrEduExam ie:list
        ) {
            EntityWrapper ew0 = new EntityWrapper();
            ew0.eq("ee_id",ie.getEeId());
            ew0.eq("user_id",userId);
            List<IrEduProgress> list1 = eduProgressDao.selectList(ew0);
            if(list1.size()==0){
                return false;
            }
            if(!list1.get(0).getEpVideoProgress().equals("-1")){
                return false;
            }
        }
        return true;
    }
}
