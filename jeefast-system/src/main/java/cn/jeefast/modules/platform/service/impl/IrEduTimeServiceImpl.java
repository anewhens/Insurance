package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrEduTime;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.dao.IrEduTimeDao;
import cn.jeefast.modules.platform.service.IrEduTimeService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrEduTimeServiceImpl extends ServiceImpl<IrEduTimeDao, IrEduTime> implements IrEduTimeService {

    @Autowired
    private IrEduTimeDao eduTimeDao;

    @Override
    public R selectList1(Integer page, Integer limit) {
        List<IrEduTime> irEduTimes = eduTimeDao.selectList1(page, limit);
        List<IrEduTime> irEduTimes1 = eduTimeDao.selectList(new EntityWrapper<>());
        Map map = new HashMap();
        map.put("list",irEduTimes);
        map.put("count",irEduTimes1.size());
        return R.ok("成功").put("data",map);
    }

    @Override
    public Long lastSignId() {
        EntityWrapper ew = new EntityWrapper();
        ew.orderBy("et_id",false);
        List<IrEduTime> list = eduTimeDao.selectList(ew);
        if(list.size()==0){
            return -1L;
        }
        return list.get(0).getEtId();
    }
}
