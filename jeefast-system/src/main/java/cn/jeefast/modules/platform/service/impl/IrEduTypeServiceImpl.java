package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrEduType;
import cn.jeefast.modules.platform.dao.IrEduTypeDao;
import cn.jeefast.modules.platform.service.IrEduTypeService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrEduTypeServiceImpl extends ServiceImpl<IrEduTypeDao, IrEduType> implements IrEduTypeService {
    @Resource
    private IrEduTypeDao irEduTypeDao;
    @Override
    public Page<IrEduType> selectByPage(Page<IrEduType> p, Map<String, Object> params) {
        List<IrEduType> list=irEduTypeDao.selectByPage(p,params);
        return p.setRecords(list);
    }
}
