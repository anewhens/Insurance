package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrEducation;
import cn.jeefast.modules.platform.dao.IrEducationDao;
import cn.jeefast.modules.platform.service.IrEducationService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrEducationServiceImpl extends ServiceImpl<IrEducationDao, IrEducation> implements IrEducationService {
	
}
