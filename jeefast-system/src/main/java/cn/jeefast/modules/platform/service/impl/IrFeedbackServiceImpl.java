package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrFeedback;
import cn.jeefast.modules.platform.dao.IrFeedbackDao;
import cn.jeefast.modules.platform.service.IrFeedbackService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrFeedbackServiceImpl extends ServiceImpl<IrFeedbackDao, IrFeedback> implements IrFeedbackService {

    @Autowired
    private  IrFeedbackDao feedbackDao;

    @Override
    public Page<IrFeedback> selectByPage(Page<IrFeedback> p, Map<String, Object> params) {
        List<IrFeedback> irFeedbacks = feedbackDao.selectByPage(p, params);
        return p.setRecords(irFeedbacks);
    }
}
