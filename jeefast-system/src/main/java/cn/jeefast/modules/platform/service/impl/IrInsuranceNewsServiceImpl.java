package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrInsuranceNews;
import cn.jeefast.common.entity.IrNews;
import cn.jeefast.modules.platform.dao.IrInsuranceNewsDao;
import cn.jeefast.modules.platform.dao.IrNewsDao;
import cn.jeefast.modules.platform.service.IrInsuranceNewsService;
import cn.jeefast.modules.platform.service.IrNewsService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrInsuranceNewsServiceImpl extends ServiceImpl<IrInsuranceNewsDao, IrInsuranceNews> implements IrInsuranceNewsService {
    @Resource
    private IrInsuranceNewsDao irInsuranceNewsDao;
    @Override
    public Page<IrInsuranceNews> selectByPage(Page<IrInsuranceNews> p, Map<String, Object> params) {
        List<IrInsuranceNews> list=irInsuranceNewsDao.selectByPage(p,params);
        return p.setRecords(list);
    }
}
