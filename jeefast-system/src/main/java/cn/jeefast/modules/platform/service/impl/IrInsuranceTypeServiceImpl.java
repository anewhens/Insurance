package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrInsuranceType;
import cn.jeefast.modules.platform.dao.IrInsuranceTypeDao;
import cn.jeefast.modules.platform.service.IrInsuranceTypeService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrInsuranceTypeServiceImpl extends ServiceImpl<IrInsuranceTypeDao, IrInsuranceType> implements IrInsuranceTypeService {

    @Autowired
    private IrInsuranceTypeDao irInsuranceTypeDao;

    @Override
    public List<IrInsuranceType> typeListByPaye(Integer page, Integer limit) {
        return irInsuranceTypeDao.typeListByPaye(page,limit);
    }

    @Override
    public Page<IrInsuranceType> selectByPage(Page<IrInsuranceType> p, Map<String, Object> params) {
        List<IrInsuranceType> list=irInsuranceTypeDao.selectByPage(p,params);
        return p.setRecords(list);
    }
}
