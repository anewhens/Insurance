package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrLow;
import cn.jeefast.modules.platform.dao.IrLowDao;
import cn.jeefast.modules.platform.service.IrLowService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrLowServiceImpl extends ServiceImpl<IrLowDao, IrLow> implements IrLowService {
    @Resource
    private IrLowDao irLowDao;
    @Override
    public Page<IrLow> selectByPage(Page<IrLow> p, Map<String, Object> params) {
        List<IrLow> list=irLowDao.selectByPage(p,params);
        return p.setRecords(list);
    }
}
