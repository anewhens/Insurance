package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrMsgLunbo;
import cn.jeefast.modules.platform.dao.IrMsgLunboDao;
import cn.jeefast.modules.platform.service.IrMsgLunboService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页消息轮播 服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrMsgLunboServiceImpl extends ServiceImpl<IrMsgLunboDao, IrMsgLunbo> implements IrMsgLunboService {
    @Resource
    private IrMsgLunboDao irMsgLunboDao;
    @Override
    public Page<IrMsgLunbo> selectByPage(Page<IrMsgLunbo> p, Map<String, Object> params) {
        List<IrMsgLunbo> list=irMsgLunboDao.selectByPage(p,params);
        return p.setRecords(list);
    }
}
