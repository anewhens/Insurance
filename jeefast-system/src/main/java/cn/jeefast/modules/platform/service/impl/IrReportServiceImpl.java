package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrReport;
import cn.jeefast.modules.platform.dao.IrReportDao;
import cn.jeefast.modules.platform.dao.IrReportUploadDao;
import cn.jeefast.modules.platform.service.IrReportService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrReportServiceImpl extends ServiceImpl<IrReportDao, IrReport> implements IrReportService {
    @Resource
    private IrReportDao irReportDao;
    @Resource
    private IrReportUploadDao irReportUploadDao;
    @Override
    public Page<IrReport> selectByPage(Page<IrReport> p, Map<String, Object> params) {
        List<IrReport> list=irReportDao.selectByPage(p,params);
        for (IrReport irReport:list
             ) {
           //添加已上传的个数
            Integer number=irReportUploadDao.getNumber(irReport.getIrrId());
            irReport.setNumber(number+"");
        }
        return p.setRecords(list);
    }
}
