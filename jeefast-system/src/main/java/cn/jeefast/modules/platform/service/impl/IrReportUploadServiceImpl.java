package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrReportUpload;
import cn.jeefast.modules.platform.dao.IrReportUploadDao;
import cn.jeefast.modules.platform.service.IrReportUploadService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 模板上传记录表 服务实现类
 * </p>
 *
 * @author lzl
 * @since 2020-03-12
 */
@Service
public class IrReportUploadServiceImpl extends ServiceImpl<IrReportUploadDao, IrReportUpload> implements IrReportUploadService {
	
}
