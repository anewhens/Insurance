package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrWheel;
import cn.jeefast.common.utils.R;
import cn.jeefast.modules.platform.dao.IrWheelDao;
import cn.jeefast.modules.platform.service.IrWheelService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class IrWheelServiceImpl extends ServiceImpl<IrWheelDao, IrWheel> implements IrWheelService {

    @Autowired
    private IrWheelDao wheelDao;

    @Override
    public R selectWheelList() {
        List<Map> result=new ArrayList<>();
        Map map1=new HashMap();
        map1.put("title","首页轮播图");
        Integer number1=wheelDao.getNumber("1");
        map1.put("number",number1);
        map1.put("typeId","1");
        Map map2=new HashMap();
        Integer number2=wheelDao.getNumber("2");
        map2.put("number",number2);
        map2.put("typeId","2");
        map2.put("title","监管政策法规轮播图");
        Map map3=new HashMap();
        Integer number3=wheelDao.getNumber("3");
        map3.put("number",number3);
        map3.put("typeId","3");
        map3.put("title","保险超级课堂轮播图");
        Map map4=new HashMap();
        Integer number4=wheelDao.getNumber("4");
        map4.put("number",number4);
        map4.put("typeId","4");
        map4.put("title","保险公司热销产品轮播图");
        Map map5=new HashMap();
        Integer number5=wheelDao.getNumber("5");
        map5.put("number",number5);
        map5.put("typeId","5");
        map5.put("title","金融保险资讯轮播图");
        result.add(map1);
        result.add(map2);
        result.add(map3);
        result.add(map4);
        result.add(map5);
        return R.ok("查询成功").put("data",result);
    }

    @Override
    public R selectDetailList(Integer typeId, Integer page, Integer limit) {
        List<IrWheel> irWheels = wheelDao.selectDetailList(typeId, page, limit);
        return R.ok("查询成功").put("data",irWheels);
    }

    @Override
    public Page<IrWheel> selectByPage(Page<IrWheel> p, Map<String, Object> params) {
        List<IrWheel> irWheels = wheelDao.selectByPage(p, params);
        return p.setRecords(irWheels);
    }
}
