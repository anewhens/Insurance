package cn.jeefast.modules.platform.service.impl;

import cn.jeefast.common.entity.IrBank;
import cn.jeefast.common.entity.IrNews;
import cn.jeefast.model.TbUserVO;
import cn.jeefast.modules.platform.dao.IrBankDao;
import cn.jeefast.modules.platform.dao.IrNewsDao;
import cn.jeefast.modules.platform.dao.TbUserDao;
import cn.jeefast.modules.platform.entity.TbUser;
import cn.jeefast.modules.platform.service.IrNewsService;
import cn.jeefast.modules.platform.service.TbUserService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author theodo
 * @since 2020-02-27
 */
@Service
public class TbUserServiceImpl extends ServiceImpl<TbUserDao, TbUser> implements TbUserService {
    @Resource
    private TbUserDao tbUserDao;
    @Resource
    private IrBankDao irBankDao;
    @Override
    public Page<TbUser> selectByPage(Page<TbUser> p, Map<String, Object> params,List<Long> l) {
        List<TbUser> list=tbUserDao.selectByPage(p,params,l);
        for (TbUser tbUser:list
             ) {
            IrBank irBank = irBankDao.selectById(tbUser.getProvinceBank());
            IrBank bank = irBankDao.selectById(tbUser.getCityBank());
            tbUser.setProvinceBankName(irBank==null?"":irBank.getBankName());
            tbUser.setCityBankName(bank==null?"":bank.getBankName());

        }
        return p.setRecords(list);
    }

    @Override
    public List<TbUserVO> getUserList(List<Long> userIdList) {
        List<TbUserVO> list=new ArrayList<>();
        for (Long id:userIdList
             ) {
            TbUserVO tbUserVO=new TbUserVO();
            TbUser tbUser=tbUserDao.selectById(id);
            tbUserVO.setUserId(tbUser==null?"":tbUser.getUserId()+"");
            tbUserVO.setUserName(tbUser==null?"":tbUser.getUsername());
            tbUserVO.setUserMobile(tbUser==null?"":tbUser.getMobile());
            tbUserVO.setSubBank(tbUser==null?"":tbUser.getSubBank());
            tbUserVO.setLocation(tbUser==null?"":tbUser.getLocation());
            IrBank irBank = irBankDao.selectById(tbUser.getProvinceBank());
            IrBank bank = irBankDao.selectById(tbUser.getCityBank());
            tbUserVO.setProvinceBankName(irBank==null?"":irBank.getBankName());
            tbUserVO.setCityBankName(bank==null?"":bank.getBankName());
            list.add(tbUserVO);
        }
        return list;
    }

    @Override
    public List<TbUser> selectByExport(List<Long> list) {
        List<TbUser> tbUsers = tbUserDao.selectByExport(list);
        return tbUsers;
    }
}
