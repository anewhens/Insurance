package cn.jeefast.modules.upload.service.impl;

import cn.jeefast.common.utils.R;
import cn.jeefast.modules.upload.FileUtil;
import cn.jeefast.modules.upload.service.FileUploadService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * FileUploadServiceImpl
 *
 * @author WY
 * @date 2018-08-29 0029
 */
@Service
public class FileUploadServiceImpl implements FileUploadService {

    @Override
    public String oneUpload(MultipartFile file, String folder) {
        return FileUtil.oneFile(file, folder);
    }

    @Override
    public boolean delete(List<String> delPicList, String folder) {
        boolean b = false;
        for (int i = 0; i < delPicList.size(); i++) {
            ///freight/upload/153991954275560.jpg
            String filename = StringUtils
                    .substring(delPicList.get(i), delPicList.get(i).lastIndexOf("/") + 1);
            b = cn.jeefast.modules.upload.FileUtil.deleteFile(filename, folder);
        }
        return b;
    }


    @Override
    public R oneUpload2(MultipartFile file, String folder) {
        String path = cn.jeefast.modules.upload.FileUtil.oneFile(file, folder);
        String smallPath = cn.jeefast.modules.upload.FileUtil.saveSmallPicture(file, folder);
        return R.ok().put("path", path).put("smallPath", smallPath);
    }

  @Override
  public R oneUpload3(MultipartFile file, String folder) {
    return FileUtil.oneFile2(file, folder);
  }
}
