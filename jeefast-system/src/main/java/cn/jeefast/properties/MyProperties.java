package cn.jeefast.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource({"classpath:config/pay.properties"})
@ConfigurationProperties(prefix = "pay")
public class MyProperties {
    private AlipayConfig alipayconfig = new AlipayConfig();
    private WxpayConfig wxpayconfig = new WxpayConfig();

    public WxpayConfig getWxpayconfig() {
        return this.wxpayconfig;
    }

    public void setWxpayconfig(WxpayConfig wxpayconfig) {
        this.wxpayconfig = wxpayconfig;
    }

    public AlipayConfig getAlipayconfig() {
        return this.alipayconfig;
    }

    public void setAlipayconfig(AlipayConfig alipayconfig) {
        this.alipayconfig = alipayconfig;
    }

}
