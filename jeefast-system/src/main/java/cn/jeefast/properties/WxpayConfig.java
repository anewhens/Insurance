package cn.jeefast.properties;

public class WxpayConfig {
    private String appId; //APP指定唯一标识
    private String mchId; //商户号
    private String notifyUrl; //通知地址，回调地址
    private String notifyUr;//？
    private String key;//秘钥
    private String payUrl; //支付路径
    private String refundUrl; //退款路径
    private String withdrawUrl;//提款路径
    private String certPath;//证书路径

    public String getWithdrawUrl() {
        return withdrawUrl;
    }

    public void setWithdrawUrl(String withdrawUrl) {
        this.withdrawUrl = withdrawUrl;
    }

    public String getAppId() {
        return this.appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getMchId() {
        return this.mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getNotifyUrl() {
        return this.notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPayUrl() {
        return this.payUrl;
    }

    public void setPayUrl(String payUrl) {
        this.payUrl = payUrl;
    }

    public String getRefundUrl() {
        return this.refundUrl;
    }

    public void setRefundUrl(String refundUrl) {
        this.refundUrl = refundUrl;
    }

    public String getCertPath() {
        return this.certPath;
    }

    public void setCertPath(String certPath) {
        this.certPath = certPath;
    }

    public String getNotifyUr() {
        return notifyUr;
    }

    public void setNotifyUr(String notifyUr) {
        this.notifyUr = notifyUr;
    }
}
