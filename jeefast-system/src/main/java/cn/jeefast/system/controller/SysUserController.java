package cn.jeefast.system.controller;

import cn.jeefast.common.utils.MapObjectUtils;
import cn.jeefast.model.ProgressVO;
import cn.jeefast.model.SysUserVO;
import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.plugins.Page;

import cn.jeefast.common.annotation.Log;
import cn.jeefast.common.base.BaseController;
import cn.jeefast.common.excel.ExcelTemplate;
import cn.jeefast.common.excel.ExcelUtil;
import cn.jeefast.common.utils.DateUtils;
import cn.jeefast.common.utils.Query;
import cn.jeefast.common.utils.R;
import cn.jeefast.common.validator.Assert;
import cn.jeefast.common.validator.ValidatorUtils;
import cn.jeefast.common.validator.group.AddGroup;
import cn.jeefast.common.validator.group.UpdateGroup;
import cn.jeefast.system.entity.SysUser;
import cn.jeefast.system.service.SysUserRoleService;
import cn.jeefast.system.service.SysUserService;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.*;

import javax.servlet.http.HttpServletResponse;

/**
 * 系统用户
 * 
 * @author theodo
 * @email 36780272@qq.com
 * @date 2016年10月31日 上午10:40:10
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends BaseController {
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysUserRoleService sysUserRoleService;
	
	/**
	 * 所有用户列表
	 */
	@RequestMapping("/list")
	@RequiresPermissions("sys:user:list")
	public R list(@RequestBody Map<String, Object> params){
		//查询列表数据
		Query query = new Query(params);
		Page<SysUser> pageUtil = new Page<SysUser>(query.getPage(), query.getLimit());
		Page<SysUser> page = sysUserService.queryPageList(pageUtil,query);
		return R.ok().put("page", page);
	}
	
	/**
	 * 获取登录的用户信息
	 */
	@RequestMapping("/info")
	public R info(){
		return R.ok().put("user", getUser());
	}
	
	/**
	 * 修改登录用户密码
	 */
	@Log("修改密码")
	@RequestMapping("/password")
	public R password(String password, String newPassword){
		Assert.isBlank(newPassword, "新密码不为能空");

		//sha256加密
		password = new Sha256Hash(password, getUser().getSalt()).toHex();
		//sha256加密
		newPassword = new Sha256Hash(newPassword, getUser().getSalt()).toHex();
		
		SysUser user = new SysUser();
		user.setUserId(getUserId());
		user.setPassword(newPassword);
		//更新密码
		boolean bFlag = sysUserService.updateById(user);
		if(!bFlag){
			return R.error("原密码不正确");
		}
		
		return R.ok();
	}
	
	/**
	 * 用户信息
	 */
	@RequestMapping("/info/{userId}")
	@RequiresPermissions("sys:user:info")
	public R info(@PathVariable("userId") Long userId){
		SysUser user = sysUserService.selectById(userId);
		
		//获取用户所属的角色列表
		List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
		user.setRoleIdList(roleIdList);
		
		return R.ok().put("user", user);
	}
	
	/**
	 * 保存用户
	 */
	@Log("保存用户")
	@RequestMapping("/save")
	@RequiresPermissions("sys:user:save")
	public R save(@RequestBody SysUser user){
		ValidatorUtils.validateEntity(user, AddGroup.class);
		
		user.setCreateTime(new Date());
		user.setCreateUserId(getUserId());
		List<Long> ids = new ArrayList<>();
		ids.add(user.getRoleId());
		user.setRoleIdList(ids);
		sysUserService.save(user);
		
		return R.ok();
	}
	
	/**
	 * 修改用户
	 */
	@Log("修改用户")
	@RequestMapping("/update")
	@RequiresPermissions("sys:user:update")
	public R update(@RequestBody SysUser user){
//		ValidatorUtils.validateEntity(user, UpdateGroup.class);
//		user.setCreateUserId(getUserId());
		sysUserService.update(user);
		
		return R.ok();
	}
	
	/**
	 * 删除用户
	 */
	@Log("删除用户")
	@RequestMapping("/delete")
	@RequiresPermissions("sys:user:delete")
	public R delete(@RequestBody Long[] userIds){
		if(ArrayUtils.contains(userIds, 1L)){
			return R.error("系统管理员不能删除");
		}
		
		if(ArrayUtils.contains(userIds, getUserId())){
			return R.error("当前用户不能删除");
		}
		
		sysUserService.deleteBatch(userIds);
		
		return R.ok();
	}
	
	/**
	 * 导出用户
	 * @throws IOException 
	 */
	@Log("导出用户")
	@RequestMapping("/downloadExcel")
	@RequiresPermissions("sys:user:exportExcel")
	public void exportExcel(@RequestParam(value = "userIdList") List<Long> userIdList,HttpServletResponse response) throws IOException{
		try {
			List<SysUserVO> userList = sysUserService.getUserList(userIdList);
			ExcelUtil.setInfoAndExport(response, "后台用户表", "后台用户表.xls",
					"sysuser-template.xls", "cn.jeefast.model.SysUserVO", userList);
		} catch (Exception e) {
			logger.error("【导出失败，原因 --> {}】", e.toString());
		}
	}
	/**
	 * @param map
	 * @return R
	 * @Title: 修改状态
	 * @author: lzl
	 * @Description:
	 */
	@RequestMapping("/doUpdateStatus")
	public R doUpdateStatus(@RequestBody Map map) {
		String userId = MapObjectUtils.getObject(map.get("userId"));
		String status = MapObjectUtils.getObject(map.get("status"));
		SysUser sysUser = sysUserService.selectById(userId);
		if (status.equals("1")) { //停用
			sysUser.setStatus(0);
			sysUserService.updateById(sysUser);
			return R.ok("停用成功");
		} else if (status.equals("2")) { // 恢复
			sysUser.setStatus(1);
			sysUserService.updateById(sysUser);
			return R.ok("恢复成功");
		}
		return R.error("数据错误");
	}
}
