package cn.jeefast.system.controller;

import cn.jeefast.common.utils.PoiUtil;
import cn.jeefast.common.utils.R;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@CrossOrigin
public class TestController {

    @RequestMapping("/ir_additional")
    @ResponseBody
    public R upload(MultipartFile file) throws Exception{
        List<String[]> excelData = PoiUtil.getExcelData(file);
//        for (String[] e:excelData) {
//            System.out.println("------------------");
//            for (String n:e) {
//                System.out.println("+++++++"+n);
//            }
//        }

        /**
         *  开始分解信息
         */

        //获取数据纵列数
        int length = excelData.get(0).length-1;
        System.out.println("+++列数"+length);

        //获取第一行，保险期间
        String[] timeStrs = excelData.get(0);
        List<String> time = new ArrayList<>();
        for (int i = 1; i < timeStrs.length; i++) {
            time.add(timeStrs[i]);
        }

        //获取第二行，缴费期间
        String[] payStrs = excelData.get(1);
        List<String> pay = new ArrayList<>();
        for (int i = 1; i <payStrs.length; i++) {
            pay.add(payStrs[i]);
        }
        //获取第二行，缴费期间
        String[] sexStrs = excelData.get(2);
        List<String> sex = new ArrayList<>();
        for (int i = 1; i <sexStrs.length; i++) {
            sex.add(sexStrs[i]);
        }


        System.out.println("++++产生实体数据++++");
        //循环取数据
        for (int i = 3; i < excelData.size(); i++) {
            //获取数据行第一行数据
            String[] strings = excelData.get(i);
            //length是金额列，+1列年龄列。
            for (int j = 0; j < length; j++) {
                Map map = new HashMap();
                //插入年龄
                map.put("age",strings[0]);
                //插入保险期间
                map.put("time",time.get(j));
                //插入缴费期间
                map.put("pay",pay.get(j));
                //插入性别
                map.put("sex",sex.get(j));
                //插入金额
                map.put("money",strings[j+1]);
                System.out.println("   "+map);
            }
            System.out.println("+++++++");
        }

        System.out.println("--------打印保险期间--------");
        System.out.println(time);
        System.out.println("--------打印缴费期间--------");
        System.out.println(pay);
        System.out.println("--------打印性别--------");
        System.out.println(sex);
        return R.ok();
    }
}
