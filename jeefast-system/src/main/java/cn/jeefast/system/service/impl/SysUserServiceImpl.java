package cn.jeefast.system.service.impl;

import cn.jeefast.common.entity.IrBank;
import cn.jeefast.model.SysUserVO;
import cn.jeefast.modules.platform.service.IrBankService;
import cn.jeefast.system.dao.SysRoleDao;
import cn.jeefast.system.entity.SysRole;
import cn.jeefast.system.entity.SysRoleDept;
import cn.jeefast.util.TimeUtils;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import cn.jeefast.common.annotation.DataFilter;
import cn.jeefast.system.dao.SysUserDao;
import cn.jeefast.system.entity.SysUser;
import cn.jeefast.system.service.SysUserRoleService;
import cn.jeefast.system.service.SysUserService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 系统用户 服务实现类
 * </p>
 *
 * @author theodo
 * @since 2017-10-28
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserDao, SysUser> implements SysUserService {
	@Autowired
	private SysUserDao sysUserDao;
	@Autowired
	private SysRoleDao sysRoleDao;
	@Autowired
	private SysUserRoleService sysUserRoleService;
	@Autowired
	private IrBankService bankService;
	
	@Override
	//@DataFilter(tableAlias = "u", user = false)
	public Page<SysUser> queryPageList(Page<SysUser> page, Map<String, Object> map) {
		page.setRecords(sysUserDao.queryPageList(page, map));
		return page;
	}
	
	@Override
	@DataFilter(tableAlias = "u", user = false)
	public List<SysUser> queryList(Map<String, Object> map){
		return sysUserDao.queryList(map);
	}
	
	@Override
	public List<String> queryAllPerms(Long userId) {
		return sysUserDao.queryAllPerms(userId);
	}

	@Override
	public List<Long> queryAllMenuId(Long userId) {
		return sysUserDao.queryAllMenuId(userId);
	}

	@Override
	public SysUser queryByUserName(String username) {
		return sysUserDao.queryByUserName(username);
	}
	
	@Override
	@Transactional
	public void save(SysUser user) {
		user.setCreateTime(new Date());
		//sha256加密
		String salt = RandomStringUtils.randomAlphanumeric(20);
		user.setPassword(new Sha256Hash(user.getPassword(), salt).toHex());
		user.setSalt(salt);
		sysUserDao.insert(user);
		
		//保存用户与角色关系
		sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
	}

	@Override
	@Transactional
	public void update(SysUser user) {
		if(StringUtils.isBlank(user.getPassword())){
			user.setPassword(null);
		}else{
			user.setPassword(new Sha256Hash(user.getPassword(), user.getSalt()).toHex());
		}
		sysUserDao.updateById(user);
		List<Long> list=new ArrayList<>();
		list.add(user.getRoleId());
		//保存用户与角色关系
		sysUserRoleService.saveOrUpdate(user.getUserId(), list);
	}

	@Override
	@Transactional
	public void deleteBatch(Long[] userId) {
		sysUserDao.deleteBatch(userId);
	}

	@Override
	public SysUser queryByMobile(String mobile) {
		return sysUserDao.queryByMobile(mobile);
	}

	@Override
	public List<SysUserVO> getUserList(List<Long> userIdList) {
		List<SysUserVO> result=new ArrayList<>();
		for (Long id:userIdList
			 ) {
			SysUserVO sysUserVO=new SysUserVO();
			sysUserVO.setUserId(id+"");
            SysUser sysUser=sysUserDao.selectById(id);
            sysUserVO.setUserName(sysUser==null?"":sysUser.getUsername());
            sysUserVO.setUserMobile(sysUser==null?"":sysUser.getMobile());
            sysUserVO.setCreationTime(sysUser==null?"": TimeUtils.getDateTime(sysUser.getCreateTime()));
            //添加用户的角色
			String roleName="";
			List<Long> list = sysUserRoleService.queryRoleIdList(id);
			for (Long roleId:list
				 ) {
				SysRole sysRole = sysRoleDao.selectById(roleId);
				roleName+=(sysRole==null?"":sysRole.getRoleName()+" ");
			}
			sysUserVO.setRoleName(roleName);
			result.add(sysUserVO);
		}
		return result;
	}

	@Override
	public List<Long> getPower(SysUser user) {
		List<Long> ids = new ArrayList<>();
		//获取用户最高权限
		if(user.getCity()==null){
			if(user.getProvince()!=null){
				//最高权限为省行，查询省行及其市行
				EntityWrapper ew = new EntityWrapper();
				ew.eq("back_parent",user.getProvince());
				List<IrBank> list = bankService.selectList(ew);
				for (IrBank bank:list) {
					ids.add(bank.getBankId());
				}
				ids.add(user.getProvince());
			}else{
				return null;
			}
		}else{
			ids.add(user.getCity());
		}

		return ids;
	}
}
