/**
 * Copyright ? 2018河北优枫网络科技有限公司. All rights reserved.
 *
 * @Title: ListUtil.java
 * @Prject:
 * @Package: package com.jy.rest.util
 * @Description: TODO
 * @author: lzl
 * @date: 17:57 2019/11/15 0015
 * @version: V1.0
 */
package cn.jeefast.util;

import java.util.ArrayList;
import java.util.List;

public class ListUtil {
    /*
     * List分割
     */
    public static List<List<String>> groupList(List<String> list) {
        List<List<String>> listGroup = new ArrayList<List<String>>();
        if (list == null || list.size() == 0) {
            return listGroup;
        }
        int listSize = list.size();
        //子集合的长度
        int toIndex = 1000;
        for (int i = 0; i < list.size(); i += 1000) {
            if (i + 1000 > listSize) {
                toIndex = listSize - i;
            }
            List<String> newList = list.subList(i, i + toIndex);
            listGroup.add(newList);
        }
        return listGroup;
    }
}