/**
 * Copyright © 2018河北优枫网络科技有限公司. All rights reserved.
 *
 * @Title: StringMethods.java
 * @Prject:
 * @Package: package cn.jeefast.util
 * @Description: TODO
 * @author: lzl
 * @date: 13:17 2019/5/23
 * @version: V1.0
 */
package cn.jeefast.util;

import java.util.ArrayList;
import java.util.List;

public class StringMethods {
    /**
     * @param str
     * @return List<String>
     * @Title: 获取图片的集合
     * @author: lzl
     * @Description:
     */
    public static List<String> getListByString(String str) {
        List<String> list = new ArrayList<>();
        if (str == null || str.trim().length() == 0) {
            return list;
        }
        String[] arr = str.split(",");
        for (String s : arr
        ) {
            if (s != null && s.trim().length() > 0) {
                list.add(s);
            }
        }
        return list;
    }
}
