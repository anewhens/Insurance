package cn.jeefast.util;


import org.apache.commons.lang.StringUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TimeUtils {

    public static String time() {
        String time;
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy" + "年" + "MM" + "月" + "dd" + "日");
        time = sdf.format(date);
        return time;
    }

    public static String getMonthName() {
        String time;
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy" + "年" + "MM" + "月");
        time = sdf.format(date);
        return time;
    }

    public static String checktime() {
        String time;
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy" + "-" + "MM" + "-" + "dd");
        time = sdf.format(date);
        return time;
    }

    public static String getBirthTime(Date date) {
        String time;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy" + "-" + "MM" + "-" + "dd");
        time = sdf.format(date);
        return time;
    }

    public static String getDateTime(Date date) {
        String time;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy" + "年" + "MM" + "月" + "dd" + "日");
        time = sdf.format(date);
        return time;
    }

    public static String alltime() {
        String time;
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy" + "年" + "MM" + "月" + "dd" + "日" + " HH" + "时" + "mm" + "分");
        time = sdf.format(date);
        return time;
    }

    public static String alltime2() {
        String time;
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy" + "-" + "MM" + "-" + "dd" + " " + "HH" + ":" + "mm");
        time = sdf.format(date);
        return time;
    }

    //得到当前日期的年月日
    public static Date getDate() throws ParseException {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateNowStr = simpleDateFormat.format(date);
        Date today = simpleDateFormat.parse(dateNowStr);
        return today;
    }

    public static String emptytime() {
        return "2000" + "年" + "01" + "月" + "01" + "日" + " 00" + "时" + "00" + "分";
    }

    //将当前某种格式日期转换成Timestamp格式
    public static Timestamp nowTime() {
        String nowTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()).toString();
        return Timestamp.valueOf(nowTime);
    }

    //将string转换成data
    public static Date getByString(String time) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分");
        return sdf.parse(time);
    }


    //将string转换成data
    public static Date getByString2(String time) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.parse(time);
    }

    //判断当前时间是否在时间范围内
    public static Boolean checkIsInRange(Date beginTime, Date endTime) throws ParseException {
        if (beginTime == null || endTime == null) {
            return false;
        }
        //得到当前日期的年月日
        long now = getDate().getTime();
        //如果在此日期内，则返回true
        if (now >= beginTime.getTime() && now <= endTime.getTime()) {
            return true;
        }
        return false;
    }

    //将string转换成data
    public static Date getByString3(String time) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.parse(time);
    }

    //将string转换成localdatetime
    public static LocalDateTime getByString4(String time) throws ParseException {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(time, df);
    }
    //将string转换成localdatetime
    public static LocalDate getByString5(String time) throws ParseException {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(time, df);
    }
    //将string转换成localtime
    public static LocalTime getByString6(String time) throws ParseException {
        if(StringUtils.isBlank(time)){
            return null;
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern("HH:mm");
        return LocalTime.parse(time, df);
    }
    //将string转换成localdatetime
    public static LocalDate getByString7(String time) throws ParseException {
        if(StringUtils.isBlank(time)){
            return null;
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(time, df);
    }
    //将string转换成localdatetime
    public static LocalDateTime getByString8(String time) throws ParseException {
        if(StringUtils.isBlank(time)){
            return null;
        }
        if(time.contains(".")){
            time=time.substring(0,time.indexOf("."));
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(time, df);
    }
    //将string转换成localdatetime
    public static LocalDate getByString9(String time) throws ParseException {
        if(StringUtils.isBlank(time)){
            return null;
        }
        if(time.length()==4){
           time+="-01-01";
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(time, df);
    }

    //得到7天前的时间
    public static String oneMonthTime() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy" + "年" + "MM" + "月" + "dd" + "日" + " HH" + "时" + "mm" + "分");
        Calendar c = Calendar.getInstance();
        //过去七天
        c.setTime(new Date());
        c.add(Calendar.DATE, -30);
        Date d = c.getTime();
        String day = format.format(d);
        return day;
    }

    public static List<Date> findDates(Date dBegin, Date dEnd) {
        List lDate = new ArrayList();
        lDate.add(dBegin);
        Calendar calBegin = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calBegin.setTime(dBegin);
        Calendar calEnd = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calEnd.setTime(dEnd);
        // 测试此日期是否在指定日期之后
        while (dEnd.after(calBegin.getTime())) {
            // 根据日历的规则，为给定的日历字段添加或减去指定的时间量
            calBegin.add(Calendar.DAY_OF_MONTH, 1);
            lDate.add(calBegin.getTime());
        }
        return lDate;
    }

    public static List<String> getEachDays(String beginTime, String endTime) throws ParseException {
        List list = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日");
        //将string时间转换成date类型
        Date beginDate = simpleDateFormat.parse(beginTime);
        Date endDate = simpleDateFormat.parse(endTime);
        //将日期添加到日历中
        Calendar beginCalendar = Calendar.getInstance();
        beginCalendar.setTime(beginDate);
        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(endDate);
        list.add(simpleDateFormat.format(beginDate));
        //判断该日历日期是否在时间范围内
        while (endDate.after(beginCalendar.getTime())) {
            //如在范围内则日期+1
            beginCalendar.add(Calendar.DAY_OF_MONTH, 1);
            Date nowDate = beginCalendar.getTime();
            //转换成string类型添加到list中
            String days = simpleDateFormat.format(nowDate);
            list.add(days);
        }
        return list;
    }

    /**
     * 获取过去或者未来 任意天内的日期数组
     *
     * @param intervals intervals天内
     * @return 日期数组
     */
    public static ArrayList<String> test(int intervals) {
        ArrayList<String> pastDaysList = new ArrayList<>();
        ArrayList<String> fetureDaysList = new ArrayList<>();
        for (int i = 0; i < intervals; i++) {
            pastDaysList.add(getPastDate(i));
            fetureDaysList.add(getFetureDate(i));
        }
        return pastDaysList;
    }

    /**
     * 获取过去第几天的日期
     *
     * @param past
     * @return
     */
    public static String getPastDate(int past) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
        Date today = calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String result = format.format(today);
        return result;
    }

    /**
     * 获取过去几天的日期数组
     *
     * @param past
     * @return
     */
    public static List<String> getPastDateList(int past) {
        Calendar calendar = Calendar.getInstance();
        List<String> list = new ArrayList<>();
        //正序输出日期
        int day = calendar.get(Calendar.DAY_OF_YEAR);
        for (int i = past - 1; i >= 0; i--) {
            calendar.set(Calendar.DAY_OF_YEAR, day - i);
            Date today = calendar.getTime();
            SimpleDateFormat format = new SimpleDateFormat("MM-dd");
            String result = format.format(today);
            list.add(result);
        }
        return list;
    }

    /**
     * 获取未来 第 past 天的日期
     *
     * @param past
     * @return
     */
    public static String getFetureDate(int past) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + past);
        Date today = calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String result = format.format(today);
        return result;
    }

    //判断轮播图的起始结束时间
    public static Integer checkRotationPicture(Date pictureBeginTime, Date pictureEndTime) {
        if (pictureBeginTime == null || pictureEndTime == null) {
            return null;
        }
        //得到当前日期的年月日
        long now = System.currentTimeMillis();
        //如果在此日期内，则返回true
        if (now >= pictureBeginTime.getTime() && now <= pictureEndTime.getTime()) {
            return 1;//1表示生效中
        }
        if (now < pictureBeginTime.getTime()) {
            return 2;//2表示未生效
        }
        if (now > pictureEndTime.getTime()) {
            return 3;//3表示已失效
        }
        return null;
    }

    //判断黑名单的禁言的起始结束时间
    public static Integer getBlacklistStatus(Date blacklistBeginTime, Date blacklistEndTime) {
        if (blacklistBeginTime == null || blacklistEndTime == null) {
            return null;
        }
        //得到当前日期的年月日
        long now = System.currentTimeMillis();
        //如果在此日期内，则返回true
        if (now >= blacklistEndTime.getTime() && now <= blacklistEndTime.getTime()) {
            return 1;//1禁言中
        }
        if (now > blacklistEndTime.getTime()) {
            return 2;//2表示禁言结束，恢复正常
        }
        if (now < blacklistEndTime.getTime()) {
            return 3;//3未启动禁言
        }
        return null;
    }

    /**
     * @Title: 转换localdatetime日期
     * @author: lzl
     */
    public static String getTime(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return "";
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return df.format(localDateTime);
    }
    /**
     * @Title: 转换localdatetime日期
     * @author: lzl
     */
    public static String getTime3(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return "";
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH:mm:ss");
        return df.format(localDateTime);
    }
    /**
     * @Title: 转换localdatetime日期
     * @author: lzl
     */
    public static String getTime31(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return "";
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm");
        return df.format(localDateTime);
    }
    /**
     * @Title: 转换localdatetime日期
     * @author: lzl
     */
    public static String getTime4(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return "";
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern("MM月dd日 HH:mm:ss");
        return df.format(localDateTime);
    }
    /**
     * @Title: 转换localdatetime日期
     * @author: lzl
     */
    public static String getTime2(LocalDate localDate) {
        if (localDate == null) {
            return "";
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return df.format(localDate);
    }
    /**
     * @Title: 转换localdatetime日期
     * @author: lzl
     */
    public static String getTime5(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return "";
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return df.format(localDateTime);
    }


    /**
     * @Title: 判断两个时间间隔, 返回相应内容
     * @author: lzl
     */
    public static String getBetween(LocalDateTime localDateTime) {
        try {
            if (localDateTime == null) {
                return "";
            }
            Duration duration = Duration.between(localDateTime, LocalDateTime.now());
            //如果是三天前，则显示“年月日”
            if (duration.toDays() > 3) {
                DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                return df.format(localDateTime);
            } else if (duration.toDays() == 3) {
                return "3天前";
            } else if (duration.toDays() == 2) {
                return "2天前";
            } else if (duration.toDays() == 1) {
                return "1天前";
            } else if (duration.toHours() > 15) {
                return "15小时前";
            } else if (duration.toHours() > 2) {
                return "2小时前";
            } else if (duration.toHours() >= 1) {
                return "1小时前";
            }else {
                return "1小时内";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getTime3(LocalDate localDate) {
        if (localDate == null) {
            return "";
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return df.format(localDate);
    }
    /**
     * @Title: 判断时间与现在时间是否在15天之内
     * @author: lzl
     */
    public static Boolean checkIsIn15Days(LocalDateTime localDateTime) {
        Duration duration=Duration.between(localDateTime,LocalDateTime.now());
        if(duration.toDays()>=15){
            return false;
        }
      return true;
    }

    public static String getTime6(Date date) {
        if(date==null){
            return "";
        }
        SimpleDateFormat formatter  = new SimpleDateFormat("yyyy.MM.dd");
        return formatter.format(date);
    }
    /**
     * @Title: 判断时间与现在时间是否在35天之内
     * @author: lzl
     */
    public static Boolean checkIsIn35Days(String localDate) throws ParseException {
        LocalDate date = getByString7(localDate);
        int i=(int)(date.toEpochDay() - LocalDate.now().toEpochDay());
        if(i>35){
            return true;
        }
        return false;
    }
    /**
     * @Title: 判断时间与现在时间是否在35天之内
     * @author: lzl
     */
    public static Boolean checkIsIn35Days2(LocalDateTime localDateTime) throws ParseException {
        int i=(int)(localDateTime.toLocalDate().toEpochDay() - LocalDate.now().toEpochDay());
        if(i>35){
            return true;
        }
        return false;
    }

    public static void main(String[] args) throws ParseException {

    }
}
