package cn.jeefast.util;

import cn.jeefast.common.utils.R;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;

/**
 * @author chenssy
 *
 * 将指定文件/文件夹压缩成zip、rar压缩文件
 */
public class ZipCompressor {

    /**
     * 默认构造函数
     */
    public ZipCompressor() {

    }

    /**
     * @param targetPath 目的压缩文件保存路径
     * @return void
     * @throws Exception
     * @desc 将源文件/文件夹生成指定格式的压缩文件,格式zip
     */
    public void compressedFile(String resourcesPath, String targetPath) throws Exception {
        File resourcesFile = new File(resourcesPath);     //源文件
        File targetFile = new File(targetPath);           //目的文件夹
        //如果目的路径不存在，则新建
        if (!targetFile.exists()) {
            targetFile.mkdirs();
        }
        String targetName = "";
        if (resourcesFile.getName().indexOf(".") != -1) {
            targetName = resourcesFile.getName().substring(0, resourcesFile.getName().indexOf("."))+ System.currentTimeMillis() + ".zip";      //如果是文件则取文件名字
        } else {
            targetName = resourcesFile.getName() +System.currentTimeMillis()+ ".zip";   //目的压缩文件名
        }
        FileOutputStream outputStream = new FileOutputStream(targetPath + "\\" + targetName);
        CheckedOutputStream cos = new CheckedOutputStream(outputStream, new CRC32());
        ZipOutputStream out = new org.apache.tools.zip.ZipOutputStream(cos);
        createCompressedFile(out, resourcesFile, "");
        out.close();

    }

    /**
     * @param targetPath 目的压缩文件保存路径      并下载
     * @return void
     * @throws Exception
     * @desc 将源文件/文件夹生成指定格式的压缩文件,格式zip
     */
    public static void compressedFile2(String resourcesPath, String targetPath, HttpServletResponse response) throws Exception {
        File resourcesFile = new File(resourcesPath);     //源文件
        File targetFile = new File(targetPath);           //目的文件夹
        //如果目的路径不存在，则新建
        if (!targetFile.exists()) {
            targetFile.mkdirs();
        }
        String targetName = "";
        if (resourcesFile.getName().indexOf(".") != -1) {
            targetName = resourcesFile.getName().substring(0, resourcesFile.getName().indexOf("."))+ System.currentTimeMillis() + ".zip";      //如果是文件则取文件名字
        } else {
            targetName = resourcesFile.getName() +System.currentTimeMillis()+ ".zip";   //目的压缩文件名
        }
        FileOutputStream outputStream = new FileOutputStream(targetPath + "\\" + targetName);
        CheckedOutputStream cos = new CheckedOutputStream(outputStream, new CRC32());
        ZipOutputStream out = new org.apache.tools.zip.ZipOutputStream(cos);
        createCompressedFile(out, resourcesFile, "");
        out.close();

        //开始下载
        response.setHeader("content-type", "application/octet-stream");
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=" + targetName);
        byte[] buff = new byte[1024];
        BufferedInputStream bis = null;
        OutputStream os = null;
        try {
            os = response.getOutputStream();
            bis = new BufferedInputStream(new FileInputStream(
                    new File(targetPath + "\\" + targetName)));
            int i = bis.read(buff);

            while (i != -1) {
                os.write(buff, 0, buff.length);
                os.flush();
                i = bis.read(buff);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * @param out  输出流
     * @param file 目标文件
     * @return void
     * @throws Exception
     * @desc 生成压缩文件。
     * 如果是文件夹，则使用递归，进行文件遍历、压缩
     * 如果是文件，直接压缩
     */
    public static void createCompressedFile(ZipOutputStream out, File file, String dir) throws Exception {
        //如果当前的是文件夹，则进行进一步处理
        if (file.isDirectory()) {
            //得到文件列表信息
            File[] files = file.listFiles();
            //将文件夹添加到下一级打包目录
            out.putNextEntry(new ZipEntry(dir + "/"));

            dir = dir.length() == 0 ? "" : dir + "/";

            //循环将文件夹中的文件打包
            for (int i = 0; i < files.length; i++) {
                createCompressedFile(out, files[i], dir + files[i].getName());         //递归处理
            }
        } else {   //当前的是文件，打包处理
            //文件输入流
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
            ZipEntry entry = new ZipEntry(dir + file.getName());
            out.putNextEntry(entry);
            // out.putNextEntry(new ZipEntry(dir));
            //进行写操作
            int j = 0;
            byte[] buffer = new byte[1024];
            while ((j = bis.read(buffer)) > 0) {
                out.write(buffer, 0, j);
            }
            //关闭输入流
            bis.close();
        }
    }

    public static void main(String[] args) throws Exception {
        ZipCompressor zipCompressor = new ZipCompressor();
        zipCompressor.compressedFile("D:\\test\\cn","D:\\test\\cn2");
    }
}
