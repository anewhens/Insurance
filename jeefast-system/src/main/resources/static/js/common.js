//jqGrid的配置信息
$.jgrid.defaults.width = 1000;
$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

//工具集合Tools
window.T = {};

// 获取请求参数
// 使用示例
// location.href = http://localhost/index.html?id=123
// T.p('id') --> 123;
var url = function (name) {
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
  var r = window.location.search.substr(1).match(reg);
  if (r != null) {
    return unescape(r[2]);
  }
  return null;
};
T.p = url;

//请求前缀
var baseURL = "/jeefast/";
//图片前缀
// var imageURL = "http://www.uphonechina.com/";
var imageURL = "http://www.u-banker.net/";
//业务前缀123.57.247.11
var businessURL = "/jeefast/modules/platform/";

//登录token
var token = localStorage.getItem("token");
if (token == 'null') {
  parent.location.href = baseURL + 'login.html';
}

//jquery全局配置
$.ajaxSetup({
  dataType: "json",
  cache: false,
  headers: {
    "token": token
  },
  // xhrFields: {
  //     withCredentials: true
  // },
  complete: function (xhr) {
    //token过期，则跳转到登录页面
    // if (xhr.responseJSON.code == 401) {
    //   parent.location.href = baseURL + 'login.html';
    // }
  }
});

//jqgrid全局配置
$.extend($.jgrid.defaults, {
  ajaxGridOptions: {
    headers: {
      "token": token
    }
  }
});

//权限判断
function hasPermission(permission) {
  if (window.parent.permissions.indexOf(permission) > -1) {
    return true;
  } else {
    return false;
  }
}

//重写alert
window.alert = function (msg, callback) {
  parent.layer.alert(msg, function (index) {
    parent.layer.close(index);
    if (typeof (callback) === "function") {
      callback("ok");
    }
  });
};

//重写confirm式样框
window.confirm = function (msg, callback) {
  parent.layer.confirm(msg, {btn: ['确定', '取消']},
      function () {//确定事件
        if (typeof (callback) === "function") {
          callback("ok");
        }
      });
};

//选择一条记录
function getSelectedRow() {
  var table = layui.table
  var checkStatus = table.checkStatus('idTest')
      , data = checkStatus.data;
  // console.log(JSON.stringify(data));
  if (data.length == 0) {
    alert("请选择一条记录");
    return;
  }

  var selectedIDs = data;
  if (selectedIDs.length > 1) {
    alert("只能选择一条记录");
    return;
  }

  return selectedIDs[0];
}

//选择多条记录
function getSelectedRows() {
  var table = layui.table
  var checkStatus = table.checkStatus('idTest')
      , data = checkStatus.data;
  console.log(JSON.stringify(data));
  if (data.length == 0) {
    alert("请选择一条记录");
    return;
  }

  return data;
}

//格式化日期
function Format(datetime, fmt) {
  if (parseInt(datetime) == datetime) {
    if (datetime.length == 10) {
      datetime = parseInt(datetime) * 1000;
    } else if (datetime.length == 13) {
      datetime = parseInt(datetime);
    }
  }
  datetime = new Date(datetime);
  var o = {
    "M+": datetime.getMonth() + 1,                 //月份
    "d+": datetime.getDate(),                    //日
    "h+": datetime.getHours(),                   //小时
    "m+": datetime.getMinutes(),                 //分
    "s+": datetime.getSeconds(),                 //秒
    "q+": Math.floor((datetime.getMonth() + 3) / 3), //季度
    "S": datetime.getMilliseconds()             //毫秒
  };
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1,
        (datetime.getFullYear() + "").substr(4 - RegExp.$1.length));
  }
  for (var k in o) {
    if (new RegExp("(" + k + ")").test(fmt)) {
      fmt = fmt.replace(RegExp.$1,
          (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(
              ("" + o[k]).length)));
    }
  }
  return fmt;
}