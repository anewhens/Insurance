getQueryString = function(name) {
	try {
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
		var r = decodeURI(window.location.href)
			.split("?")[1]
			.match(reg);
		if (r != null) return unescape(r[2]);
		return null;
	} catch (e) {
		return null;
	}
};
// var baseUrl = 'http://122.14.213.160:7084';
var baseUrl = 'http://www.u-banker.net';
// var baseUrl = 'http://localhost:7084';
// var ImgbaseUrl = 'http://www.uphonechina.com/';
var ImgbaseUrl = 'http://www.u-banker.net/';
// 上传图片接口
// var uploadPic="http://122.14.213.160:7084/jeefast/api/upload";
var uploadPic="http://www.u-banker.net/jeefast/api/upload";
// var uploadPic = "https://jsonplaceholder.typicomode.com/posts/"; //测试
axios.interceptors.request.use((request) => {
	const token = localStorage.getItem('token');
	// console.log("111")
	// console.log(token);
	// const token = "admin";
	if (request.params) {
		request.params.token = token;
	}
	if (request.data) {
		request.data.token = token;
	}
	return request;
});
var axiosPostOption = {
	headers: {
		'Content-Type': 'application/x-www-form-urlencoded'
	},
	transformRequest: [
		function(data) {
			let ret = '';
			for (let it in data) {
				ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&';
			}
			return ret;
		}
	]
}
var mix = {
	data: function() {
		return {
			// 分页
			page: 1, //总页数
			limit: 10, //每页显示个数
			total: 0
		}
	},
	methods: {
		// 分页
		handleCurrentChange: function(currentPage) {
			this.page = currentPage;
			this.getTable()
		},
	},
	mounted: function() {

	}
}
var axiosGetOption = {
	headers: {
		'Content-Type': 'application/json'
	},
}
// 考试批次列表
function timeList(params) {
	return axios.get(baseUrl + '/jeefast/irEduTime/timeList', {params}, axiosGetOption);
}
// 添加考试批次
function addTime(params) {
	return axios.post(baseUrl + '/jeefast/irEduTime/addTime', params, axiosPostOption);
}
//删除考试批次
function deleteTime(params){
	return axios.get(baseUrl + '/jeefast/irEduTime/deleteTime', {params}, axiosGetOption);
}
// 根据考试批次获取试题列表
function examList(params) {
	return axios.get(baseUrl + '/jeefast/irEduExam/examList', {params}, axiosGetOption);
}
//试题详情
function examDetail(params) {
	return axios.get(baseUrl + '/jeefast/irEduExam/examDetail', {params}, axiosGetOption);
}
//删除试题
function deleteExam(params) {
	return axios.get(baseUrl + '/jeefast/irEduExam/deleteExam', {params}, axiosGetOption);
}
// 添加视频创建试题
function addExam(params) {
	return axios.post(baseUrl + '/jeefast/irEduExam/addExam', params, axiosPostOption);
}
// 考试成绩列表
function projexamList(params) {
	return axios.get(baseUrl + '/jeefast/irEduProgress/examList', {params}, axiosGetOption);
}
function eduInfoDetail(params) {
	return axios.get(baseUrl + '/jeefast/irEduProgress/eduInfoDetail', {params}, axiosGetOption);
}
// 省行列表
function provinceBankList(params) {
	return axios.get(baseUrl + '/jeefast/irBank/provinceBankList', {params}, axiosGetOption);
}
// 删除银行
function deleteBank(params) {
	return axios.get(baseUrl + '/jeefast/irBank/delete', {params}, axiosGetOption);
}
// 添加省行
function addProvienceBank(params) {
	return axios.post(baseUrl + '/jeefast/irBank/addProvienceBank', params, axiosPostOption);
}
// 市行列表
function cityBankList(params) {
	return axios.get(baseUrl + '/jeefast/irBank/cityBankList', {params}, axiosGetOption);
}
// 添加市行
function addCityBank(params) {
	return axios.post(baseUrl + '/jeefast/irBank/addCityBank', params, axiosPostOption);
}
// 保险公司列表
function companyList(params) {
	return axios.get(baseUrl + '/jeefast/irCompany/companyList', {params}, axiosGetOption);
}
// 保险公司详情
function companyById(params) {
	return axios.get(baseUrl + '/jeefast/irCompany/compById', {params}, axiosGetOption);
}
// 添加/修改保险公司
function addOrSaveCompany(params) {
	return axios.post(baseUrl + '/jeefast/irCompany/addOrSaveCompany', params, axiosPostOption);
}
// 继续教育分类列表
function eduTypeList(params) {
	return axios.get(baseUrl + '/jeefast/irEduType/eduTypeList', {params}, axiosGetOption);
}
// 添加保费——保额
function addPlan1(params) {
	return axios.post(baseUrl + '/jeefast/iInsuredAmount/addPlan', params, axiosPostOption);
}
//产品计划详情
function planDetail(params) {
	return axios.get(baseUrl + '/jeefast/iInsuredAmount/detail', {params}, axiosGetOption);
}
//产品计划书列表
function planList(params) {
	return axios.get(baseUrl + '/jeefast/iInsuredAmount/list', {params}, axiosGetOption);
}
//删除计划书
function deletePlan(params) {
	return axios.get(baseUrl + '/jeefast/iInsuredAmount/delete', {params}, axiosGetOption);
}
// 保险产品类别列表
function insuranceTypeList(params) {
	return axios.get(baseUrl + '/jeefast/irInsuranceType/insuranceTypeList', {params}, axiosGetOption);
}
// 附加险列表
function additionalList(params) {
	return axios.get(baseUrl + '/jeefast/irAdditional/additional/list', {params}, axiosGetOption);
}

// 删除附加险
function deleteAdditional(params) {
	return axios.get(baseUrl + '/jeefast/irAdditional/additional/delete', {params}, axiosGetOption);
}

// 附加险详情
function additionalDetail(params) {
	return axios.get(baseUrl + '/jeefast/irAdditional/additional/detail', {params}, axiosGetOption);
}

// 添加附加险
function additional(params) {
	return axios.post(baseUrl + '/jeefast/irAdditional/additional/add', params, axiosPostOption);
}

// 添加用户
function addUser(params) {
	return axios.post(baseUrl + '/jeefast/tbUser/addUser', params, axiosPostOption);
}
// 用户详情
function editUser(params) {
	return axios.get(baseUrl + '/jeefast/tbUser/detail', {params}, axiosGetOption);
}
// 删除用户
function deleteUser(params) {
	return axios.get(baseUrl + '/jeefast/tbUser/delete', {params}, axiosGetOption);
}

// 日期转时间戳
function transdate(dateStr) {
	var date = Date.parse(new Date(dateStr));
	var time = date;
	return time.toString()
}