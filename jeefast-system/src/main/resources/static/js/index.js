//生成菜单
var menuItem = Vue.extend({
    name: 'menu-item',
    props: {item: {}, index: 0},
    methods: {
        handleOpen: function (key, keyPath) {
            console.log(key, keyPath);
        },
        handleClose: function (key, keyPath) {
            console.log(key, keyPath);
        },
        goUrl: function (url) {
            console.log('------',url)
            if(url&&url.length>1){
                location.href = '#' + url;
            }else {
                location.href =  "index.html";
            }

            $('.atv').removeClass('atv');
            $(event.currentTarget).addClass('atv');
        },
    },
    template: [
        '<el-row class="tac">' +
        '<el-col :span="12" style="width: 100%;">' +
        '<el-menu default-active="2" class="el-menu-vertical-demo" @open="handleOpen" @close="handleClose">  ' +
        '<el-submenu index="1" v-if="item.type === 0"> ' +
        '<template slot="title" v-if="item.parentId === 0"> ' +
        '<i v-bind:class="item.icon"></i>' +
        '<span>{{item.name}}</span>' +
        '</template>' +
        '<template slot="title" v-else> ' +
        '&nbsp;&nbsp;&nbsp;<i v-bind:class="item.icon"></i>' +
        '<span>{{item.name}}</span>' +
        '</template>' +
        '<menu-item :item="item" :index="index" v-for="(item, index) in item.list" :key="index">' +
        '<el-menu-item index="1-1">{{item.name}}</el-menu-item>' +
        '</menu-item>' +
        '</el-submenu>' +
        '<el-menu-item @click="goUrl(item.url)" index="2" v-if="item.type === 1 && item.parentId != 0" >' +
        '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i v-bind:class="item.icon"></i>' +
        // '<a style="text-decoration:none" slot="title" :href="\'#\'+item.url" data-url="item.url">{{item.name}}</a>' +
        '<a style="text-decoration:none" slot="title">{{item.name}}</a>' +
        '</el-menu-item>' +
        '<el-menu-item @click="goUrl(item.url)" index="2" v-else-if="item.type === 1 && item.parentId === 0" >' +
        '<i v-bind:class="item.icon"></i>' +
        // '<a style="text-decoration:none" slot="title" :href="\'#\'+item.url" data-url="item.url">{{item.name}}</a>' +
        '<a style="text-decoration:none" slot="title">{{item.name}}</a>' +
        '</el-menu-item>' +
        '</el-menu>' +
        '</el-col>' +
        '</el-row>'
    ].join('')
});

//iframe自适应
$(window).on('resize', function () {
    var $content = $('.content');
    $content.height($(this).height() - 120);
    $content.find('iframe').each(function () {
        $(this).height($content.height());
    });
}).resize();

//注册菜单组件
Vue.component('menuItem', menuItem);

var vm = new Vue({
    el: '#jeefastapp',
    data: {
        user: {},
        menuList: {},
        main: "main.html",
        password: '',
        newPassword: '',
        newPassword2: '',
        navTitle: "欢迎页"
    },
    methods: {
        getMenuList: function () {
            $.getJSON(baseURL + "sys/menu/nav", function (r) {
                vm.menuList = r.menuList;
                window.permissions = r.permissions;
            });
        },
        getUser: function () {
            $.getJSON(baseURL + "sys/user/info", function (r) {
                vm.user = r.user;
                if (vm.user == null || vm.user == undefined) {
                    alert('token失效，2秒后跳转到登录页面！');
                    var timeout = setTimeout(function () {
                        window.parent.location.href = baseURL + "login.html";
                        clearTimeout(timeout);
                    }, 2000)
                }
            });
        },
        profile: function () {
            $.getJSON(baseURL + "sys/user/info", function (r) {
                vm.user = r.user;
            });
        },
        updatePassword: function () {
            vm.password = "",
                vm.newPassword = "",
                vm.newPassword2 = ""
            layer.open({
                type: 1,
                title: "修改密码",
                area: ['400px', '330px'],
                scrollbar: false,
                shade: 0,
                shadeClose: false,
                content: jQuery("#passwordLayer"),
                btn: ['修改', '取消'],
                btn1: function (index) {
                    if (vm.password == null || vm.password == "") {
                        vm.$message({
                            message: '请填写原密码',
                            type: 'error'
                        });
                        vm.password = ""
                        vm.$refs.password.focus()
                        return false
                    }
                    if (!/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]{6,20})$/.test(vm.password)) {
                        vm.$message({
                            message: '请填写6-20位数字和字母组合',
                            type: 'error'
                        });
                        vm.password = ""
                        vm.$refs.password.focus()
                        return false
                    }
                    if (vm.newPassword == null || vm.newPassword == "") {
                        vm.$message({
                            message: '请填写新密码',
                            type: 'error'
                        });
                        vm.newPassword = ""
                        vm.$refs.newPassword.focus()
                        return false
                    }
                    if (!/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]{6,20})$/.test(vm.newPassword)) {
                        vm.$message({
                            message: '请填写6-20位数字和字母组合',
                            type: 'error'
                        });
                        vm.newPassword = ""
                        vm.$refs.newPassword.focus()
                        return false
                    }
                    if (vm.newPassword2 == null || vm.newPassword2 == "") {
                        vm.$message({
                            message: '请填写确认密码',
                            type: 'error'
                        });
                        vm.newPassword2 = ""
                        vm.$refs.newPassword2.focus()
                        return false
                    }
                    if (!/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]{6,20})$/.test(vm.newPassword2)) {
                        vm.$message({
                            message: '请填写6-20位数字和字母组合',
                            type: 'error'
                        });
                        vm.newPassword2 = ""
                        vm.$refs.newPassword2.focus()
                        return false
                    }
                    if (vm.newPassword != vm.newPassword2) {
                        vm.$message({
                            message: '新密码和确认密码不相同',
                            type: 'error'
                        });
                        vm.newPassword2 = ""
                        vm.$refs.newPassword2.focus()
                        return false
                    }
                    var data = "password=" + vm.password + "&newPassword="
                        + vm.newPassword;
                    $.ajax({
                        type: "POST",
                        url: baseURL + "sys/user/password",
                        data: data,
                        dataType: "json",
                        success: function (r) {
                            if (r.code == 0) {
                                layer.close(index);
                                vm.$message({
                                    message: '修改密码成功,重新登录',
                                    type: 'success'
                                });
                                var timeout = setTimeout(function () {
                                    window.parent.location.href = baseURL + "login.html";
                                    clearTimeout(timeout);
                                }, 2000)
                            } else {
                                vm.$message({
                                    message: r.msg,
                                    type: 'error'
                                });
                            }
                        }
                    });
                }
            });
        },
        //退出登录
        logout: function () {
            //删除本地token
            localStorage.removeItem("token");
            //跳转到登录页面
            location.href = baseURL + 'login.html';
        }
    },
    created: function () {
        this.getMenuList();
        this.getUser();
    },
    updated: function () {
        //路由
        var router = new Router();
        routerList(router, vm.menuList);
        router.start();
    }
});

function routerList(router, menuList) {
    for (var key in menuList) {
        var menu = menuList[key];
        if (menu.type == 0) {
            routerList(router, menu.list);
        } else if (menu.type == 1) {
            router.add('#' + menu.url, function () {
                var url = window.location.hash;

                //替换iframe的url
                vm.main = url.replace('#', '');

                //导航菜单展开
                $(".submenu li").removeClass("active");
                $(".nav-list li").removeClass("active");
                $("a[href='" + url + "']").parents("li").addClass("active");

                vm.navTitle = $("a[href='" + url + "']").text();
            });
        }
    }
}
