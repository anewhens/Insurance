var vm = new Vue({
    el: '#jeefastapp',
    data: function () {
        return {
            //列表url
            listUrl: 'sys/user/list',
            //删除url
            delUrl: 'sys/user/delete',
            //导出url
            excelExportUrl: 'sys/user/downloadExcel',
            excelExportUrlAll: 'sys/user/downloadExcelAll',
            q: {
                username: '',
                mobile: '',
                roleId:''
            },
            user: {
            },
            showList: true,
            roleList: [],
            entityList: [],
            currentPage: 1,
            pageSize: 10,
            total: 1000,
            provinceList:'',
            provience:null,
            cityList:'',
            changeArr: [],
            title: '',
            folder: 'user',
            rules: {
                username: [{
                    required: true,
                    message: '用户名不能为空'
                }],
                // realname: [{
                //     required: true,
                //     message: '真实姓名不能为空'
                // }],
                password: [{
                    required: true,
                    message: '密码不能为空'
                }],
                mobile: [{
                    required: true,
                    validator: checkPhone,
                    trigger: 'blur'
                }],
                roleId: [{
                    required: true,
                    message: '角色不能为空',
                }]
            }
        }
    },
    methods: {
        query(){
          this.currentPage=1
          this.getEntityList()
        },
        downLoadExcel(){
            var ids = [];
            $.each(vm.changeArr, function (k, v) {
                ids.push(v.userId);
            })
            if (ids.length == 0) {
                vm.vAlert('请选择导出的用户', '错误')
                return false;
            }
            var url = baseURL + vm.excelExportUrl + '?token=' + token + "&userIdList=" + ids;
            window.top.location.href = url;
        },
        //修改状态
        updateStatus(userId, status) {
            var message = ""
            if (status == 1) {
                message = '确定要停用该用户?'
            } else if (status == 2) {
                message = '确定要恢复该用户?'
            }
            this.$confirm(message, '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(function () {
                var data = {};
                data.userId = userId
                data.status = status
                var url = baseURL + "sys/user/doUpdateStatus"
                axios.post(url + '?token=' + token, data)
                    .then(function (res) {
                        if (res.data.code === 0) {
                            vm.$message({
                                message: res.data.msg,
                                type: 'success'
                            });
                            setTimeout(function () {
                                vm.getEntityList();
                            }, 500)
                        } else {
                            vm.$message({
                                message: res.data.msg,
                                type: 'error'
                            });
                        }
                    })
            }).catch(function (error) {
                console.log(error);
            })
        },
        changeType(val) {
            if (val) {
                // this.q.roleId=val.value
                this.$set(this.q, this.q.roleId, val.value)
            }
            this.getEntityList()
        },
        strToList(aroles, astores) {
            //返回结果
            let res = []
            return res;
        },
        listToStr(list) {
            var strs = [];
            $.each(list, function (k, v) {
                strs.push(v.storeName)
            });
            return strs.join(",")
        },
        add: function () {
            vm.showList = false;
            vm.title = "新增";
        },
        update: function (index, row) {
            var userId = row.userId;
            if (userId == null) {
                return;
            }

            vm.showList = false;
            vm.title = "修改";
            vm.getUser(userId);
            //获取角色信息
            this.getRoleList();
        },
        saveOrUpdate: function (user) {
            this.$refs[user].validate((valid) => {
                if (valid) {
                    if (vm.user.userId == null) {
                        if (vm.user.password == '') {
                            vm.vAlert('请输入密码!', '错误')
                            return false;
                        }
                    }
                    // var test= /^[a-zA-Z0-9]+$/
                    // if(!test.tes请输入英文或者数字的用户名t(vm.user.username)){
                    //     vm.vAlert('!', '错误')
                    //     return false;
                    // }
                    var url = vm.user.userId == null ? "sys/user/save"
                        : "sys/user/update";
                    $.ajax({
                        type: "POST",
                        url: baseURL + url,
                        contentType: "application/json",
                        data: JSON.stringify(vm.user),
                        success: function (r) {
                            if (r.code === 0) {
                                alert('操作成功', function () {
                                    vm.reload()
                                });
                            } else {
                                alert(r.msg);
                            }
                        }
                    });
                } else {
                    return false;
                }
            });
        },
        // 省行
        getProvince:function(){
            $.get(baseURL + "irBank/provinceBankList?limit=10000" , function (r) {
                vm.provinceList = r.data;
            });
            // provinceBankList({limit:10000}).then(res=>{
            //     if(res.data.code==0){
            //     this.provinceList=res.data.data
            // }
        // })
        },
        // 市行
        getCity:function(){
            //选项置空
            // vm.user.city = '';
            $.get(baseURL + "irBank/cityBankList?limit=10000&provienceId="+vm.user.province , function (r) {
                vm.cityList = r.data;
            });
        //     cityBankList({limit:10000}).then(res=>{
        //         if(res.data.code==0){
        //         this.cityList=res.data.data
        //     }
        // })
        },
        getUser: function (userId) {
            $.get(baseURL + "sys/user/info/" + userId, function (r) {
                vm.user = r.user;
                if (r.user.roleIdList.length > 0) {
                    vm.user.roleId = r.user.roleIdList[0]
                }
                vm.user.password = null;
            });
        },   //初始化密码
        updatePassword(row) {
            this.$confirm('确定要初始化当前用户密码?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                var args = {}
                args.userId = row.userId
                $.ajax({
                    type: "POST",
                    url: baseURL + "sys/user/doUpdatePassword",
                    data: args,
                    async: false,
                    dataType: "JSON",
                    success: function (data) {
                        if (data.code == 0) {
                            vm.$message({
                                message: data.msg,
                                type: 'success'
                            });
                            setTimeout(function () {
                                vm.addInfos = false;
                                vm.getEntityList()
                            }, 500)
                        } else {
                            vm.$message({
                                message: data.msg,
                                type: 'error'
                            });
                        }
                    },
                    error: function () {
                        vm.$message({
                            message: '系统错误',
                            type: 'error'
                        });
                    }
                })
            }).catch(() => {
                vm.$message({
                    type: 'info',
                    message: '已取消'
                });
            });
        },
        reload: function () {
            vm.showList = true;
            location.href = location.href
        },
        delBy: function (index, row) {
            var userIds = [];
            userIds.push(row.userId);

            vm.del(userIds);
        },
        dels: function () {
            var userIds = [];
            $.each(vm.changeArr, function (k, v) {
                userIds.push(v.userId);
            })
            if (userIds.length == 0) {
                alert('请选择您要删除的用户！')
                return false;
            }
            vm.del(userIds)
        }
    },
    created: function () {
        this.getRoleList();
        this.getProvince();
        // this.getCity();
        this.getEntityList();
    }
});