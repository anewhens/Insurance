Vue.use(window.VueQuillEditor)
const toolbarOptions = [
  ["bold", "italic", "underline", "strike"], // 加粗 斜体 下划线 删除线
  ["blockquote", "code-block"], // 引用  代码块
  [{header: 1}, {header: 2}], // 1、2 级标题
  [{list: "ordered"}, {list: "bullet"}], // 有序、无序列表
  [{script: "sub"}, {script: "super"}], // 上标/下标
  [{indent: "-1"}, {indent: "+1"}], // 缩进
  // [{'direction': 'rtl'}],                         // 文本方向
  [{size: ["small", false, "large", "huge"]}], // 字体大小
  [{header: [1, 2, 3, 4, 5, 6, false]}], // 标题
  [{color: []}, {background: []}], // 字体颜色、字体背景颜色
  // [{font: []}], // 字体种类
  [{align: []}], // 对齐方式
  ["clean"], // 清除文本格式
  // ["link", "image", "video"] // 链接、图片、视频
  ["link", "image"] // 链接、图片、视频
];
Vue.component('q-edit', {
  template: `
    <div>
    <!-- 文件上传input 将它隐藏-->
      <el-upload
        class="avatar-uploader quill"
        :action="serverUrl"
        name="img"
        :headers="header"
        :show-file-list="false"
        :before-upload="quillUpload">
      </el-upload>

        <quill-editor 
        class="editor"
        v-model="content"
        ref="myQuillEditor" 
        :options="editorOption" 
        @blur="onEditorBlur($event)" @focus="onEditorFocus($event)"
        @change="onEditorChange($event)">
        </quill-editor>
        <div class="limit">当前已输入 <span style="color: darkred">{{nowLength()}}</span> 个字符，您还可以输入 <span style="color: darkred">{{SurplusLength()}}</span> 个字符。</div>
  </div>
  `,
  props: {
    /*编辑器的内容*/
    value: {
      type: String
    },
    /*图片大小*/
    maxSize: {
      type: Number,
      default: 4000 //kb
    }
  },
  data() {
    return {
      content: '',    // 文章内容
      fullscreenLoading: false,
      quillUpdateImg: false, // 根据图片上传状态来确定是否显示loading动画，刚开始是false,不显示
      editorOption: {
        theme: "snow", // or 'bubble'
        placeholder: "请输入...",
        modules: {
          toolbar: {
            container: toolbarOptions,
            handlers: {
              image: function (value) {
                if (value) {
                  let that = this
                  // 触发input框选择图片文件
                  document.querySelector(".avatar-uploader.quill input").click();
                } else {
                  this.quill.format("image", false);
                }
              }
            }
          }
        }
      },
      serverUrl: '', // 这里写你要上传的图片服务器地址
      header: {
        token: token
      } // 有的图片服务器要求请求头需要有token
    };
  },
  methods: {
    nowLength() {
      return this.content.length
    },
    SurplusLength() {   // 计算属性 获得当前输入字符长度
      let num = 10000 - Number(this.content.length)
      if (num > 0) {
        return num
      } else {
        return 0
      }
    },
    quillUpload: function (file) {
      this.quillUpdateImg = true;
      var fd = new FormData();
      fd.append('file', file);//传文件
      fd.append('folder', vm.folder);//传其他参数
      // axios.post(baseURL + 'business/upload?token=' + token, fd).then(
      axios.post(baseURL + 'api/upload', fd).then((res) => {
            let quill = this.$refs.myQuillEditor.quill;
            // 如果上传成功
            if (res.data.code == 0) {
              // 获取光标所在位置
              let length = quill.getSelection().index;
              // 插入图片  res.url为服务器返回的图片地址
              quill.insertEmbed(length, "image", imageURL + res.data.path);
              // 调整光标到最后
              quill.setSelection(length + 1);
            } else {
              this.$message.error("图片插入失败");
            }
            // loading动画消失
            this.quillUpdateImg = false;
          }).catch(function (error) {
        console.log(error);
        vm.$message({
          message: '上传失败',
          type: 'error'
        });
      });
    },
    // 点击图片ICON触发事件
    imgHandler(state) {
      this.addRange = this.$refs.myQuillEditor.quill.getSelection()
      if (state) {
        let fileInput = document.getElementById('imgInput')
        fileInput.click() // 加一个触发事件
      }
      this.uploadType = 'image'
    },

    // 点击视频ICON触发事件
    videoHandler(state) {
      this.addRange = this.$refs.myQuillEditor.quill.getSelection()
      if (state) {
        let fileInput = document.getElementById('imgInput')
        fileInput.click() // 加一个触发事件
      }
      this.uploadType = 'video'
    },
    // 编辑器光标离开 将编辑器内容发射给父组件
    onEditorBlur(editor) {
      // console.log(this.content);
      this.$emit('get-value', this.content)
    },
    // 编辑器获得光标
    onEditorFocus(editor) {
      editor.enable(true)   // 实现达到上限字符可删除
    },
    // 编辑器文本发生变化
    onEditorChange({ editor, html, text }) {
      let textLength = text.length
      if (textLength > 10000) {
        this.$message.error('最多输入10000个字符')
        editor.enable(false)
      }
      this.$emit('get-value', this.content)
    },
    // 清除编辑器内容
    callMethod() {
      this.content = ''
    },
    // 页面加载后执行 为编辑器的图片图标和视频图标绑定点击事件
    mounted() {
      // 为图片ICON绑定事件  getModule 为编辑器的内部属性
      this.$refs.myQuillEditor.quill.getModule('toolbar').addHandler('image', this.imgHandler)
      this.$refs.myQuillEditor.quill.getModule('toolbar').addHandler('video', this.videoHandler)  // 为视频ICON绑定事件
    }
  }
})