Vue.component('my-table', {
  template: `
    <el-table ref="multipleTable" :data="data" @selection-change="changeFun">
      <template v-for="colConfig in colConfigs">
        <slot v-if="colConfig.slot" :name="colConfig.slot"></slot>
        <component
        	v-else-if="colConfig.component"
        	:is="colConfig.component" 
        	:col-config="colConfig">
      	</component>
     		<el-table-column v-else v-bind="colConfig" align="center"></el-table-column>
      </template>  
    </el-table>
  `,
  props: ['colConfigs', 'data'],
  methods: {
    changeFun: function (val) {
      vm.changeArr = val;
    },
  }
})